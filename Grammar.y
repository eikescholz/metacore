{
module Grammar where
import Data.Char
import Scanner 
import Position
import MetaCore
import qualified IdentMap as IM
import qualified Data.Set as Set 

{- DO NOT EDIT Grammar.hs IT WAS GENERATED FROM Grammar.y 

The grammar is an actual superset of the core language to be able
to extend semantic using the meta-processor - so that more can be parsed than
is valid. For strongly typed languages that rejection possibility is given
by the type system anyway. It does not matter at what stage a parsed program is 
rejected as invalid. 

This grammar might change for a while to find a sweet spot between
fixed structure for all macro-implemented DSLs and thus between
clarity of extensions and extension flexibility. 

Metacore BNF grammar using happy generator since
 
  - Happy has good conflict detection allowing to develop fast and
    simple grammars. Simplicity comes at the cost of longer grammars,
    but the parsers are linear complexity, in particular: 

  - LALR(1) is in particular fast to parse, i.e. linear with no backtracking
    (Before the 4 Stack layout a 2-stack friendly parser combinator library
     had been used, that required cumbersome scoping rules to archive linear
     complexity)

  - Fixity levels have been limited to 10 allow to write the levels 
    as explicit rules, somewhat # but clear since happy BNF
    is stupidly simple.

  - Happy has a nice info mechanisms for finding out conflicts in grammars

  ( Currently figuring out how to access tokens with position information.
    It is possible, did that years ago )

  - The grammar does not need to yield well formed AST since due to the
    untyped nature of the meta process that check is probably necessary
    in any case. 

  - Are the macro vars really necessary ?

  - The below Grammar uses left recursion to read lists
    this is space efficient but requires to reverse the yielded
    list to get the right order. 

  - The grammar is given full - however fixity is context sensitive
    depending on what is in the module header
    the header is thus extracted as a partial parser 
    to first parsed to extract information

  - Note that the grammar is not meant to be exact metacore but 
    as ambiguous as possible while allowing to use all metacores syntax
    sugar and parenthesis omission.
    The reason for this is that it allows a lot more DSLs to be written
    using the meta/macro system.
    Specifically BNF like parser specification should be readable

      rule = a b c {}
           | d e {}
    
    (Still the case? Forgot this idea for a while, but its useful)

-}


}

%name parseModule Module
%partial parseModuleHeader ModuleHeader

%tokentype { Token }

%token 
        let             { KeywordTok ($$,"let") }

        do              { KeywordTok ($$,"do") }
        in              { KeywordTok ($$,"in") }
        import          { KeywordTok ($$,"import") }
        class           { KeywordTok ($$,"class") }
        instance        { KeywordTok ($$,"instance") }
        where           { KeywordTok ($$,"where") }
        global          { KeywordTok ($$,"global") }
        shunt           { KeywordTok ($$,"shunt") }
        collect         { KeywordTok ($$,"collect") }
        struct          { KeywordTok ($$,"struct") }
        union           { KeywordTok ($$,"union") }
        subtype         { KeywordTok ($$,"subtype") }
        

        num             { NumTok $$ } 
        var             { VarTok $$ }
        con             { ConTok $$ }
        metavar         { MetaVarTok $$ }
        metacon         { MetaConTok $$ }
        '.method'       { MethTok $$ }
        prefix          { PrefixTok $$ }
        postfix         { PostfixTok $$ }

        assocSpec       { InfixSpecTok _ $$ }

        infix0l         { InfixTok $$ (AssocLeft 0) }
        infix1l         { InfixTok $$ (AssocLeft 1) }
        infix2l         { InfixTok $$ (AssocLeft 2) }
        infix3l         { InfixTok $$ (AssocLeft 3) }
        infix4l         { InfixTok $$ (AssocLeft 4) }
        infix5l         { InfixTok $$ (AssocLeft 5) }
        infix6l         { InfixTok $$ (AssocLeft 6) }
        infix7l         { InfixTok $$ (AssocLeft 7) }
        infix8l         { InfixTok $$ (AssocLeft 8) }
        infix9l         { InfixTok $$ (AssocLeft 9) }
        infix0r         { InfixTok $$ (AssocRight 0) }
        infix1r         { InfixTok $$ (AssocRight 1) }
        infix2r         { InfixTok $$ (AssocRight 2) }
        infix3r         { InfixTok $$ (AssocRight 3) }
        infix4r         { InfixTok $$ (AssocRight 4) }
        infix5r         { InfixTok $$ (AssocRight 5) }
        infix6r         { InfixTok $$ (AssocRight 6) }
        infix7r         { InfixTok $$ (AssocRight 7) }
        infix8r         { InfixTok $$ (AssocRight 8) }
        infix9r         { InfixTok $$ (AssocRight 9) }
        
        '<='            { InfixSymTok ($$,"<=") }
        
        '$'             { PrefixSymTok ($$,"$") }

        '@'             { InfixSymTok ($$,"@") }  
        '&'             { InfixSymTok ($$,"&") } 
        '|'             { InfixSymTok ($$,"|") }
     
        '::'            { InfixSymTok ($$,"::") }

        '&&'            { SymTok ($$,"&&") } 
        '||'            { SymTok ($$,"||") }

        ':'             { SymTok ($$,":") } 
        '?'             { SymTok ($$,"?") } 
        
        ';'             { SymTok ($$,";") } 
        '='             { SymTok ($$,"=") }
        '->'            { SymTok ($$,"->") }
        '=>'            { SymTok ($$,"=>") }

        ','             { SymTok ($$,",") }
--        '.'             { SymTok ($$,".") }

        '('             { SymTok ($$,"(") }
        ')'             { SymTok ($$,")") }

        '{'             { SymTok ($$,"{") }
        '}'             { SymTok ($$,"}") }

        '{:'            { SymTok ($$,"{:") }
        ':}'            { SymTok ($$,":}") }


        '['             { SymTok ($$,"[") }
        ']'             { SymTok ($$,"]") }

        'postfix['      { SymTok ($$,"postfix[") }
        'postfix('      { SymTok ($$,"postfix(") }



%%

Module  : ModuleHeader ';' Stmts               { ($1,$3) }
        | Stmts                                { (Precs NoSpec,rv $1) }

ModuleHeader : import ModPath ';' ModuleHeader { Import (rv $2) $4 }
             | AssocSpecs                      { Precs $1 }        

AssocSpecs : assocSpec PrecOps ';' AssocSpecs  { Prec $1 $2 $4}
           |                                   { NoSpec }
 
PrecOps : infix0l PrecOps                      { (snd $1:$2) }
        | infix0l                              { [] {- infix0l is default -} }
 
Stmts : Stmts ';' Stmt                         { $3:$1  }
      | Stmt                                   { [$1]   }

Stmt : Expr                                    { $1 }
     | LetDecls                                { $1 [] Nothing }
     | LetDecls where '(' Binds ')'            { $1 (rv $4) Nothing }

ModPath : ModPath '::' var                     { ($3:$1) }
        | var                                  { [$1] }

Named : var     { \ ps -> MVar     (fst $1) (IM.subStrsToIdent (rv ($1 : ps))) UnTy}
      | con     { \ ps -> MCon     (fst $1) (IM.subStrsToIdent (rv ($1 : ps))) UnTy}
      | metavar { \ ps -> MMetaVar (fst $1) (IM.subStrsToIdent (rv ($1 : ps)))}
      | metacon { \ ps -> MMetaCon (fst $1) (IM.subStrsToIdent (rv ($1 : ps)))}

PathNamed : ModPath '::' Named                 { $3 $1 } 
          | Named                              { $1 [] }

Exprs : Exprs ',' Expr                         { $3:$1 }
      | Expr                                   { [$1]  }

InterfaceNames : InterfaceNames ',' con        {  IM.subStrsToIdent [$3] : $1 }
               | con                           { [IM.subStrsToIdent [$1]]     }

Groupid : num                                  { MNum (fst $1) (snd $1)} 
        | PathNamed                            { $1 }
        | '$' 'postfix(' Expr ')'              { MMetaEval $3 }
        | '(' ')'                              { MTuple $1 [] }
        | '(' Expr ')'                         { $2 }
        | '(' Expr ',' Exprs ')'               { MTuple $1 ($2:reverse $4) }
        | '[' ']'                              { MRefCon $1 [] UnTy}
        | '[' Exprs ']'                        { MRefCon $1 $2 UnTy}
        | '{'  '}'                             { MArray $1 [] UnTy}
        | '{' Exprs '}'                        { MArray $1 $2 UnTy}
        | '{:' InterfaceNames  ':}'            { MInterfaceVal $1 $2 }
        | Groupid '.method'                    { MAtMethod (fst $2) $1 (snd $2) UnTy}
        | Groupid 'postfix[' Expr ']'          { MAt $1 $3 UnTy}
        | Groupid 'postfix(' Expr ')'          { MApp $1 $3 UnTy}
 
PostfixedGroupid : Groupid postfix             { MAtMethod (fst $2) $1 (snd (toPostfix $2)) UnTy }
                 | Groupid '@' Region          { MAtRegion $2 $1 $3 } 
                 | Groupid                     { $1 } 
 
PrefixedGroupid : prefix PostfixedGroupid      { MAtMethod (fst $1) $2 (snd (toPrefix $1)) UnTy }
                | PostfixedGroupid             { $1 } 
 
App : App PrefixedGroupid                      { MApp $1 $2 UnTy } 
    | PrefixedGroupid                          { $1 }
 
Expr9l : Expr9l infix9l App                    { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | App                                   { $1 } 
 
Expr9r : Expr9l infix9r Expr9r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr9l                                { $1 }
 
Expr8l : Expr8l infix8l Expr9r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr9r                                { $1 } 
 
Expr8r : Expr8l infix8r Expr8r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr8l                                { $1 }
 
Expr7l : Expr7l infix7l Expr8r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr8r                                { $1 } 
 
Expr7r : Expr7l infix7r Expr7r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr7l                                { $1 }
 
Expr6l : Expr6l infix6l Expr7r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr7r                                { $1 } 
 
Expr6r : Expr6l infix6r Expr6r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr6l                                { $1 }
 
Expr5l : Expr5l infix5l Expr6r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr6r                                { $1 } 
 
Expr5r : Expr5l infix5r Expr5r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr5l                                { $1 }
 
Expr4l : Expr4l infix4l Expr5r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr5r                                { $1 } 
 
Expr4r : Expr4l infix4r Expr4r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr4l                                { $1 }
 
Expr3l : Expr3l infix3l Expr4r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr4r                                { $1 } 
 
Expr3r : Expr3l infix3r Expr3r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr3l '&&' Expr3r                    { MAnd $1 $3 } 
       | Expr3l                                { $1 }
 
Expr2l : Expr2l infix2l Expr3r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr3r                                { $1 } 
 
Expr2r : Expr2l infix2r Expr2r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr2l '||' Expr2r                    { MOr $1 $3 }  
       | Expr2l                                { $1 }
 
Expr1l : Expr1l infix1l Expr2r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr2r                                { $1 } 
 
Expr1r : Expr1l infix1r Expr1r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr1l                                { $1 }
 
Expr0l : Expr0l infix0l Expr1r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr1r                                { $1 } 
 
Expr0r : Expr0l infix0r Expr0r                 { MApp ($1`mkAtInfixMeth`$2) $3 UnTy}
       | Expr0l                                { $1 }
 

LetDecls : let '(' OptReg Decls ')'            { MLet $1 $3 (rv $4) }  
         | let '(' global OptReg Decls ')'     { MLetGlobal $1 $4 (rv $5) } 
         | let '(' shunt OptReg Decls ')'      { MLetShunt $1 $4 (rv $5) } 
 
UntypedExpr 
 : Expr0r                                      { $1 }
 | Region Expr0r                               { MAtShuntReg $1 $2 } 
 | collect OptReg Expr0r                       { MCollectAt $2 $3 }
 | shunt collect OptReg Expr0r                 { MShuntCollectAt $3 $4 }
 | shunt Expr0r                                { MShunt $1 $2}
 | do '(' Stmts ')'                            { MDo (rv $3) UnTy}
 | LetDecls where '(' Binds ')' in '(' Expr ')'{ $1 (rv $4) (Just $8) }
 | LetDecls in '(' Expr ')'                    { $1 [] (Just $4) }
 | TypeClassOrVal                              { $1 }

TypeClassOrVal 
  : class App where '(' DeclMeths ')'          { MInterface $1 $2 $5 }
  | instance App where '(' BindMeths ')'       { MInstance $1 $2 $5 }
  | struct App where '(' Decls ')'             { MStruct $1 $2 $5 }
  | union OptTypedExpr where '(' Decls ')'     { MUnion $1 $2 (rv $5) }
  

TypedExpr : Expr0r ':' OptAbstractoid          { MOfType $2 $1 $3 }

OptTypedExpr : TypedExpr                       { $1 }
             | UntypedExpr                     { $1 }

-- domain Expr01 applications are remappted to a currying sequence
-- the gramamr seems to get ambiguous otherwise         
OptAbstractoid : Expr0r '->' OptAbstractoid    { MAbstractoid $1 $3 }
               | Expr0r '=>' OptAbstractoid    { MMetaAbstr $1 $3 }
               | OptTypedExpr                  { $1 }

OptConjunctions 
  : OptAbstractoid '&' OptConjunctions         { MConj $1 $3 }
  | OptAbstractoid                             { $1 }
                     
OptAlternatives 
  : OptConjunctions '|' OptAlternatives        { MAlt $1 $3 }
  | OptConjunctions                            { $1 }
             
MatchExpr : Expr0r '?' OptAlternatives         { MMatches $2 $1 $3 UnTy}

Expr : MatchExpr where '(' Stmts ')'           { MWhere $3 $1 $4 }                                    
     | MatchExpr                               { $1 } 
     | OptAlternatives                         { $1 }
     
DefVar : App ':' OptAbstractoid '=' Expr       { MDefVar $1 $3 $5}

DeclVar : App ':' OptAbstractoid               { MDeclVar $1 $3 }
        | Bind                                 { $1 }
       
Decls : Decls ';' DeclVar                      { $3:$1  }
      | DeclVar                                { [$1]   }

DefMeth 
  : Expr0r ':' OptAbstractoid '=' Expr         { MDefMeth $1 $3 $5}

DeclMeth : Expr0r ':' OptAbstractoid           { MDeclMeth $1 $3}
         | DefMeth                             { $1 }

DeclMeths : DeclMeths ';' DeclMeth             { $3:$1  }
          | DeclMeth                           { [$1]   }

Bind : App '=' Expr                            { MBindVar $1 $3 }
     | DefVar                                  { $1 }
     | TypeClassOrVal                          { $1 }

BindMeth : Expr0r '=' Expr                     { MBindMeth $1 $3}
         | DefMeth                             { $1 }  

Binds : Binds ';' Bind                         { $3:$1  }
      | Bind                                   { [$1]   }

BindMeths : BindMeths ';' BindMeth             { $3:$1  }
          | BindMeth                           { [$1]   }

RegOrd : var '<=' var                          { ($1,$3) }

RegOrds : RegOrds ',' RegOrd                   { $3:$1 }
        | RegOrd                               { [$1] } 

Region : '@' var 'postfix(' RegOrds ')'        { ($2,reverse $4) }
       | '@' var                               { ($2,[]) }

OptReg : '@' var                               { Just ($2,[]) }
       |                                       { Nothing }
 
{ 
 
-- For prototype parsing terminate at first error is OK

happyError :: [Token] -> a
happyError t = error ("Parse error, unexpected: "++show t++"\n")


}

