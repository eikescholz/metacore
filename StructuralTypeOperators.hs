module StructuralTypeOperators where
{-
 This module defines structural level type union and intersection
 where structural means that only the information weather
 a constructor name is a interface or a predicate 
 and the base type they are defined on are used.  
 
 NOTE: The operations remove any typing, since generating the typing
       ought be the last step before translating to core. 

 TODO: Complete

-}
import MetaCore
import qualified IdentMap as IM
import qualified Data.Map as Map
import qualified Data.Set as Set
import Text.PrettyPrint
import MetaAlphaConversion
import Position
import Data.Data
import Data.Generics.Uniplate.Data
import Control.Monad.Except
import Data.List
import Control.Monad.Trans.State.Lazy


-- Not all SExprs form a type value

isType :: MExpr -> Bool
isType (MVar _ _ _) = True 
isType (MCon _ _ _) = True
isType (MMetaVar _ _) = True 
isType (MMetaCon _ _) = True
isType (MNum _ _) = True
isType (MTuple _ es) = foldr1 (&&) (map isType es)
--isType (MArray _ es _) = foldr1 (&&) (map isType es) -- thats a  value not
isType (MAt _ (MVar _ _ _) _) = True
isType (MAt _ (MNum _ _) _) = True
isType (MApp a b _) = isType a && isType b
isType (MMetaAbstr a b) = isType a && isType b



-- scup should be the principal
-- combines to types into a principal type
-- the only subtype relation is given by interfaces.
-- interfaces must be a-cyclic
-- the principal of ($a, I0 $b)  (I1 $a, $b) is ($a, $b)
-- so a descend is needed. 
--
-- For 
--  f : (a => b)
--  g : (x => y)
-- a variable with a major corresponding mayor type 
-- h : (a => b) `major` (x => y) = (a `minor` x => b `major` y )
-- 
-- A major is a super-type, however for functions arguments compatible i.e.
--  h = f and h = g  
-- must be a valid binding, i.e. h x == f x as well as h x == g x  must 
-- be type save.
-- (This is the reason for the `minor` in the domain)


majorTypeOf :: MType -> MType -> EMT MType
majorTypeOf a b = majorTypeOf' (alphaConversionToNF a) (alphaConversionToNF b)
 
majorTypeOf' :: MType -> MType -> EMT MType
majorTypeOf' t0@(MMetaAbstr p0 c0) t1@(MMetaAbstr p1 c1) = do
  p <- minorTypeOf' p0 p1
  c <- majorTypeOf' c0 c1
  return $ MMetaAbstr p c

majorTypeOf' t0 t1 = biDescendWith majorTypeOf'' t0 t1
  
  
majorTypeOf'' t0 t1 = do
  (ns0,t0') <- splitIfNames t0
  (ns1,t1') <- splitIfNames t1
  if ns0 == [] && ns1 == [] 
    then if toConstr t0' == toConstr t1' 
            then if children t0' == [] || children t1' == []
                   then if t0' == t1' -- compare leave values 
                           then return t0  -- if equal use left 
                           else throwError $ majorErrMsg t0' t1' 
                   else biDescendWith majorTypeOf' t0' t1'
            else throwError $ majorErrMsg t0' t1'
    else do 
      con <- intersectIfs (pos t0') ns0 ns1
      e   <- majorTypeOf' t0' t1'
      return $ MApp con e UnTy 
   
majorErrMsg t0 t1 = error "unimplemented"      
   
intersectIfs :: Pos ->[IM.ValueIdent] -> [IM.ValueIdent] -> EMT MType
intersectIfs p is is' = return $ idents2IfCon p (is `intersect` is')



-- also intersection, contains all values 
-- that of which the arguments are super-sets
-- the main Problem is the abstraction 
-- For 
--  f : (a => b)
--  g : (x => y)
-- a variable with a corresponding minor type is
-- h : (a => b) `minor` (x => y) = (a `major` x => b `minor` y )
-- 
-- A minor is a subtype, but for function types argument must be
-- compatible i.e.
--  f = h and g = h
-- must be a valid binding, and f x == h x as well as g x = h x must be type save.
-- (This is the reason for the `major` in the domain)
--
-- where is the minor required ? (case type calculation?) ?


minorTypeOf :: MType -> MType -> EMT MType
minorTypeOf a b = minorTypeOf' (alphaConversionToNF a) (alphaConversionToNF b)

minorTypeOf' :: MType -> MType -> EMT MType
minorTypeOf' t0@(MMetaAbstr p0 c0) t1@(MMetaAbstr p1 c1) = do
  p <- majorTypeOf' p0 p1
  c <- minorTypeOf' c0 c1
  return $ MMetaAbstr p c

minorTypeOf' t0 t1 = biDescendWith minorTypeOf'' t0 t1

minorTypeOf'' t0 t1 = do
  (ns0,t0') <- splitIfNames t0
  (ns1,t1') <- splitIfNames t1
  if ns0 == [] && ns1 == [] 
    then if toConstr t0' == toConstr t1' 
            then if children t0' == [] || children t1' == []
                   then if t0' == t1' -- compare leave values 
                           then return t0  -- if equal use left 
                           else throwError $ minorErrMsg t0' t1' 
                   else biDescendWith minorTypeOf' t0' t1'
            else throwError $ minorErrMsg t0' t1'
    else do 
      con <- mergeIfs (pos t0) ns0 ns1
      e   <- minorTypeOf' t0' t1'
      return $ MApp con e UnTy

minorErrMsg t0 t1 = error "unimplemented"

mergeIfs' :: [IM.ValueIdent] -> [IM.ValueIdent] -> EMT [IM.ValueIdent]
mergeIfs' is is' = do 
  js  <- filterM ( \ i  -> isSubIf' i  is' ) is   
  js' <- filterM ( \ i' -> isSubIf' i' is  ) is'                                   
  return $ nub ( (is\\js) ++ (is'\\js'))                                     


mergeIfs :: Pos -> [IM.ValueIdent] -> [IM.ValueIdent] -> EMT MType
mergeIfs p is is' = do is'' <- mergeIfs' is is'
                       return $ idents2IfCon p is'' 


isSubIf :: IM.ValueIdent -> IM.ValueIdent -> EMT Bool
isSubIf i i' = do 
  st <- lift $ get
  case Map.lookup i' (interfaces st) of
    Just ds -> return $ i `Set.member` subInterfaces ds 
               
isSubIf' :: IM.ValueIdent -> [IM.ValueIdent] -> EMT Bool                
isSubIf' i is = do 
  bs <- mapM (isSubIf i) is
  return $ foldr (||) False bs



idents2IfCon :: Pos -> [IM.ValueIdent] -> MType
idents2IfCon p [i] = MCon p i UnTy
idents2IfCon p is  = MInterfaceVal p is

splitIfNames :: MType -> EMT ([IM.ValueIdent],MType)
splitIfNames expr@(MApp (MInterfaceVal p ns) e _) = return (ns,e)      
splitIfNames expr@(MApp (MCon p n _) e _) = do 
  ns <- getInterfacesInScope
  if n `Map.member` ns 
    then return ([n],e)
    else return ([],expr)
splitIfNames expr = return ([],expr)     
