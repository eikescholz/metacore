module ScannerTests where
import Position
import Scanner
import Layout
import Control.Monad.Trans.State.Lazy
import Scanner 
import TestBase
import Data.List

testLex str = do
   let ls  = scan p11 str
   let lls = layout ls
   return lls 

scansTo x y = do 
  a <- x
  b <- y 
  if (a == b)
    then return TestSuccess
    else return (TestFailure ("Condition `scansTo` failed!"
                                      ++ "\n\n LHS: [ "++pp a 
                                      ++ "\n\n RHS: [ "++pp b  ))
 where
   pp ts = concat (intersperse ",\n        " (map show ts)) ++ "]\n"

   
p11 = Pos (1,1,"")



scannerTests = [
  Describe "Scanner"
    [ It "scans (foo)"  
        (testLex "(foo)" `scansTo` return 
          [ SymTok (Pos (1,1,""),"("),
            VarTok (Pos (1,2,""),"foo"),
            SymTok (Pos (1,5,""),")")] ),

      It "scans (foo,bar)"  
        (testLex "(foo,bar)" `scansTo` return 
          [ SymTok (Pos (1,1,""),"("),
            VarTok (Pos (1,2,""),"foo"),
            SymTok (Pos (1,5,""),","),
            VarTok (Pos (1,6,""),"bar"),
            SymTok (Pos (1,9,""),")")] ),

      It "scans (foo,(bla,fasel))"
        (testLex "(foo,(bla,fasel))" 
          `scansTo` return 
            [ SymTok (Pos (1,1,""),"("),
              VarTok (Pos (1,2,""),"foo"),
              SymTok (Pos (1,5,""),","),
              SymTok (Pos (1,6,""),"("),
              VarTok (Pos (1,7,""),"bla"),
              SymTok (Pos (1,10,""),","),
              VarTok (Pos (1,11,""),"fasel"),
              SymTok (Pos (1,16,""),")"),
              SymTok (Pos (1,17,""),")")] ),

      It "scans (n-1)"
       (testLex "(n-1)" `scansTo` return 
         [ SymTok (Pos (1,1,""),"("),
           VarTok (Pos (1,2,""),"n"),
           InfixTok (Pos (1,3,""),"-") (AssocLeft 0),
           NumTok (Pos (1,4,""),defaultTargetWord 1),
           SymTok (Pos (1,5,""),")")] ), 


      It "scans &foo"
       (testLex "&foo" `scansTo` return 
         [ PrefixSymTok (Pos (1,1,""),"&"),
           VarTok (Pos (1,2,""),"foo")] ), 

      It "scans xs.foo"
       (testLex "xs.foo" `scansTo` return 
          [ VarTok (Pos (1,1,""),"xs"),
            MethTok (Pos (1,3,""),".foo")]),
      
      It "scans n0.infix-"
      (testLex "n0.infix-" `scansTo` return 
         [ VarTok (Pos (1,1,""),"n0"),
           MethTok (Pos (1,3,""),".infix-")]),
     
            
      It "scans ++i"
       (testLex "++i" `scansTo` return 
         [ PrefixTok (Pos (1,1,""),"++"),
           VarTok (Pos (1,3,""),"i")]),

       It "scans ++(i)"
        (testLex "++(i)" `scansTo` return 
          [ PrefixTok (Pos (1,1,""),"++"),
            SymTok (Pos (1,3,""),"("),
            VarTok (Pos (1,4,""),"i"),
            SymTok (Pos (1,5,""),")")]),
 
       It "scans f ++x y"
        (testLex "f ++x y" `scansTo` return 
          [ VarTok (Pos (1,1,""),"f"),
            PrefixTok (Pos (1,3,""),"++"),
            VarTok (Pos (1,5,""),"x"),
            VarTok (Pos (1,7,""),"y")]),

       It "scans $xs[i]"
        (testLex "$xs[i]" `scansTo` return 
          [ MetaVarTok (Pos (1,2,""),"$xs"),
            SymTok (Pos (1,4,""),"postfix["),
            VarTok (Pos (1,5,""),"i"),
            SymTok (Pos (1,6,""),"]")] ),

       It "scans xs[i][j][k]"
        (testLex "xs[i][j][k]" `scansTo` return 
          [ VarTok (Pos (1,1,""),"xs"),
            SymTok (Pos (1,3,""),"postfix["),
            VarTok (Pos (1,4,""),"i"),
            SymTok (Pos (1,5,""),"]"),
            SymTok (Pos (1,6,""),"postfix["),
            VarTok (Pos (1,7,""),"j"),
            SymTok (Pos (1,8,""),"]"),
            SymTok (Pos (1,9,""),"postfix["),
            VarTok (Pos (1,10,""),"k"),
            SymTok (Pos (1,11,""),"]")] ),
      
       It "scans *xs[i](j)[k]"
        (testLex "*xs[i](j)[k]" `scansTo` return 
          [ PrefixTok (Pos (1,1,""),"*"),
            VarTok (Pos (1,2,""),"xs"),
            SymTok (Pos (1,4,""),"postfix["),
            VarTok (Pos (1,5,""),"i"),
            SymTok (Pos (1,6,""),"]"),
            SymTok (Pos (1,7,""),"postfix("),
            VarTok (Pos (1,8,""),"j"),
            SymTok (Pos (1,9,""),")"),
            SymTok (Pos (1,10,""),"postfix["),
            VarTok (Pos (1,11,""),"k"),
            SymTok (Pos (1,12,""),"]")]),
{-
       It "scans ++f(x)"
        (testLex "++f(x)" `scansTo` return []),

       It "scans f(x)++"
        (testLex "f(x)++" `scansTo` return []),
-}
        It "scans *x++"
         (testLex "*x++" `scansTo` return 
           [ PrefixTok (Pos (1,1,""),"*"),
             VarTok (Pos (1,2,""),"x"),
             PostfixTok (Pos (1,3,""),"++")]), 

        It "scans *xs[i].foo(j)++" 
         (testLex "*xs[i].foo(j)++" `scansTo` return 
           [ PrefixTok (Pos (1,1,""),"*"),
             VarTok (Pos (1,2,""),"xs"),
             SymTok (Pos (1,4,""),"postfix["),
             VarTok (Pos (1,5,""),"i"),
             SymTok (Pos (1,6,""),"]"),
             MethTok (Pos (1,7,""),".foo"),
             SymTok (Pos (1,11,""),"postfix("),
             VarTok (Pos (1,12,""),"j"),
             SymTok (Pos (1,13,""),")"),
             PostfixTok (Pos (1,14,""),"++")]),

        It "scans a + b" 
         (testLex "a + b" `scansTo` return 
           [ VarTok (Pos (1,1,""),"a"),
             InfixTok (Pos (1,3,""),"+") (AssocLeft 0),
             VarTok (Pos (1,5,""),"b")] ),

        It "scans  a + b (with initial space)" 
         (testLex " a + b" `scansTo` return 
           [ VarTok (Pos (1,2,""),"a"),
             InfixTok (Pos (1,4,""),"+") (AssocLeft 0),
             VarTok (Pos (1,6,""),"b")] ),
 
        It "scans - b" -- That should not parse anymore
         (testLex "- b" `scansTo` return 
           [ InfixTok (Pos (1,1,""),"-") (AssocLeft 0),
             VarTok (Pos (1,3,""),"b")]),

         It "scans x -> f x" 
          (testLex "x -> f x" `scansTo` return 
            [ VarTok (Pos (1,1,""),"x"),
              SymTok (Pos (1,3,""),"->"),
              VarTok (Pos (1,6,""),"f"),
              VarTok (Pos (1,8,""),"x")]),

         It "scans x -> f x | y -> f y" 
          (testLex "x -> f x | y -> f y" `scansTo` return 
            [ VarTok (Pos (1,1,""),"x"),
              SymTok (Pos (1,3,""),"->"),
              VarTok (Pos (1,6,""),"f"),
              VarTok (Pos (1,8,""),"x"),
              InfixSymTok (Pos (1,10,""),"|"),
              VarTok (Pos (1,12,""),"y"),
              SymTok (Pos (1,14,""),"->"),
              VarTok (Pos (1,17,""),"f"),
              VarTok (Pos (1,19,""),"y")]),
 
         It "scans x -> f x | y -> f y | z -> f z" 
          (testLex "x -> f x | y -> f y | z -> f z" `scansTo` return 
            [ VarTok (Pos (1,1,""),"x"),
              SymTok (Pos (1,3,""),"->"),
              VarTok (Pos (1,6,""),"f"),
              VarTok (Pos (1,8,""),"x"),
              InfixSymTok (Pos (1,10,""),"|"),
              VarTok (Pos (1,12,""),"y"),
              SymTok (Pos (1,14,""),"->"),
              VarTok (Pos (1,17,""),"f"),
              VarTok (Pos (1,19,""),"y"),
              InfixSymTok (Pos (1,21,""),"|"),
              VarTok (Pos (1,23,""),"z"),
              SymTok (Pos (1,25,""),"->"),
              VarTok (Pos (1,28,""),"f"),
              VarTok (Pos (1,30,""),"z")]),

        It "scans x : Int -> x" 
         (testLex "x : Int -> x" `scansTo` return 
           [ VarTok (Pos (1,1,""),"x"),
             SymTok (Pos (1,3,""),":"),
             ConTok (Pos (1,5,""),"Int"),
             SymTok (Pos (1,9,""),"->"),
             VarTok (Pos (1,12,""),"x")] ),

        It "scans let x = y" 
         (testLex "let y = y" `scansTo` return 
            [ KeywordTok (Pos (1,1,""),"let"),
              SymTok (Pos (1,1,""),"("),
              VarTok (Pos (1,5,""),"y"),
              SymTok (Pos (1,7,""),"="),
              VarTok (Pos (1,9,""),"y"),
              SymTok (Pos (0,0,""),")")] )
 
     ]
   ]


