{- A generator for names
   used for name normalizing and
   fresh unbound/unsused variable generation. 
-}

module NameGen where

initialVarChars = "abcdefghijklmnopqrstuvwxyz"
initialConChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

mkNamesFrom :: String -> [String] 
mkNamesFrom cs = [ [c] | c <- cs ] ++ [ c : show n | n <- [0..] , c <- cs  ] 

varNames = mkNamesFrom initialVarChars 
conNames = mkNamesFrom initialConChars 

metaVarNames = map (\cs->'$':cs) varNames
metaConNames = map (\cs->'$':cs) conNames
