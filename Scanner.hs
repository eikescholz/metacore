{-# LANGUAGE DeriveDataTypeable #-}
-- This module extracts lexemes form a string
module Scanner where
import Data.Char
import Data.Data
import Position
import CharSpecs
import CoreNumbers
import Data.Word
{-
This is a greedy O(n) scanner that primary solves the problem of
of discriminating

a++b and a ++ b
from a++ b 
and a ++b

-}


-- TODO: Cleanup/Rewrite Scanner

data Assoc = AssocLeft Int | AssocRight Int  
 deriving (Show,Eq,Data)

-- initial scan creates default assocs
-- precedence is added in a separate sweep 
defaultAssoc = AssocLeft 0 


data Token = MetaVarTok    SubStr -- $foo 
           | VarTok        SubStr  
           | ConTok        SubStr
           | MetaConTok    SubStr
           | SymTok        SubStr
           | MethTok       SubStr
           | KeywordTok    SubStr
           | PrefixTok     SubStr
           | InfixTok      SubStr Assoc
           | InfixSpecTok  Pos Assoc
           | PostfixTok    SubStr
           | PrefixSymTok  SubStr
           | InfixSymTok   SubStr
           | NumTok        (Pos,CoreNumber)
           | StrTok        SubStr 
  deriving (Show,Eq,Data,Typeable) 

instance HasPos Token where
  pos (InfixSpecTok p _) = p
  pos t = genericPos t

skipSpaces :: Pos -> String -> (Pos,String)
skipSpaces p (' ':cs) = skipSpaces (incCol p) cs
skipSpaces p ('#':cs) = let (comment,cs') = span (/='\n') cs 
                            p'            = addCol p (length comment)
                         in (p',cs')

-- factor out in parameter for cross compilation
defaultTargetWord = CNWord64 
defaultTargetFloat = CNFloat64 

mkDefaultTargetWord  str = defaultTargetWord (read str);
mkDefaultTargetFloat str = defaultTargetFloat (read str);

-- TODO: Change these to use proper list of reserved
-- symbols

reservedSyms = ["=",":=","->",","]


mkPrefixTok p "&"  = PrefixSymTok (p,"&")
--mkPrefixTok p "*"  = PrefixSymTok (p,"*")
mkPrefixTok p "%"  = PrefixSymTok (p,"%")
mkPrefixTok p s | s `elem` reservedSyms = SymTok (p,s)
mkPrefixTok p str  = PrefixTok (p,str)

-- TODO: Why was there a distinction between infixSymTok and SymTok ?
--       Is it still required? Won't change untill parser tests are sufficient.

mkInfixTok p "@" = InfixSymTok (p,"@")
mkInfixTok p "&" = InfixSymTok (p,"&")
mkInfixTok p "|" = InfixSymTok (p,"|")
mkInfixTok p "::" = InfixSymTok (p,"::")
mkInfixTok p s | s `elem` reservedSyms = SymTok (p,s)
mkInfixTok p str = InfixTok (p,str) defaultAssoc

mkPostfixTok p s | s `elem` reservedSyms = SymTok (p,s)
mkPostfixTok p str = PostfixTok (p,str)


scanPrefixOrInfix :: Pos -> String -> [Token]
scanPrefixOrInfix p cs
  = let (str,cs') = span isSym cs
        p'        = addCol p (length str)
     in case cs' of 
          []                     -> mkInfixTok  p str    : scan p' cs'
          (x:xs) | isEndSep x    -> mkInfixTok  p str    : scan p' cs'
          _                      -> mkPrefixTok p str    : scan p' cs'
 
scanPostfixOrInfix :: Pos -> String -> [Token]
scanPostfixOrInfix p [] = []
scanPostfixOrInfix p ('.':c:cs) | isAlpha c =  scanMeth p ('.':c:cs)
scanPostfixOrInfix p (c:cs) | isSym c
  = let (str,cs') = span isSym (c:cs)
        p'  = addCol p (length str)
     in case cs' of 
          []                  -> mkPostfixTok p str : scan p' cs'
          (x:xs) | isEndSep x -> mkPostfixTok p str : scan p' cs'                    
          _                   -> mkInfixTok p str : scan p' cs'
scanPostfixOrInfix p cs = scan p cs
 
scanVarOrToc ::  Pos -> String -> [Token]
scanVarOrToc p (c:cs) | isLower c = scanVar p (c:cs)
scanVarOrToc p (c:cs) | isUpper c = scanCon p (c:cs)



scanOptionalPostfix p ('(':cs)         = SymTok (p,"postfix(") : scan (incCol p) cs
scanOptionalPostfix p ('{':cs)         = SymTok (p,"postfix{") : scan (incCol p) cs
scanOptionalPostfix p ('[':cs)         = SymTok (p,"postfix[") : scan (incCol p) cs
scanOptionalPostfix p (c:cs) | isSym c = scanPostfixOrInfix p (c:cs) 
scanOptionalPostfix p cs               = scan p cs

scanMVar :: Pos -> String -> [Token]
scanMVar p cs
  = let (name,cs') = span isNameChar cs
        p' = addCol p (length name)
     in MetaVarTok (p,'$':name) : scanOptionalPostfix p' cs'

scanMeth :: Pos -> String -> [Token]     
scanMeth p ('.':'p':'r':'e':'f':'i':'x':c:cs) | isSym c
  = let (op,cs') = span isSym (c:cs)
        p' = addCol p (length op + length ".prefix") 
     in MethTok (p,".prefix"++op) : scanOptionalPostfix p' cs'
scanMeth p ('.':'i':'n':'f':'i':'x':c:cs) | isSym c
  = let (op,cs') = span isSym (c:cs)
        p' = addCol p (length op + length ".infix") 
     in MethTok (p,".infix"++op) : scanOptionalPostfix p' cs'

scanMeth p ('.':'p':'o':'s':'t':'f':'i':'x':c:cs) | isSym c
  = let (op,cs') = span isSym (c:cs)
        p' = addCol p (length op + length ".postfix") 
     in MethTok (p,".postfix"++op) : scanOptionalPostfix p' cs'

scanMeth p ('.':cs)
     = let (name,cs') = span isNameChar cs
           p' = addCol p (length name + 1)
        in MethTok (p,'.':name) : scanOptionalPostfix p' cs'
        

scanVar :: Pos -> String -> [Token]     
scanVar p cs
  = let (name,cs') = span isNameChar cs
        p' = addCol p (length name)
     in VarTok (p,name) : scanOptionalPostfix p' cs'

scanCon :: Pos -> String -> [Token]
scanCon p cs
  = let (name,cs') = span isNameChar cs
        p' = addCol p (length name)
     in ConTok (p,name) : scanOptionalPostfix p' cs'


scanNum :: Pos -> String -> [Token]
scanNum p cs 
 = let (n,p',cs') = scanCoreNumber mkDefaultTargetWord mkDefaultTargetWord p cs 
    in NumTok (p,n) : scanOptionalPostfix p' cs' 



scan :: Pos -> String -> [Token]

scan p ('#':cs)  = scan p (dropWhile (/='\n') cs)
scan p (' ':cs)  = scan (incCol p) cs
scan p ('\n':cs) = scan (newLn p)  cs

scan p ('(':cs) = SymTok (p,"(") : scan (incCol p) cs
scan p ('{':cs) = SymTok (p,"{") : scan (incCol p) cs
scan p ('[':cs) = SymTok (p,"[") : scan (incCol p) cs
scan p (')':cs) = SymTok (p,")") : scanOptionalPostfix (incCol p) cs
scan p ('}':cs) = SymTok (p,"}") : scanOptionalPostfix (incCol p) cs
scan p (']':cs) = SymTok (p,"]") : scanOptionalPostfix (incCol p) cs
scan p (',':cs) = SymTok (p,",") : scan (incCol p) cs
scan p (';':cs) = SymTok (p,";") : scan (incCol p) cs

scan p ('-':'>':c:cs) | not (isSym c) 
  = SymTok (p,"->") : scan (addCol p 2) (c:cs)
scan p ('=':'>':c:cs) | not (isSym c) 
  = SymTok (p,"=>") : scan (addCol p 2) (c:cs)

scan p (':':c:cs) | not (isSym c) = SymTok (p,":") : scan (incCol p) (c:cs)
scan p ('?':c:cs) | not (isSym c) = SymTok (p,"?") : scan (incCol p) (c:cs)



scan p ('=':c:cs) | not (isSym c) = SymTok (p,"=") : scan (incCol p) (c:cs)

scan p ('i':'n':'f':'i':'x':d:'l':c:cs) | not (isNameChar c) 
  = InfixSpecTok p (AssocLeft (read [d]))  : scan (addCol p 7) (c:cs)

scan p ('i':'n':'f':'i':'x':d:'r':c:cs) | not (isNameChar c) 
  = InfixSpecTok p (AssocRight (read [d]))  : scan (addCol p 7) (c:cs)

scan p ('g':'l':'o':'b':'a':'l':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"global") : scan (addCol p 6) (c:cs)
scan p ('i':'m':'p':'o':'r':'t':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"import") : scan (addCol p 6) (c:cs)
scan p ('s':'t':'r':'u':'c':'t':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"struct") : scan (addCol p 6) (c:cs)
scan p ('u':'n':'i':'o':'n':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"union") : scan (addCol p 5) (c:cs)
scan p ('c':'l':'a':'s':'s':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"class") : scan (addCol p 5) (c:cs)
scan p ('i':'n':'s':'t':'a':'n':'c':'e':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"instance") : scan (addCol p 8) (c:cs)
scan p ('w':'h':'e':'r':'e':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"where") : scan (addCol p 5) (c:cs)
scan p ('t':'y':'p':'e':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"type") : scan (addCol p 4) (c:cs) 
scan p ('f':'o':'r':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"for") : scan (addCol p 3) (c:cs)
scan p ('l':'e':'t':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"let") : scan (addCol p 3) (c:cs) 
scan p ('d':'o':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"do") : scan (addCol p 2) (c:cs)
scan p ('i':'n':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"in") : scan (addCol p 2) (c:cs)
scan p ('b':'e':c:cs) | not (isNameChar c) 
  = KeywordTok (p,"be") : scan (addCol p 2) (c:cs)

scan p ('$':c:cs) | isLower c = scanMVar (incCol p) (c:cs)
scan p ('$':c:cs) | isUpper c = error "unimplemented" 
scan p (c:cs) | isDigit c = scanNum p (c:cs)
scan p (c:cs) | isLower c = scanVar p (c:cs)
scan p (c:cs) | isUpper c = scanCon p (c:cs)
scan p (c:cs) | isSym   c = scanPrefixOrInfix p (c:cs)

scan p (c:cs) = (error ("Lexical error: Invalid character at line "++
                  (show (ln p))++" and col "++(show (col p))))
scan p [] = []


