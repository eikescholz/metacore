{- associate makes all names unique over all modules
   and removes ambiguities. 

   Association is defined by scoping.

   If a variable is in scope it will be associated with scope value.
   AbstractionVars as for 
     $a => $a => $a 
   must associate to the same variable. 

   The SFINAE implemementation should assume that nested bindings
   are bound to the same unique identifier .. 
   let $a : .. 
     in let $a : ... 

   I.e. the language actually does not support shadowing
   variables, but that yields support of dynamic patterns patterns instead. 

   Collisions in substitution semantics are solved via
   mandatory alpha conversion of bound variables to fresh ones. 

   For association this should make things easy, just
   bind all variables in a block to the unique name generated
   for the first name without a unique identifier.

   The underlying Rule:  
   The programmer writing the a name refers, as long as the
   name is in scope, to the same entity and does not redefine it while it 
   is in scope. i.e.
    (x -> x -> f x) Will check the second argument for equality with the
    first.  

 -}  

module Associate where

import IdentMap
import Data.Set
import qualified Data.Map as Map
import qualified IdentMap as IM
import Control.Monad.Trans.State.Lazy
import MetaCore
import Text.PrettyPrint
import Data.Generics.Uniplate.Operations
import Position

type ModulePath = [String] -- TODO: Move where it belongs

type AssocScope = Map.Map Ident UIdent

type AssocState = (ModulePath,AssocScope)

type AssocMonad = State AssocState



nestDoLets :: PExpr -> PExpr 
nestDoLets = transform nest
  where 
    nest :: PExpr -> PExpr
    nest (MDo es tp) = MDo (nest' es) tp
    nest e           = e

    nest' ( MLet       p r ds ws Nothing : es ) = MLet       p r ds ws Nothing : nest' es
    nest' ( MLetGlobal p r ds ws Nothing : es ) = MLetGlobal p r ds ws Nothing : nest' es
    nest' ( MLetShunt  p r ds ws Nothing : es ) = MLetShunt  p r ds ws Nothing : nest' es
    nest' []                                    = []

annotateRegions :: PExpr -> SExpr 
annotateRegions e = transform annotate e
  where -- just annotate reg with impossible position for an r
    annotate (MLet       p Nothing ds ws is) = MLet       p (Just ((addCol p 3,"reg" ),[])) ds ws is 
    annotate (MLetGlobal p Nothing ds ws is) = MLetGlobal p (Just ((addCol p 3,"reg" ),[])) ds ws is
    annotate (MLetShunt  p Nothing ds ws is) = MLetShunt  p (Just ((addCol p 3,"reg" ),[])) ds ws is
    annotate e                               = e 

-- This is at the minimum an other trick to only need an LALR(1) parser
-- maybe be needed to be interwoven into the pattern meta patternmachting

deAbstractoid :: PExpr -> PExpr -- :: PExpr -> Maybe MAbstr  
deAbstractoid e = case cases e of
                    [] -> descend deAbstractoid e -- e is not an abstractoid abstraction of any kind
                    cs -> MAbstr cs UnTy
  where
    cases :: MExpr -> [(SPat,[(Maybe MExpr,MExpr)])]
    cases (MAbstractoid pat ex)                                  = [(pat,[(Nothing,ex)])]
    cases (MAlt (MAbstractoid pat ex) e)                         = (pat,[(Nothing,ex)])   : cases e
    cases (MAlt (MAlt (MAbstractoid pat ex) cs) e)               = (pat,[(Nothing,ex)])   : cases e
    cases (MAlt (MConj cond (MAbstractoid pat ex)) e)            = (pat,[(Just cond,ex)]) : cases e 
    cases (MAlt (MConj (MConj cond (MAbstractoid pat ex)) ds) e) = (pat,(Just cond,ex) : conds ds) : cases e
    cases e                                                      = []

    conds :: MExpr -> [(Maybe MExpr,MExpr)]
    conds (MAbstractoid cond ex)            = [(Just cond,ex)]
    conds (MConj (MAbstractoid cond ex) ds) = (Just cond,ex) : conds ds
    conds d                                 = []

{- Pseudocode. Is that a good idea? Requied? Checked after transformation to core?  }
-- only pure decls in the first block, makes detecting duplicate binds easier.
-- after this check decl and bind block independently ... 
--   replaces def with a decl and a bind. 
desugarLet :: SExpr IM.ValueIdent SReg -> SExpr IM.ValueIdent SReg
desugarLet = transform desug 
  where 
    desug (MLet       p r ds ws es) = dsug' MLet       p r ds ws es
    desug (MLetGlobal p r ds ws es) = dsug' MLetGlobal p r ds ws es
    desug (MLetShunt  p r ds ws es) = dsug' MLetShunt  p r ds ws es
    desug e                         = e
    -- Note: Using a decl and a bind instead of a def, will cause double bind error. 
    desug' con p r ds ws es 
      = let decls = [ v | v@(MDeclVar _ _ ) <- ds ]
            binds = [ v | v@(MBindVar _ _ ) <- ds ] 
            defs  = [ v | v@(MDefVar _ _ _) <- ds ]

            bindDecls = [ MDeclVar pat (mkTy (pos pat)) | MBindVar pat ty <- binds ]
            defDecls  = [ MDeclVar pat ty | MDefVar pat ty _ <- decls ]
            defBinds  = [ MBindVar pat e | MDefVar pat _ e <- decls ]

            interface = decls ++ bindDecls ++ defDecls 
            implement = ws ++ defBinds
         in if length ds != length decls + length binds + length defs 
              then error (show p ++ ": let desugar found unsupprted subexprs") 
              else con p r interface inmplement es     

    mkTy p = mkUIdent (incPos p,"t")
 -}

associate :: AssocState -- Module Path 
              -> SExpr 
              -> MExpr -- FIXME: transformM does not support that kind of type rewrite ... 
associate state expr = evalState (assoc expr) state
 
 where
    
  updateIdent :: (Pos -> ValueIdent -> b) -> Pos -> ValueIdent -> AssocMonad b
  updateIdent con p (UIdent s) = return $ con p (UIdent s) 
  updateIdent con p (Ident  s) 
    = do (path,scope) <- get
         case Map.lookup s scope of 
           Just s' -> return $ con p (UIdent s') -- already bound -> replace
           Nothing -> do let n' = path ++ s
                             s' = (n',getLn p,getCol p,0)
                         put (path,Map.insert s s' scope)
                         return $ con p (UIdent s') 
  
  descendWithScope :: SExpr -> AssocMonad MExpr
  descendWithScope e = do st <- get
                          e' <- descendM assoc e
                          put st
                          return e'

  assoc :: SExpr -> AssocMonad MExpr
  
  assoc (MVar     p s tp) = updateIdent (\p' i->MVar p' i tp) p s 
  assoc (MMetaVar p s )   = updateIdent MMetaVar p s 
  assoc (MCon     p s tp) = updateIdent (\p' i->MCon p' i tp) p s  
  assoc (MMetaCon p s )   = updateIdent MMetaCon p s 
  
  -- use the same assoc rules for meta and non meta abstractions
  assoc e@(MAbstractoid _ _      ) = error "Internal error, associate applied to before deAbstractoid!"
  assoc e@(MAbstr       cases tp ) = assocCases cases >>= \ cs -> return $ MAbstr cs tp
  assoc e@(MMetaAbstr   _ _      ) = descendWithScope e 
  assoc e@(MLet         _ _ _ _ _) = descendWithScope e
  assoc e@(MLetGlobal   _ _ _ _ _) = descendWithScope e
  assoc e@(MLetShunt    _ _ _ _ _) = descendWithScope e
  assoc e                          = descendM assoc e  

  assocCases :: [(SPat,[(Maybe SExpr,SExpr)])] -> AssocMonad [(SPat,[(Maybe SExpr,SExpr)])]
  assocCases [] = return []
  assocCases ((pat,conds):cs) = do st <- get
                                   pat' <- assoc pat
                                   conds' <- assocConds conds 
                                   put st
                                   cs' <- assocCases cs
                                   return ((pat',conds'):cs')
  
  assocConds :: [(Maybe SExpr,SExpr)] -> AssocMonad [(Maybe SExpr,SExpr)]
  assocConds [] = return []
  assocConds ((Just cond, ex):ds) = do cond' <- assoc cond
                                       ex'   <- assoc ex
                                       ds'   <- assocConds ds
                                       return $ ((Just cond',ex'):ds')
  assocCands ((Nothing,ex):ds) = do ex'   <- assoc ex
                                    ds'   <- assocConds ds
                                    return $ ((Nothing,ex'):ds')