{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}

-- module Compile where

import System.Process
import Data.String

import Control.Monad.State.Lazy

import Control.Monad.Except
import qualified Data.Text.Lazy.IO as T  
import qualified Data.ByteString as DBS

import LLVM.AST.FunctionAttribute as FA
import LLVM.AST.Global
import LLVM.Prelude
import LLVM.Pretty  -- from the llvm-hs-pretty package
import LLVM.AST hiding (function,functionAttributes)
import qualified LLVM.AST as AST
import LLVM.AST.Type as AST
import qualified LLVM.AST.Float as F
import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.Operand as O


import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant
import LLVM.AST.Typed

import LLVM.Target
import LLVM.Context
import LLVM.CodeModel
import LLVM.Module as Mod


import LLVM.PassManager
import LLVM.Transforms
import qualified LLVM.Transforms as Tr
import LLVM.Analysis
import LLVM.AST.Linkage
import LLVM.AST.ParameterAttribute

inlineFunction
  :: MonadModuleBuilder m
  => Name  -- ^ Function name
  -> [(Type, ParameterName)]  -- ^ Parameter types and name suggestions
  -> Type  -- ^ Return type
  -> ([Operand] -> IRBuilderT m ())  -- ^ Function body builder
  -> m Operand
inlineFunction label argtys retty body = do
  let tys = fst <$> argtys
  (paramNames, blocks) <- runIRBuilderT emptyIRBuilder $ do
    paramNames <- forM argtys $ \(_, paramName) -> case paramName of
      NoParameterName -> fresh
      ParameterName p -> fresh `named` p
    body $ zipWith LocalReference tys paramNames
    return paramNames
  let
    def = GlobalDefinition functionDefaults
      { name        = label
      , parameters  = (zipWith (\ty nm -> Parameter ty nm []) tys paramNames, False)
      , returnType  = retty
      , basicBlocks = blocks
      , functionAttributes = [Right FA.AlwaysInline]
      }
    funty = ptr $ FunctionType retty (fst <$> argtys) False
  emitDefn def
  pure $ ConstantOperand $ C.GlobalReference funty label






vmStateArg :: [(Type,ParameterName)]
vmStateArg = [ (ptr i8,"asp"), (ptr i8, "ssp"),
               (ptr i8,"rsp"), (ptr i8, "csp") ]

vmStateRet = StructureType False (map fst vmStateArg)




vmCont -- vm continuation 
  :: MonadModuleBuilder m
  => Name  -- ^ Function name
  -> ([Operand] -> IRBuilderT m ())  -- ^ Function body builder
  -> m Operand
vmCont label body = do
  let tys = fst <$> vmStateArg
  (paramNames, blocks) <- runIRBuilderT emptyIRBuilder $ do
    paramNames <- forM vmStateArg $ \(_, paramName) -> case paramName of
      NoParameterName -> fresh
      ParameterName p -> fresh `named` p
    body $ zipWith LocalReference tys paramNames
    return paramNames
  let
    def = GlobalDefinition functionDefaults
      { name        = label
      , parameters  = (zipWith (\ty nm -> Parameter ty nm [NoAlias]) tys paramNames, False)
      , returnType  = vmStateRet
      , basicBlocks = blocks
      , functionAttributes = [Right FA.AlwaysInline]
      }
    funty = ptr $ FunctionType vmStateRet (fst <$> vmStateArg) False
  emitDefn def
  pure $ ConstantOperand $ C.GlobalReference funty label

 


globalConst
  :: MonadModuleBuilder m
  => Name       -- ^ Variable name
  -> Type       -- ^ Type
  -> C.Constant -- ^ Initializer
  -> m Operand
globalConst nm ty initVal = do
  emitDefn $ GlobalDefinition globalVariableDefaults
    { name                  = nm
    , LLVM.AST.Global.type' = ty
    , linkage               = Internal
    , initializer           = Just initVal
    , isConstant            = True
    }
  pure $ ConstantOperand $ C.GlobalReference (ptr ty) nm




data VmState = VmState { asp :: Operand
                       , ssp :: Operand
                       , rsp :: Operand
                       , csp :: Operand
                       }

type VmBuilder m a = StateT VmState m a                                       



-- The overwrite with undef properly activaetes dead store elemination
-- for prior vmPushArgs, however the stores remain in the llvm
-- but - reasonably so - do not emit any actual store instructions 

vmPopArg :: (MonadIRBuilder m, MonadModuleBuilder m)
             =>  Type -> VmBuilder m Operand

vmPopArg t = do st <- get
                asp1 <- bitcast (asp st) (ptr (ptr t)) `named` "asp"
                offset <- int64 (-1) 
                asp2 <- gep asp1 [offset] `named` "asp"
                asp3 <- bitcast asp2 (ptr i8) `named` "asp"
                val <- load asp2 0
                let udef = O.ConstantOperand (C.Undef (ptr t))
                store asp2 0 udef -- mark as undef fore dead store elimination
                put (st { asp = asp3 })
                return val


vmPushArg :: (MonadIRBuilder m, MonadModuleBuilder m)
             =>  Operand -> VmBuilder m ()
vmPushArg v = do st <- get
                 asp1 <- bitcast (asp st) (ptr (typeOf v)) `named` "asp"
                 store asp1 0 v 
                 offset <- int64 1
                 asp2 <- gep asp1 [offset] `named` "asp"
                 asp3 <- bitcast asp2 (ptr i8) `named` "asp"
                 put (st { asp = asp3 })                     
                 return ()

vmRet :: (MonadIRBuilder m, MonadModuleBuilder m)
             =>  VmBuilder m ()
vmRet =  mdo
      st <- get 
      v0 <- struct Nothing False [ C.Undef (ptr i8), C.Undef (ptr i8),
                                   C.Undef (ptr i8), C.Undef (ptr i8) ]

      v1 <- insertValue v0 (asp st) [0]
      v2 <- insertValue v1 (ssp st) [1]
      v3 <- insertValue v2 (rsp st) [2]
      v4 <- insertValue v3 (csp st) [3]
      ret v4




llcCmd = "llc-7 --x86-asm-syntax=intel "
asCmd  = "as"
clangCmd  = "clang-7" -- used for final linking 

-- TODO: Make a VmBuilder monad, conteining the vm state 

vmWord = i64


vmProc n stmts= vmCont n $ 
                   \ [asp,ssp,rsp,csp] -> mdo 
                        entry <- block `named` "entry"; do
                          evalStateT stmts (VmState asp ssp rsp csp)






vmCall :: MonadIRBuilder m => Name -> VmBuilder m ()
vmCall n = do 
   st <- get 
   let f = O.ConstantOperand (C.GlobalReference (ptr vmProcType) n)
   call f [(asp st,[NoAlias]),(ssp st,[NoAlias]),
           (rsp st,[NoAlias]),(csp st,[NoAlias])]
   return ()

vmProcType = FunctionType vmStateRet (map fst vmStateArg) False 

vmStackSize :: Integer 
vmStackSize = 50000000

vmMemType = (ArrayType (fromInteger (2*vmStackSize)) i8)


vmMain :: (MonadIRBuilder m, MonadModuleBuilder m)
             => VmBuilder m Operand
vmMain = mdo
   c0 <- global "c0" i64 (C.Int 64  0)
   c1 <- globalConst "c1" i64 (C.Int 64 21)
   c2 <- globalConst "c2" i64 (C.Int 64  2)
   vmPushArg c0 
   vmPushArg c1 
   vmPushArg c2
   vmCall "add_Word"
   v <- load c0 0 
   return v

mkMain =  mdo

  vmMem <- global "vmMem"  vmMemType (C.Undef vmMemType)

  globalStringPtr "Hello Work!\n" "str" 

  extern "puts" [ptr i8] i32

  vmProc "add_Word" $ mdo 
    arg2 <- vmPopArg vmWord  
    arg1 <- vmPopArg vmWord 
    arg0 <- vmPopArg vmWord
    v1   <- load arg1 0
    v2   <- load arg2 0 
    res <- add v1 v2
    store arg0 0 res
    vmRet  

  function "main" [] i64 $ \ [] -> mdo 
    entry <- block `named` "entry"; do
      beginOffset <- int64 0
      midOffset <- int64 vmStackSize
      endOffset <- int64 (2*vmStackSize)
      asp0 <- gep vmMem [beginOffset]
      ssp0 <- gep vmMem [midOffset]
      rsp0 <- gep vmMem [midOffset]
      csp0 <- gep vmMem [endOffset]
      asp <- bitcast asp0 (ptr i8) `named` "asp"
      ssp <- bitcast ssp0 (ptr i8) `named` "ssp"
      rsp <- bitcast rsp0 (ptr i8) `named` "rsp" 
      csp <- bitcast csp0 (ptr i8) `named` "csp"
      v <- evalStateT vmMain (VmState asp ssp rsp csp)
      ret v


{-
passes :: PassSetSpec
passes = defaultCuratedPassSetSpec 
           { optLevel = Just 3
           , useInlinerWithThreshold = Just 10000
           }
-}

passes :: PassSetSpec
passes = defaultPassSetSpec 
           { transforms = [  
--               CorrelatedValuePropagation,
--               DeadCodeElimination,
--               InductionVariableSimplify, 
--               InstructionCombining,
--               DemoteRegisterToMemory, 
--               EarlyCommonSubexpressionElimination,
--               SimplifyControlFlowGraph,
--               GlobalDeadCodeElimination,
--               ConstantMerge,
--               Reassociate,
--               ArgumentPromotion,
--               InterproceduralConstantPropagation,
--               InterproceduralSparseConditionalConstantPropagation,


               -- mandatory optimizations (order matters ! ...)
               DeadStoreElimination, -- required to set up analysis ! (but why) 
               ConstantPropagation,
               PromoteMemoryToRegister,
               TailCallElimination,
               Tr.FunctionAttributes, 
               FunctionInlining 10000,
               GlobalValueNumbering False, -- mandatory
               DeadStoreElimination  -- do actual elim
            ]
          }





optimize :: AST.Module -> IO AST.Module
optimize mod = do
  withContext $ \context ->
    withModuleFromAST context mod $ \m ->
      withPassManager passes $ \pm -> do
        runPassManager pm m
        optmod <- moduleAST m
        return optmod


simple :: IO ()
simple = do let mod = buildModule "testMain" $ mkMain
            optMod <- optimize mod 
--            T.putStrLn $ ppllvm $ mod
            T.putStrLn $ ppllvm $ optMod
             
 


-- TODO: Security, this is easy but permissions and races, been done wrong
--       elsewhere already 

llvmCompile :: String -- module name 
                -> ModuleBuilder a
                -> IO ()
llvmCompile name builder = do
  mod <- optimize $ buildModule (fromString name) $ builder
  let modStr = ppllvm mod   
      fileBase = "tmp/" ++ name
      llvmFile = fileBase ++ ".ll"
      asFile  = fileBase ++ ".s"
      objFile = fileBase ++ ".o"
  T.writeFile llvmFile modStr 
  callCommand (llcCmd    ++ " " ++ llvmFile)
  callCommand (asCmd     ++ " " ++ asFile ++ " -o " ++ objFile)
  callCommand (clangCmd  ++ " " ++ objFile  ++ " -o " ++ fileBase)

