# The most compact form of a factorial implementation
# expands with a lot of syntax sugar 
let fac : Word => Word
        = 0 -> 1 
        | x -> x * fac(x-1)

