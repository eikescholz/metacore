# This is the factorial after the name and shift all temporaries pass. 
# this is the representation feed into the translation to Reforth stage

let 
  fac : Word => Word
 where   
  fac = t0 -> ( t0 ? 0 -> 1 
                   | x -> let n0 : Word  # introduce n0,n1,t2 : Word syntax ?
                              t1 : Word => Word
                              t2 : Word
                              t3 : Word 
                              t4 : Word => Word
                            where 
                              n0 = 1
                              t1 = n0.infix-
                              t2 = t1 x 
                              t3 = fac t2
                              t4 = x.infix*
                            in t4 t2 )