# The fac1.mc example in with separated declare and bind blocks. 

let 
  fac : Word => Word
 where   
  fac = 0 -> 1 
      | x -> x * fac(x-1)