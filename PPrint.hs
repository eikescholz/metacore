{- A family pack of pretty printers for debugging in most cases ... -}
{-# LANGUAGE FlexibleInstances #-}


module PPrint where
import MetaCore
import Core
import Text.PrettyPrint
import qualified Text.PrettyPrint as PP
import qualified IdentMap as IM
import Position

pprintErr e = error $ "pprint for " ++ show e ++ "not implemented"


class PPrint a where
  pprint :: a -> Doc

tuply :: (a -> Doc) -> [a] -> Doc
tuply pp es = parens (hcat (punctuate comma (map pp es)))

pshow :: PPrint a => a -> String
pshow x = show (pprint x)

instance PPrint Pos where
  pprint p = text (strfy p)

instance PPrint IM.UIdent where
  pprint i = text $ IM.showUIdent i

instance PPrint IM.ValueIdent where
  pprint (IM.Ident  i) = text $ IM.showIdent i
  pprint (IM.UIdent i) = text $ IM.showUIdent i

-- Metacore prettyprinting (incomplete)
instance PPrint MExpr where
  pprint (MCon _ s _) = text $ show s
  pprint (MVar _ s _) = text $ show s
  pprint (MMetaVar _ s) = text $ show s
  pprint (MMetaCon _ s) = text $ show s
  pprint (MApp a b _) = pprint a <+> pprint b
  pprint (MAbstractoid a b) = pprint a <+> text "-> " <+> pprint b
  pprint (MMetaAbstr a b) = pprint a <+> text "=> " <+> pprint b
  pprint (MTuple _ es) = tuply pprint es
  pprint e = pprintErr e

pprintArg (str,ty) = text str <+> text ":" <+> pprint ty

instance PPrint Decl where
  pprint (Decl _ s t _) = text s <+> text ":" <+> pprint t
--  pprint e = pprintErr e

instance PPrint Bind where
  pprint (Bind pat e _) = pprint pat <+> text "=" <+> pprint e
--  pprint e = pprintErr e


instance PPrint Pat where
  pprint (PatVar s _) = text $ IM.showUIdent s
  pprint (PatCon s _) = text $ IM.showUIdent s
  pprint (PatApp a b _) = pprint a <+> pprint b
  pprint (PatWord w _) = text (show w)
  pprint e = pprintErr e


pprintGuardedCases :: [(Maybe Expr, Expr)] -> Doc
pprintGuardedCases cs = vcat (map pgc cs)
  where
    pgc :: (Maybe Expr,Expr) -> Doc
    pgc (Just g,c)  = text "&" <+> pprint g <+> text "->" <+> pprint c 
    pgc (Nothing,c) = text "->" <+> pprint c 


pprintCasePats :: ( [Pat] , [(Maybe Expr,Expr )]) -> Doc 
pprintCasePats (pats,_) = hcat (map pprint pats) 

pprintCases :: [( [Pat] , [(Maybe Expr,Expr )])] -> Doc 
pprintCases (c:cs) = text "?" <+> pprintCasePats c $$ (vcat (map ppc (c:cs) ))
 where
   ppc :: ( [Pat] , [(Maybe Expr,Expr )]) -> Doc 
   ppc (_,c) = text "|" <+> pprintGuardedCases c 


instance PPrint Expr where
  pprint (Let r ds wds bs e _) 
    =   text "let@" PP.<> text r <+> vcat (map pprint ds)
                        $$ text " where " <+> vcat (map pprint wds)
                                          <+> vcat (map pprint bs)
                        $$ text " in" <+> pprint e
  pprint (App a b _) = pprint a <+> pprint b
  pprint (Tuple es _) = tuply pprint es  

  pprint (Match e (Cases cs _) _) = parens ( pprint e <+> pprintCases cs)
  pprint (Word i _) = text (show i)
  pprint (Var s _) = text $ IM.showUIdent s
  pprint (Con s _) = text $ IM.showUIdent s
  pprint (Abstr s t r e _) = ( parens (text (IM.showUIdent s) <+> text ":" 
                                <+> pprint t) 
                                PP.<> text "@" PP.<> text r ) <+> text "->" 
                                <+> pprint e
  pprint (Unit _) = text "()"
  pprint e = pprintErr e