{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveDataTypeable #-}

{- 
  This module contains handling routines for mapping 
  identifiers to unique identifiers with 
  efficient aliasing support
  etc.

  All identifiers map to one exact entity that can also
  be identified by a full module path and src line coloumn numbers. 

  We assume that the mapping from identifiers and global identifiers 
  is thus stored in a state using a simple data.map with log(n) lookup time.  
  
-}

module IdentMap where
import Data.Word
import Data.List
import qualified Data.Map as Map
import Position
import Control.Monad.State.Lazy
import Text.PrettyPrint 
import Data.Data

type Column = Int
type Line = Int

type Ident = [String] -- Identifier
type LinkerIdent = Ident -- Identifier with full exact module path. 
type ACUid = Word -- Alpha conversion fresh uid, before first alpha conversion its 0, the cheap way to avoid name capture in supstitutions. 
type UIdent = (LinkerIdent,Line,Column,ACUid) -- If ambigous module exports the last declared names as LinkerName 
   
  
type IdentMap a = Map.Map UIdent a

data ValueIdent = Ident Ident 
                | UIdent UIdent
  deriving (Show,Eq,Ord,Data,Typeable) 

class MonadState s m => HasIdentState s m where
  getUIdentMap :: m (Map.Map Ident UIdent)
  putUIdentMap :: Map.Map Ident UIdent -> m ()  

lookupUIdent :: HasIdentState s m => Ident -> m (Maybe UIdent)
lookupUIdent i = do
  im <- getUIdentMap 
  return $ Map.lookup i im

uIdentFromValueIdent :: HasIdentState s m => ValueIdent -> m (Maybe UIdent)
uIdentFromValueIdent (Ident i)  = lookupUIdent i
uIdentFromValueIdent (UIdent i) = return $ Just i


insertUIdent :: HasIdentState s m => Ident -> UIdent -> m () 
insertUIdent i ui = do
  im <- getUIdentMap
  putUIdentMap (Map.insert i ui im)
  
lookup :: HasIdentState s m => Ident -> IdentMap a -> m (Maybe a)
lookup i m = do
  im <- getUIdentMap 
  case  Map.lookup i im of
    Nothing   -> return Nothing
    (Just i') -> return $ Map.lookup i' m
            
mkUIdent :: [SubStr] -> UIdent
mkUIdent ss = ( map snd ss
              , getLn  (pos (last (map fst ss)))
              , getCol (pos (last (map fst ss)))
              , 0 ) 

showIdent :: Ident -> String
showIdent is = concat (intersperse "::" (map show is))

showUIdent :: UIdent -> String
showUIdent (i,l,c,_) = showIdent i ++ ":" ++ show l ++":" ++ show c

showValueIdent :: ValueIdent -> String
showValueIdent (Ident  i)  = showIdent i
showValueIdent (UIdent i) = showUIdent i
  
substrToIdent :: SubStr -> ValueIdent
substrToIdent sstr = Ident $ [snd sstr]   

subStrsToIdent :: [SubStr] -> ValueIdent
subStrsToIdent ss = Ident $ map snd ss



-- TODO: Remove - Deprecated by the associator step
-- a little lookup helper
--withUIdentOf :: HasIdentState s m 
--             => ValueIdent 
--             -> (UIdent -> m (Either Doc  ()))
--             -> m (Either Doc ())
--withUIdentOf i f = do
--  mi <- uIdentFromValueIdent i
--  case mi of 
--    Nothing -> return $ Left $ text (showValueIdent i ++ " undeclared")
--    Just i' -> f i'