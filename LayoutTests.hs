module LayoutTests where
import Position
import Scanner
import Layout
import Control.Monad.Trans.State.Lazy
import Scanner 
import TestBase
import Data.List

layoutScan str = do
   let ls  = scan (Pos (1,1,"")) str
   let lls = layout ls
   return lls -- tails and init remove layout data

layoutsTo x y = do 
  a <- x
  b <- y 
  if (a == b)
    then return TestSuccess
    else return (TestFailure ("Condition `layoutsTo` failed!"
                                      ++ "\n\n LHS: [ "++pp a 
                                      ++ "\n\n RHS: [ "++pp b  ))
 where
   pp ts = concat (intersperse ",\n        " (map show ts)) ++ "]\n"


layoutTests = [
  Describe "Layout"[ 
    It "inserts block scopes for \"let\""
      (layoutScan ( "let x = y\n" ++ 
                    "    z = w\n"    )
           `layoutsTo`

       return [ KeywordTok (Pos (1,1,""),"let"),
                SymTok (Pos (1,1,""),"("),
                VarTok (Pos (1,5,""),"x"),
                SymTok (Pos (1,7,""),"="),
                VarTok (Pos (1,9,""),"y"),
                SymTok (Pos (2,5,""),";"),
                VarTok (Pos (2,5,""),"z"),
                SymTok (Pos (2,7,""),"="),
                VarTok (Pos (2,9,""),"w"),
                SymTok (Pos (0,0,""),")")] ),

    It "inserts block scopes for \"do\""
       (layoutScan "do x := y" `layoutsTo` 
         return [ KeywordTok (Pos (1,1,""),"do"),
                  SymTok (Pos (1,1,""),"("),
                  VarTok (Pos (1,4,""),"x"),
                  SymTok (Pos (1,6,""),":="),
                  VarTok (Pos (1,9,""),"y"),
                  SymTok (Pos (0,0,""),")")] )
 
  ]
 ]  
 

