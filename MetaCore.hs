{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{- 

MetaCore and its values are given by a generalized form of lisp like syntax
expressions, that contain a superset of the parseable AST and encodes all
values. 

The SExpr is the "value type" of the meta-processor language, that is 
actually an untyped lambda calculus variation - like the C++ template 
processor, only in this case directly designed as such.
Rename to MetaVal ? ... also they are literally syntax expressions
so sexpr would be fine too. 

-}

module MetaCore where
import Data.Char
import Scanner 
import Position
import qualified IdentMap as IM
import qualified Data.Map as Map
import qualified Data.Set as Set 
import Control.Monad.Trans.State.Lazy
import Data.Data
import Data.Generics.Uniplate.Data
import Control.Monad.Except
import Text.PrettyPrint
import CoreNumbers


data ModuleHeader = Import [SubStr] ModuleHeader
                  | Precs AssocSpecs
  deriving (Show,Eq) 


data AssocSpecs = Prec Assoc [String] AssocSpecs
                | NoSpec
  deriving (Show,Eq) 


type SType = SExpr -- for clarity
type SBind = SExpr
type SDecl = SExpr
type SPat  = SExpr

type SReg  = (SubStr,[(SubStr,SubStr)]) 


-- the UIdent are used for internal processing during the meta and
-- type-checking phase. 


-- The parser yields symbolic expressions of the type
type PExpr = SExpr 

-- After the association stage all names have a unique global assignement, 
-- using the full module path and row and col of the declaration. 
-- and yield 

type MExpr = SExpr 
type MType = SExpr 
type MPat  = SType 
type MReg  = SReg 
type MDecl = SExpr
type MBind = SExpr


data Typing = UnTy  
            | OfTy MType -- Place for meta eval type results. 
  deriving (Show,Eq,Data,Typeable) 
-- TODO: The (Maybe MExpr i r)) parts in the let* are unused. 
-- where the symbolic expressions are bit structurally richer then 
-- in lisp: 

data SExpr = -- Core Value Yielding Expressions, 
             MVar Pos IM.ValueIdent Typing-- foo
           | MCon Pos IM.ValueIdent Typing-- Foo
           | MArray Pos [SExpr] Typing-- {a,b,c ...} 
           | MCompr  Pos SExpr [(SPat,SExpr)] (Maybe SExpr) Typing
           | MDo [SExpr] Typing            
           | MAt SExpr SExpr Typing --foo[Bar]
           | MApp SExpr SExpr Typing               -- foo bar
           | MAbstr [(SPat,[(Maybe SExpr,SExpr)])] Typing -- desugared Abstractoid
           
           | MMatches Pos SExpr SExpr Typing -- foo ? barAbstraction
           | MRefCon Pos [SExpr]  Typing -- [exists]expr
           | MAtMethod Pos SExpr String Typing 

           -- with typings directly derivable from subexpressions
           | MTuple Pos [SExpr] -- non suffix (a,b, ... ) 
           | MAtRegion Pos SExpr SReg
           | MAtShuntReg SReg SExpr 
           | MCollectAt (Maybe SReg) SExpr 
           | MShuntCollectAt (Maybe SReg) SExpr
           | MShunt Pos SExpr
           | MNum Pos CoreNumber 

           -- Unsorted Other   
           -- Let ...                              where     in
           | MLet Pos (Maybe SReg)       [SDecl] [SBind] (Maybe SExpr) 
           | MLetGlobal Pos (Maybe SReg) [SDecl] [SBind] (Maybe SExpr) 
           | MLetShunt Pos (Maybe SReg)  [SDecl] [SBind] (Maybe SExpr) 
           
           -- fixed typed expressions
           | MAnd SExpr SExpr -- a && b
           | MOr  SExpr SExpr -- a || b
           
           -- syntax/meta expressions 
           | MConj SExpr SExpr -- a & b
           | MAlt SExpr SExpr -- a | b
           | MMetaAbstr SPat SExpr        
           | MMetaVar Pos IM.ValueIdent -- e.g $-prefixed variabels $x, $y
           | MMetaCon Pos IM.ValueIdent -- $Foo -- pattern abbreviations/macros, 
           | MInterfaceVal Pos [IM.ValueIdent] -- a nameless interface
           | MWhere Pos SExpr [SBind]
           | MDeclVar SExpr SType    -- bar : Foo
           | MBindVar SExpr SExpr    -- bar = bla
           | MDefVar SExpr SType SExpr -- bar : Foo = bla   (syntax sugar)
           | MDeclMeth SExpr SType              
           | MBindMeth SExpr SExpr
           | MDefMeth SExpr SType SExpr -- x.bar : Foo = bla   (syntax sugar)
           | MOfType Pos SExpr SExpr 
           | MMetaEval SExpr -- $(...) needed ? ($f foo) meta evals already 
          
           | MAbstractoid SPat SExpr   -- from parser
            
           | MInterface Pos SType [SExpr] 
           | MInstance Pos SType [SExpr]
           
           | MStruct Pos SExpr [SExpr] 
           | MUnion Pos SExpr [SExpr]
   deriving (Show,Data,Typeable) 

-- equality ignores, attribute data like positions and
-- make 
instance Eq SExpr where
  (==) (MNum _ n) (MNum _ n') = n == n'
  (==) a b = if toConstr a == toConstr b  
               then let as  = children a
                        bs  = children b
                        rs  = childrenBi a :: [SReg]
                        rs' = childrenBi b :: [SReg]
                        is  = childrenBi a :: [IM.ValueIdent]
                        is' = childrenBi b :: [IM.ValueIdent]
                        eq0 = foldr (&&) True (map (\(x,y)->x==y) (zip as bs))
                        eq1 = foldr (&&) True (map (\(x,y)->x==y) (zip rs rs'))
                        eq2 = foldr (&&) True (map (\(x,y)->x==y) (zip is is'))
                     in if length as == length bs
                             && length rs == length rs'
                             && length is == length is'   
                          then eq0 && eq1 && eq2
                          else False
               else False      
    
rv = reverse 
mkAtInfixMeth :: SExpr -> SubStr -> SExpr
mkAtInfixMeth a i = MAtMethod (fst i) a (snd (toInfix i)) UnTy
toPrefix :: (a, [Char]) -> (a, [Char])
toPrefix (p,s) = (p,".prefix"++s)
toInfix :: (a, [Char]) -> (a, [Char])
toInfix  (p,s) = (p,".infix"++s)
toPostfix (p,s) = (p,".postfix"++s)

-- Interface 
data InterfaceDesc = InterfaceDesc 
  { interfaceName   :: IM.ValueIdent
  , interfaceTyVar  :: IM.ValueIdent
  , subInterfaces   :: Set.Set IM.ValueIdent
  , inerfaceMethods :: Map.Map IM.ValueIdent MType 
  } 


-- the meta translation state/scope, initially defined here to avoid cyclic includes
-- which seems not to be implemented. 
-- This will also collect runtime-type information on the meta stage. 

-- Identifiers are bound to values that have a type called "coreType" to
-- avoid clashes with Haskells "type" keyword

data MTS = MTS { coreTypes :: Map.Map IM.ValueIdent MType  
               , metaVals :: Map.Map IM.ValueIdent [MExpr] -- list of "shadowed" bindings for meta overloading/SFINAE
               -- predicates contain subtype data
               -- result is a type abstraction  
               , interfaces :: Map.Map IM.ValueIdent InterfaceDesc
               , interfaceInstanceTypes :: Map.Map IM.ValueIdent (Set.Set MType)
               , lastAlphaConUID :: Word
               }

-- the meta translator (monad) 
-- added underlying either monad for error handling 
type MT = State MTS

data MetaFailure = MSF (MPat,MExpr) -- Meta substition failure 
                 | MErr Doc         -- Some actuall error

type EMT = ExceptT MetaFailure MT


getInterfacesInScope :: EMT (Map.Map IM.ValueIdent InterfaceDesc)
getInterfacesInScope = do 
  st <- lift $ get
  return $ interfaces st

--instance HasPos SExpr where


getFreshUIdentOf :: IM.UIdent -> MT IM.UIdent
getFreshUIdentOf (n,l,c,u) = do
  st <- get
  let u' = lastAlphaConUID st
  put $ st { lastAlphaConUID = u'+1 }
  return (n,l,c,u'+1)

instance HasPos SExpr where
  pos = genericPos

instance HasPos IM.ValueIdent where
  pos = genericPos

 


-- left biased descend
-- if both arguments have the same
-- constructor descend with given function
-- then apply the given function to the results
-- else just apply the given functions to the results. 
-- Is there a generic form to do that ?
--
-- TODO: Implemention is incomplete, especially for 
-- constructors that might change 

biDescendWith :: Monad m => (MExpr -> MExpr -> m MExpr )
                         -> MExpr 
                         -> MExpr 
                         -> m MExpr

biDescendWith f (MArray p0 es0 tp0) (MArray p1 es1 pt1) = do
  es <- mapM (\(x,y)-> f x y) (zip es0 es1)
  return $ MArray p0 es tp0

biDescendWith f (MDo es0 tp0) (MDo es1 pt1) = do
  es <- mapM (\(x,y)-> f x y) (zip es0 es1)  
  return $ MDo es tp0

biDescendWith f (MAt p0 e0 tp0) (MAt p1 e1 pt1) = do
  e <- f e0 e1
  return $ MAt p0 e tp0

biDescendWith f (MApp d0 e0 tp0) (MAt d1 e1 pt1) = do
  d <- f d0 d1
  e <- f e0 e1
  return $ MApp d e tp0

biDescendWith f (MTuple p0 es0) (MTuple p1 es1) = do
  es <- mapM (uncurry f) (zip es0 es1)
  return $ MTuple p0 es

biDescendWith f a b = if toConstr a == toConstr b 
                        then if children a /= ([] :: [SExpr]) 
                                  || children b /= ([] :: [SExpr])
                                then error "INTERNAL ERROR: Missing biDescewndWith Case"
                                else f a b
                        else f a b


