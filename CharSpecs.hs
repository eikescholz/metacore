module CharSpecs where
import Data.Char

isSep c = c `elem` "(){}[],; " -- separators

isEndSep c = c `elem` ")}],; "  
isStartSep c = c `elem` "({ "  

-- isSepStr (c:[]) = isSep c
-- isSepStr _      = False

isSym c = isPrint c && not (isSep c) && not (isAlphaNum c) && not (isSpace c)

isNameChar c = isAlphaNum c || c == '_'

