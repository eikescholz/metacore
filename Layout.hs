module Layout where
import Scanner
import Position

-- the layout is done by inserting additional tokens
-- Blocks are given by layout, if not they are 
-- a list of statement e.g. ( stmt0; stmt1; stmt2 ) where ; is right assoc
---further blocks are introduced by a keyword that
-- may be added to the lexer like fixities (TODO:)
-- So that macros processing blocks can easily be written, using
-- ; and ;; in macro pattern matches. 


block = ["do","let","where","in"]

layout ls = case layoutAt [1] ls of
              (SymTok (_,";"):ls) -> ls 
              ls                  -> ls

testLayout = layout

layoutAt :: [Int] -> [Token] -> [Token]

-- close layout by indention rule
layoutAt (c:cs) (l:ls) | col l <  c 
  = (SymTok (pos l,")"):layoutAt cs (l:ls))  

-- suppress layout by user supplied '(' rule
layoutAt cs (KeywordTok (p,kw):SymTok (p',"(") :ls) | kw `elem` block
  =  KeywordTok (p,kw) : SymTok (p',"(") : layoutAt cs ls

-- add new layout block rule
layoutAt (c:cs) (KeywordTok (p,kw):l:ls) | kw `elem` block
  = KeywordTok (p,kw) : SymTok (p,"(") : l : layoutAt (col l:c:cs) ls 

-- inset layout separators rule 
layoutAt (c:cs) (l:ls) | col l == c 
  = (SymTok (pos l,";"):l:layoutAt (c:cs) ls) 

-- investigate next token if no rule applies
layoutAt (c:cs) (l:ls) 
  = (l:layoutAt (c:cs) ls)

-- close open layouts blocks at end of token stream   
layoutAt (c:c':cs) []
  = (SymTok (eofPos,")"):layoutAt (c':cs) [])

-- toplevel is not closed with ')'
layoutAt [c] [] 
  = []


