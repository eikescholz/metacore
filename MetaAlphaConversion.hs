module MetaAlphaConversion where
import MetaCore
import qualified Data.Map as Map
import qualified Data.Set as Set 
import qualified IdentMap as IM
import Position
import Control.Monad.Trans.State.Lazy
import IdentMap
import Data.Generics.Uniplate.Operations
import Control.Monad.Trans.Class

-- This module implements alpha conversion for type expressions,
-- i.e. only meta variables are conversed. Macro matching would not
-- work as expected otherwise. 

-- General alpha conversion might not be needed
-- at the moment over implemented over meta abstractions.  
-- complete alpha conversion must track each type of internally
-- 
-- at the moment alpha conversion is needed to decide 
-- commutability of predicates, its just implemented
-- for meta expression that can appear in types.    
-- 

-- Fresh Alpha Conversion Monad
type FACM = StateT (Map.Map IM.UIdent IM.UIdent) MT                 


-- for fresh alpha conversion, just needs a set of the idents bound 
-- to increase the fresh variable counter. 
-- needs to converse everything not only meta vars, 
-- for computation it needs to conv


freshAlphaConversionOf :: MType -> FACM MType
freshAlphaConversionOf (MAbstractoid pat ex) = error "deAbstractoid not applied"
freshAlphaConversionOf (MMetaAbstr pat ex)   = lamcon MMetaAbstr pat ex
freshAlphaConversionOf (MAbstr es tp)        = 
  do es' <- casesConversion es
     tp' <- case tp of
               UnTy    -> return tp 
               OfTy ty -> do ty' <- freshAlphaConversionOf ty -- may not be required, but for completeness
                             return (OfTy ty') 
     return $ MAbstr es' tp'
 
freshAlphaConversionOf (MVar     p i tp) = return $ MVar p i tp 
freshAlphaConversionOf (MCon     p i tp) = return $ MCon p i tp  
freshAlphaConversionOf (MMetaVar p i ) = updateIdent (MMetaVar p) i 
freshAlphaConversionOf (MMetaCon p i ) = updateIdent (MMetaCon p) i 

freshAlphaConversionOf x@(MLet       p reg ds ws es) = letcon (MLet       p reg) ds ws es
freshAlphaConversionOf x@(MLetGlobal p reg ds ws es) = letcon (MLetGlobal p reg) ds ws es
freshAlphaConversionOf x@(MLetShunt  p reg ds ws es) = letcon (MLetShunt  p reg) ds ws es  

freshAlphaConversionOf e                             = descendM freshAlphaConversionOf e 

casesConversion :: [(SPat,[(Maybe SExpr,SExpr)])] -> FACM [(SPat,[(Maybe SExpr,SExpr)])]
casesConversion [] = return []
casesConversion ((pat,ds):es) = do
  st   <- get
  pat' <- freshAlphaPatConversionOf pat
  ds'  <- freshCondConversion ds
  put st
  es' <- casesConversion es
  return ((pat',ds'):es')

freshCondConversion [] = return []
freshCondConversion ((Just cond, ex):ds) = do cond' <- freshAlphaConversionOf cond
                                              ex'   <- freshAlphaConversionOf ex
                                              ds'   <- freshCondConversion ds
                                              return ((Just cond',ex'):ds')

freshCondConversion ((Nothing, ex):ds) = do ex'   <- freshAlphaConversionOf ex
                                            ds'   <- freshCondConversion ds
                                            return ((Nothing,ex'):ds')



-- Utility functions
lamcon con pat ex = do 
  st <- get
  pat' <- freshAlphaPatConversionOf pat
  ex'  <- freshAlphaConversionOf ex
  put st 
  return $ con pat ex

letcon :: ([SDecl] -> [SBind] -> Maybe SExpr -> MExpr)
           -> [SDecl] -> [SBind] -> Maybe SExpr -> FACM MExpr
letcon con ds ws es = do
   st   <- get
   ds'  <- mapM freshAlphaPatConversionOf ds
   ws'  <- mapM freshAlphaPatConversionOf ws -- convert pattern parts
   ds'' <- mapM freshAlphaConversionOf ds' 
   ws'' <- mapM freshAlphaConversionOf ws'     -- convert non-pattern parts
   es'' <- mapM freshAlphaConversionOf    es
   put st
   return $ con ws'' ds'' es''

updateIdent :: (IM.ValueIdent ->  MExpr) -> IM.ValueIdent -> FACM MExpr 
updateIdent con (IM.UIdent i) = do
  scope <- get
  let i'' = case Map.lookup i scope of
              Just i' -> i'
              Nothing -> i
  return $ con (IM.UIdent i'') 


freshAlphaPatConversionOf :: MType -> FACM MType 
    
freshAlphaPatConversionOf (MVar     p i tp) = return $ MVar p i tp 
freshAlphaPatConversionOf (MCon     p i tp) = return $ MCon p i tp 
freshAlphaPatConversionOf (MMetaVar p i )   = updatePatIdent (MMetaVar p) i 
freshAlphaPatConversionOf (MMetaCon p i )   = updatePatIdent (MMetaCon p) i 
    
freshAlphaPatConversionOf (MDeclVar pat ty)          = do pat' <- freshAlphaPatConversionOf pat                                                             
                                                          return $ MDeclVar pat' ty
    
freshAlphaPatConversionOf (MBindVar pat ex)          = do pat' <- freshAlphaPatConversionOf pat 
                                                          return $ MDeclVar pat' ex
    
freshAlphaPatConversionOf (MDefVar pat ty ex)        = do pat' <- freshAlphaPatConversionOf pat 
                                                          ty' <- freshAlphaConversionOf ty
                                                          ex' <- freshAlphaConversionOf ex
                                                          return $ MDefVar pat' ty' ex'
    
freshAlphaPatConversionOf e                          = descendM freshAlphaPatConversionOf e

updatePatIdent :: (IM.ValueIdent -> MExpr) -> IM.ValueIdent -> FACM MExpr    
updatePatIdent con (IM.UIdent i) = do scope <- get
                                      i'' <- case Map.lookup i scope of
                                                  Just i' -> return i'
                                                  Nothing -> lift $ getFreshUIdentOf i
                                      put (Map.insert i i'' scope)
                                      return $ con (IM.UIdent i'') 


-- create a normal form, by replacing pattern variables with 
-- with respect to meta abstraction. 
-- a predefined name set, (that can not be entered by the programmer) 

alphaConversionToNF :: MExpr -> MExpr
alphaConversionToNF e = evalState (toNF e) (defaultIdents,Map.empty)
  where
    defaultIdents :: [IM.ValueIdent]
    defaultIdents = map (\s->UIdent ([s],0,0,0)) [ '$':'t':show i | i <- [0 ..] ]

    toNF :: MExpr -> State ([IM.ValueIdent],Map.Map IM.ValueIdent IM.ValueIdent) MExpr
    toNF (MMetaAbstr pat res) = do (_,m) <- get
                                   pat' <- patToNF pat
                                   res' <- toNF res
                                   (is,_) <- get
                                   put (is,m)
                                   return $ MMetaAbstr pat' res'
    toNF e@(MMetaVar p i) = do 
      (is,m) <- get
      case Map.lookup i m of
        Just i' -> return $ MMetaVar p i' 
        Nothing -> return $ MMetaVar p i  

    toNF e = descendM toNF e 
   
    patToNF :: MExpr -> State ([IM.ValueIdent],Map.Map IM.ValueIdent IM.ValueIdent) MExpr
    patToNF e@(MMetaVar p i) = do 
      (is,m) <- get
      case Map.lookup i m of
        Just i' -> return $ MMetaVar p i' 
        Nothing -> do put $ (tail is,Map.insert i (head is) m)
                      patToNF e

    patToNF e = descendM patToNF e                   


isAlphaConvertibleTo :: MType -> MType -> Bool
isAlphaConvertibleTo a b = alphaConversionToNF a == alphaConversionToNF b

