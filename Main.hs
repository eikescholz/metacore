import System.Environment
import Debug.Trace
import Scanner
import Layout
import Data.Char
import Data.Int
import Control.Applicative
import Position
import Data.Word
import Scanner
import RunTests
import Lexer
import Grammar
import Data.List
import MetaMatchReduce
--import MetaTranslate
import PPrint
import Text.PrettyPrint
import Associate

show' xs = "[\n" ++ concat (intersperse ",\n" (map show xs) ) ++ "]\n" 

main = do args <- getArgs
          case args of 
            []         -> error "Error: Expecting an argument\n"
            ["--test"] -> runAllTests
            (a:_)      -> do 
                    str <- readFile (args!!0)
                    let ls  = scan (Pos (1,1,args!!0)) str
                    let lls = layout ls
                    putStrLn ( "Lexems: " ++ show lls ++ "\n\n")
                    mods <-lexModStr (Pos (1,1,args!!0), str) [["Main"]] []
                    let (_,ts) = head mods
                    putStrLn ( "ts: " ++ show' ts ++"\n\n")
                    let (_,es) = parseModule ts
                    putStrLn ( "MExpr: "++ show es )
                    --let cs = readExprs es
                    --putStrLn ( "Expr: \n\n"++ show (vcat (map pprint cs) ))
  
{-
                    case doBlock ts of
                      Right (s,ts') -> do 
                        putStrLn s
                        putStrLn ("\n Remaining tokens: "++ show ts')
                      Left (s,ts')  -> do 
                        putStrLn ( "Lexems: " ++ show lls ++ 
                                   "\n  Tokens: "  ++ show (reverse ts) ++ 
                                   "\n  Parsed: " ++ show s ++ 
                                   "\n  Remaining Tokens " ++ show ts' )
-}
