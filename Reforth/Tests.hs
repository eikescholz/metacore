module Tests where

import System.Exit

import qualified MicroCodes as MC
import AST
import LLVMUtils
import Compiler
import BuiltIns
import CodeGen

data RfCompTest = RfCompTest { testName :: String
                             , expectedExitCode :: Int
                             , expectedStdout :: String
                             , testModule :: RfModule
                             } 

tests = [
  
  RfCompTest "Scope begin/end instruction test" 255 "" 
     [ DefWord "$23" 23
      , DefAbstr "mcmain"
         [ BeginScope 2
         , EndScope 2
         , Yield
         ]
     ]
   
  ,RfCompTest "Copy and bindDst instructions test" 23 "" 
     [ DefWord "$23" 23
     
     , DefAbstr "mcmain" 
        [ BeginScope 2
        , BindDst 
        , Bind 1 (Word "$23") 
        , Copy 0 1
        , EndScope 2
        , Yield 
        ]
     ]
  
  ,RfCompTest "AllocResult instruction test" 23 "" 
     [ DefWord "$23" 23
     , DefAbstr "mcmain" 
        [ BeginScope 3
        , BindDst 
        , Bind 1 (Word "$23") 
        , BindResultAlloc 2 (TypeConst "Word")
        , Copy 2 1
        , Copy 0 2
        , DeallocResult (TypeConst "Word")
        , EndScope 3
        , Yield
        ]
     ]
  
  ,RfCompTest "Apply and eval instructions test" 23 "" 
     [ DefWord "$23" 23
     
     , DefAbstr "mcmain" 
        [ BeginScope 3
        , BindDst            
        , Bind 1 (Abstr "ret23")
        , Bind 2 Unit
        , Apply 0 1 2 
        , Eval
        , EndScope 3
        , Yield
        ]
     
     , DefAbstr "ret23" 
        [ BeginScope 2 
        , BindDst            
        , Bind 1 (Word "$23")
        , Copy 0 1
        , EndScope 2
        , Yield
        ]
     ]
  
  ,RfCompTest "BindArgs 1 instruction test" 23 "" 
     [ DefWord "$23" 23
     
     , DefAbstr "mcmain" 
        [ BeginScope 3
        , BindDst            
        , Bind 1 (Abstr "id")
        , Bind 2 (Word "$23")
        , Apply 0 1 2 
        , Eval
        , EndScope 3
        , Yield
        ]
     
     , DefAbstr "id" 
        [ BeginScope 2 
        , BindDst
        , BindArgs 1            
        , Copy 0 1
        , EndScope 2
        , Yield
        ]
     ] 
  
  ,RfCompTest "Suspend instruction test 1" 23 "" 
     [ DefWord "$23" 23
     
     , DefAbstr "mcmain" 
        [ BeginScope 4
        , BindDst            
        , Bind 1 Unit
        , Bind 2 (Abstr "suspendedRet23")
        , BindResultAlloc 3 (TypeConst "Abstr")  
        , Apply 3 2 1  
        , Eval
        , Apply 0 3 1
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , EndScope 4
        , Yield
        ]  
     
     , DefAbstr "suspendedRet23" 
        [ Suspend 
        , BeginScope 2 
        , BindDst            
        , Bind 1 (Word "$23")
        , Copy 0 1
        , EndScope 2
        , Yield 
        ]
     ] 
  
  ,RfCompTest "Suspend instruction test 2" 23 "" 
     [ DefWord "$23" 23
     
     , DefAbstr "mcmain" 
        [ BeginScope 5
        , BindDst            
        , Bind 1 Unit
        , Bind 2 (Abstr "twiceSuspendedRet23")
        , BindResultAlloc 3 (TypeConst "Abstr")  
        , BindResultAlloc 4 (TypeConst "Abstr")  
        , Apply 3 2 1  
        , Eval
        , Apply 4 3 1 
        , Eval 
        , Apply 0 4 1
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , EndScope 5
        , Yield 
        ]
     
     , DefAbstr "twiceSuspendedRet23"
         [ Suspend 
         , Suspend  
         , BeginScope 2 
         , BindDst            
         , Bind 1 (Word "$23")
         , Copy 0 1
         , EndScope 2
         , Yield
         ]
     ]
  
  ,RfCompTest "BindArgs instruction test 1" 23 "" 
     [ DefWord "$23" 23
     
     , DefWord "$19" 19
     
     , DefWord "$4" 24
     
     
     , DefAbstr "mcmain" 
        [ BeginScope 7
        , BindDst            
        , Bind 1 (Word "$23")
        , Bind 2 (Word "$19")
        , Bind 3 (Word "$4") 
        , Bind 4 (Abstr "first")
        , BindResultAlloc 5 (TypeConst "Abstr")
        , BindResultAlloc 6 (TypeConst "Abstr")
        , Apply 5 4 1  -- $6 := first 23
        , Eval
        , Apply 6 5 2 -- $0 := (first 23) 19
        , Eval
        , Apply 0 6 3 -- $0 := ((first 23) 19) 24
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , DeallocResult (TypeConst "Abstr")
        , EndScope 7
        , Yield
        ]   
     
     , DefAbstr "first" 
        [ Suspend 
        , Suspend 
        , BeginScope 4
        , BindDst
        , BindArgs 3      
        , Copy 0 1 
        , EndScope 4
        , Yield 
        ]
     ]
     
  ,RfCompTest "BindArgs instruction test 2" 19 "" 
     [ DefWord "$23" 23
     
     , DefWord "$19" 19
     
     , DefWord "$4" 24
     
     
     , DefAbstr "mcmain" 
        [ BeginScope 7
        , BindDst            
        , Bind 1 (Word "$23")
        , Bind 2 (Word "$19")
        , Bind 3 (Word "$4") 
        , Bind 4 (Abstr "second")
        , BindResultAlloc 5 (TypeConst "Abstr")
        , BindResultAlloc 6 (TypeConst "Abstr")
        , Apply 5 4 1  -- $6 := second 23
        , Eval
        , Apply 6 5 2 -- $0 := (second 23) 19
        , Eval
        , Apply 0 6 3 -- $0 := ((second 23) 19) 24
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , DeallocResult (TypeConst "Abstr")
        , EndScope 7
        , Yield
        ]   
     
     , DefAbstr "second" 
        [ Suspend 
        , Suspend 
        , BeginScope 4
        , BindDst
        , BindArgs 3      
        , Copy 0 2 
        , EndScope 4
        , Yield 
        ]
     ]     
  
  ,RfCompTest "BindArgs instruction test 3" 24 "" 
     [ DefWord "$23" 23
     
     , DefWord "$19" 19
     
     , DefWord "$4" 24
     
     
     , DefAbstr "mcmain" 
        [ BeginScope 7
        , BindDst            
        , Bind 1 (Word "$23")
        , Bind 2 (Word "$19")
        , Bind 3 (Word "$4") 
        , Bind 4 (Abstr "third")
        , BindResultAlloc 5 (TypeConst "Abstr")
        , BindResultAlloc 6 (TypeConst "Abstr")
        , Apply 5 4 1  -- $6 := third 23
        , Eval
        , Apply 6 5 2 -- $0 := (third 23) 19
        , Eval
        , Apply 0 6 3 -- $0 := ((third 23) 19) 24
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , DeallocResult (TypeConst "Abstr")
        , EndScope 7
        , Yield
        ]   
     
     , DefAbstr "third" 
        [ Suspend 
        , Suspend 
        , BeginScope 4
        , BindDst
        , BindArgs 3      
        , Copy 0 3 
        , EndScope 4
        , Yield 
        ]
     ]     
  
  ,RfCompTest "'Add : Word -> Word -> Word' instruction test" 42 ""
     [ DefWord "$23" 23
     
     , DefWord "$19" 19
     
     , DefAbstr "mcmain" 
        [ BeginScope 5
        , BindDst            
        , Bind 1 (Word "$23")
        , Bind 2 (Word "$19") 
        , Bind 3 (Abstr "add : Word -> Word -> Word")
        , BindResultAlloc 4 (TypeConst "Abstr")  
        , Apply 4 3 2  -- $4 := second 19
        , Eval
        , Apply 0 4 1 -- $0 := (second 19) 23
        , Eval
        , DeallocResult (TypeConst "Abstr")
        , EndScope 5
        , Yield
        ] 
     ]

     ,RfCompTest "Switch instruction test 1" 1 ""
        [ DefWord "$23" 23
        , DefWord "$19" 19
        , DefWord "$1" 1
        , DefWord "$2" 2
        , DefWord "$3" 3

      
        , DefAbstr "mcmain" 
           [ BeginScope 3
           , BindDst            
           , Bind 1 (Word "$23")
           , Switch 1 [(23,[Bind 2 (Word "$1")])
                      ,(19,[Bind 2 (Word "$2")]) 
                      ] 
                    [Bind 2 (Word "$3")]
           , Copy 0 2
           , EndScope 3
           , Yield
           ] 
        ]

     ,RfCompTest "Switch instruction test 2" 2 ""
        [ DefWord "$23" 23
        , DefWord "$19" 19
        , DefWord "$1" 1
        , DefWord "$2" 2
        , DefWord "$3" 3

      
        , DefAbstr "mcmain" 
           [ BeginScope 3
           , BindDst            
           , Bind 1 (Word "$19")
           , Switch 1 [(23,[Bind 2 (Word "$1")])
                      ,(19,[Bind 2 (Word "$2")]) 
                      ] 
                    [Bind 2 (Word "$3")]
           , Copy 0 2
           , EndScope 3
           , Yield
           ] 
        ]

      ,RfCompTest "Switch instruction test 3" 3 ""
        [ DefWord "$23" 23
        , DefWord "$19" 19
        , DefWord "$1" 1
        , DefWord "$2" 2
        , DefWord "$3" 3

      
        , DefAbstr "mcmain" 
           [ BeginScope 3
           , BindDst            
           , Bind 1 (Word "$1")
           , Switch 1 [(23,[Bind 2 (Word "$1")])
                      ,(19,[Bind 2 (Word "$2")]) 
                      ] 
                    [Bind 2 (Word "$3")]
           , Copy 0 2
           , EndScope 3
           , Yield
           ] 
        ]

  ]

runRfTests opt dm = checkTests opt dm tests


printTestLine s s' = putStrLn $ s++" "++ take (80-8-length s) (repeat '.') 
    ++" "++ s'

checkTests :: Bool -> DebugMode -> [RfCompTest] -> IO Bool
checkTests opt dm (t:ts) 
 = do passed <- checkTest opt dm t  
      if passed 
        then do
          printTestLine (testName t) "OK";
          checkTests opt dm ts 
        else do
          printTestLine (testName t) "FAILED";
          return False 
checkTests _ _ [] = return True 

checkTest :: Bool -> DebugMode -> RfCompTest -> IO Bool  
checkTest opt dm (RfCompTest nam ec eout ds)
  = do (c,out,err) <- llvmCompileAndRun "test" opt dm "" $ do 
                         MC.initialEntryPoint (64*1024) "mcmain"
                         compileRfBuilder ds
                         compileRfBuilder builtinConstants
                         return ()
       let c' = case c of 
                 (ExitSuccess)   -> 0
                 (ExitFailure i) -> i
       if c' == ec && eout == out
         then return True
         else if err /= "" 
                   then do printTestLine nam "\n";
                           putStrLn err
                           return False 
                   else return False



main = runRfTests False NoDebug
