{-# LANGUAGE OverloadedStrings #-}
module CodeGen where

import qualified LLVM.AST.Constant as C
import LLVM.IRBuilder.Instruction as I
import LLVM.AST.Operand 
import qualified LLVM.AST.Instruction as AST 
import LLVM.AST.Type
import LLVM.AST.Name
import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Constant

import Compiler   
import AST
import LLVMUtils
import Data.String
import Control.Monad.Trans.Class
import Control.Monad.State.Lazy hiding (void)

import MicroCodes



-- also returns entry point continuation (applicable? needed for modules)
compileRfBuilder :: [RfDef] -> RfBuilder () 
compileRfBuilder ds = do st  <- getCompSt
                         let (n,is) = case ds of 
                                        (DefAbstr n is:_) -> (n,is)
                                        _               -> ("",[])  
                         putCompSt $ st { curDefs = ds
                                        , curInstrs = is
                                        , curInstrIdx = [0]
                                        , curAbstrName = n 
                                        }
                         defCodeGenLoop                
 


defCodeGenLoop :: RfBuilder ()
defCodeGenLoop = do 
  st <- getCompSt                        
  case curDefs st of 
    []     -> return ()
    (d:ds) -> do defCodeGen d 
                 putCompSt $ nextDefState st
                 defCodeGenLoop


instrCodeGenLoop :: RfBuilder ()
instrCodeGenLoop = do 
  st <- getCompSt                        
  case curInstrs st of 
    []     -> return ()
    (i:is) -> do instrCodeGen i
                 putCompSt $ nextInstrState st
                 instrCodeGenLoop



defCodeGen :: RfDef -> RfBuilder ()

defCodeGen (DefAbstr nam _) = do
    ip <- curIP -- cont for the new set instructions
    instrCodeGenLoop                   
    let prevAbstr = C.Undef (ptr i8)
        prevArg = C.Undef (ptr i8)
        prevArgTy = C.Undef (ptr i8) 
        abstr = C.Array (ptr i8) [C.BitCast ip (ptr i8),
                                  prevAbstr,
                                  prevArg,
                                  prevArgTy ]
    globalConst (fromString nam) rfAbstrType abstr
    return ()

defCodeGen (DefWord nam i) = do 
    wordType <- getWordType 
    globalConst (fromString nam) wordType (C.Int 64 i) 
    return ()
  
    

-- beginScope Scope variable allocation
-- Ptr-Nr
--  0      : scope var 0 addr      (bind to rvp)
--  1      : scope var 0 type addr (unused in most (all?) cases, rtp otherwise)
--  2      : scope var 1 addr
--  3      : scope var 1 type addr
--  4      : scope var 2 addr
--  5      : scope var 2 type addr
--  6      : scope var 3 addr
--  7      : scope var 3 type addr
--  8      : scope var 4 addr
--  9      : scope var 4 type addr
-- ...
-- 2*i     : scope var i addr
-- 2*i+1   : scope var i type addr
--
-- the return address is special its has no type addr and
-- can thus not be used as an argument (since usually uninitialized anayway) 
--
-- The number of scope variables is known at compile time, so do
-- frame pointer is required.
-- to allocate n variables 
-- 
--
--

-- the start begins is an allocation on the code segment
-- however the scope uses the csp directly to address the variables
-- it can thus not be used nested - thats why its not named
-- alloc 



instrCodeGen :: RfInstr -> RfBuilder ()

instrCodeGen (BeginScope n)  
  = emitCurInstr $ do 
      st <- get
      -- allocate data on call stack
      --  2* maxVar - for a pointer to data and to the type value 
      let allocDecrement = int64 (-(2*n)) -- csp grows down
      scopeFrame <- gep (csp st) [allocDecrement] `named` "scopeFrame"
      put $ st { csp = scopeFrame }  
    
      nextInstrMC 


instrCodeGen (EndScope n)    
  = emitCurInstr $ do
      st <- get
    
      -- store a undef to signal end of life to dead store elimination pass 
      let totScSz = fromInteger (2*n) -- 2+ for saving rvp and cap
          scopeTy = ArrayType totScSz (ptr i8) 
      deadScopeAddr <- bitcast (csp st) (ptr scopeTy) `named` "deadScopeAddr"
      store deadScopeAddr 0 (ConstantOperand (C.Undef scopeTy))  
    
      -- compute stack pointer after dealocation
      let allocIncrement = int64 (2*n) -- csp grows down
      csp' <- gep (csp st) [allocIncrement] `named` "csp"
      
      -- update register state 
      put $ st { csp = csp' }
      
      nextInstrMC 
  
instrCodeGen (Yield)        
  = emitCurInstr $ do 
      retCont <- popContMC
      gotoMC retCont
    
instrCodeGen (Copy dstVarId srcVarId)      
  = emitCurInstr $ do
      dst  <- scopeVarAddrMC dstVarId `named` "copyDstAddr"  
      src  <- scopeVarAddrMC srcVarId `named` "copySrcAddr"
      typ  <- scopeVarTypeAddrMC srcVarId `named` "copySrcTyAddr"
      len  <- getValueSizeOfMC typ `named` "valueSize"
      wordType <- getWordTypeMC 
      sz <- lift $ lift $ getAllocSizeAsWord64 wordType  
      let funty = ptr $ FunctionType void [ptr i8,ptr i8,wordType] False  
          funName = mkName $ "rawmemcpy"
          memcpyIntrinsic = ConstantOperand $ C.GlobalReference funty funName 
   
      dst' <- bitcast dst (ptr i8) `named` "memcpyDst"
      src' <- bitcast src (ptr i8) `named` "memcpySrc"
      call memcpyIntrinsic [(dst',[]), (src',[]{-[NoAlias,NoCapture]-}), (len,[]) ]
    
      nextInstrMC 


instrCodeGen (BindDst)       
  = emitCurInstr $ do
    st <- get
    valAddrAddr <- scopeVarAddrAddrMC 0
    store valAddrAddr 0 (rvp st)
    nextInstrMC

instrCodeGen (BindDstType dstTypeVal)    
  = emitCurInstr $ do
      valTypeAddr <- rfValueAddrMC dstTypeVal
      valTypeAddrAddr <- scopeVarTypeAddrAddrMC 0
      store valTypeAddrAddr 0 valTypeAddr
      nextInstrMC

instrCodeGen (BindValue var val)  
  = emitCurInstr $ do
      valAddr     <- rfValueAddrMC val
      bindValueMC var valAddr 
      nextInstrMC 

instrCodeGen (BindType var typeVal)
  = emitCurInstr $ do
      typeValAddr  <- rfValueAddrMC typeVal
      bindTypeMC var typeValAddr 
      nextInstrMC
    
instrCodeGen (Bind var val)  
  = emitCurInstr $ do
      valAddr     <- rfValueAddrMC val
      valTypeAddr <- rfValueTypeAddrMC val
      bindMC var valAddr valTypeAddr 
      nextInstrMC
  
instrCodeGen (Apply y f x) 
  = emitCurInstr $ do 
      applyMC y f x
      nextInstrMC

instrCodeGen (Eval)          
  = emitCurInstr evalMC

instrCodeGen (Taileval)
  = emitCurInstr tailevalMC  

instrCodeGen (BindArgs 0)    = error "ERROR Reforth BindArgs 0 invalid" 
instrCodeGen (BindArgs n)    
  = emitCurInstr $ do 
      st <- get
      bindArgsMC n n
      put st 
      nextInstrMC

instrCodeGen (BindResultAlloc var tyVal) 
  = emitCurInstr $ do 
      st <- get
      tyValAddr <- rfValueAddrMC tyVal
      bindMC var (rsp st) tyValAddr
      len  <- getValueSizeOfMC tyValAddr `named` "resultValueSize"
      newRsp <- gep (rsp st) [len] `named` "newRsp"
      put $ st { rsp = newRsp }  
      nextInstrMC

instrCodeGen (DeallocResult tyVal) 
  = emitCurInstr $ do 
      st <- get
      tyValAddr <- rfValueAddrMC tyVal
      len  <- getValueSizeOfMC tyValAddr `named` "resultValueSize"
      minusOne <- mkWordConstMC (-1) 
      dec  <- mul (ConstantOperand minusOne) len 
      newRsp <- gep (rsp st) [dec] `named` "newRsp"
      markMemUndefMC newRsp len
      put $ st { rsp = newRsp }  
      nextInstrMC
    

instrCodeGen (LLVMBinOp nam ty dst arg1 arg2)  
  = emitCurInstr $ do 
     tyval <- lift $ lift $ lift $ llvmBaseType ty
     llvmBinOpMC nam tyval (llvmBinOpByName nam)  dst arg1 arg2 
     nextInstrMC  

instrCodeGen (LLVMUnaryOp nam tyNam dst arg1)
  = emitCurInstr $ do
      ty <-  lift $ lift $ lift $ llvmBaseType tyNam
      arg1' <- scopeVarAddrMC arg1
      dst'  <- scopeVarAddrMC dst
      v1Addr <- bitcast arg1' (ptr ty) `named` "v1Addr"
      v2Addr <- bitcast dst'  (ptr ty) `named` "v2Addr"
      v1 <- load v1Addr 0  `named` "val1"
      v2 <- lift $ (llvmUnaryOpByName nam) v1 `named` "val2"
      store v2Addr 0 v2 
      nextInstrMC    
 
instrCodeGen Suspend = do
  cont <- withNextInstrState curIP
  emitCurEntryPointDef $ do  
    entry <- lift $ block `named` "entry"; do  
      
      st <- get
      abstrAddr <- bitcast (rvp st) (ptr (ptr i8)) `named` "abstrAddr"    
      
      contAddrAddr  <- gep abstrAddr [int64 0] `named` "contAddrAddr"
      prevAbsAddrAddr <- gep abstrAddr  [int64 1] `named` "prevAbstrAddrAddr" 
      prevArgAddrAddr <- gep abstrAddr [int64 2] `named` "prevArgAddrAddr"
      prevArgTypeAddrAddr <- gep abstrAddr [int64 3] `named`"prevArgTypeAddrAddr"

      store contAddrAddr 0 $ ConstantOperand (C.BitCast cont (ptr i8))
      store prevAbsAddrAddr 0 (cap st)
      store prevArgAddrAddr 0 (avp st)
      store prevArgTypeAddrAddr 0 (atp st)
 
      retCont <- popContMC
      gotoMC retCont
  return ()

instrCodeGen (Switch var cases otherwise) = do
  st <- getCompSt
  contName <- withNextInstrState curIPName
  putCompSt $ st { curTermContName = contName } -- for corretly leaving cases 
  cs  <- emitSwitchCases var cases
  (oc,on) <- emitSwitchCaseInstrs var "otherwise" otherwise
  putCompSt st 
  emitCurEntryPointDef $ do
    wordType <- getWordTypeMC 
    varAddr  <- scopeVarAddrMC var
    varAddr' <- bitcast varAddr (ptr wordType)
    varValue <- load varAddr' 0  
    I.switch varValue (fromString on) (map fst cs)  
    emitSwitchBlocks var (map snd cs)
    emitSwitchBlock  var ("otherwise",oc)
  return ()



emitSwitchCases :: ScopeVarId  
                -> [(Integer,[RfInstr])] 
                -> RfBuilder [( (C.Constant,Name) -- for llvm switch cases
                              , (Integer,C.Constant) -- for llvm block gen. 
                              )]
emitSwitchCases var [] = return []
emitSwitchCases var ((i,c):cs) = do
  p <- emitSwitchCase var (i,c)
  ps <- emitSwitchCases var cs
  return (p:ps)

switchBlockName :: ScopeVarId -> String -> String
switchBlockName var i 
  =  "case "++show var++ " of " ++ i

emitSwitchCase :: ScopeVarId
              -> (Integer,[RfInstr]) 
              -> RfBuilder ( (C.Constant,Name) -- for llvm switch cases
                           , (Integer,C.Constant) -- for llvm block gen. 
                           )   
emitSwitchCase var (const,is) = do
  (cont,sbn) <- emitSwitchCaseInstrs var (show const) is
  c <- mkWordConst const
  return ((c,sbn),(const,cont))

emitSwitchCaseInstrs var cnam is = do
  let sbn = switchBlockName var cnam
  st <- getCompSt
  putCompSt st { 
      curInstrs = is 
    , curInstrIdx = 0 : curInstrIdx st
    -- TODO: Make more beautiful names, but thats sufficient for now 
    , curAbstrName = curAbstrName st  ++ sbn
    }
  cont <- curIP
  instrCodeGenLoop
  putCompSt st
  return (cont,fromString sbn)

emitSwitchBlocks :: ScopeVarId -> [(Integer,C.Constant)] -> MCBuilder ()
emitSwitchBlocks var [] = return ()
emitSwitchBlocks var ((cur,c):cs) = do emitSwitchBlock var (show cur,c)
                                       emitSwitchBlocks var cs

emitSwitchBlock :: ScopeVarId -> (String,C.Constant) -> MCBuilder ()                                       
emitSwitchBlock var (cur,c) =  do
  emitBlockStart (Name (fromString (switchBlockName var cur))); do
    gotoMC $ ConstantOperand c
  
--instrCodeGen x = error $ "Error: codegen for " ++ show x ++ " not implemented"  
                   


-- Basic instruction builders 
--  using a "careful" monadic wrapper to make it readable as semantic
--  spec

assert cond msg = if cond then return () else error msg


emitCurInstr -- vm continuation 
  :: MCBuilder () -> RfBuilder ()
emitCurInstr stmts 
  = do st <- getCompSt
       n' <- curIPName
       cont <- withNextInstrState curIP     
       emitCurEntryPointDef $ do  
         entry <- lift $ block `named` (fromString "entry"); do
           debugPrintLnMC $ "--> " ++ n'  
           stmts
       return ()
