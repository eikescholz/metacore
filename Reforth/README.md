Reforth Functional Low Level Virtual Machine
============================================

Reforth is motivated by forths two stack layout but extends it to
4 stacks in order to implement functional languages (i.e. closures) without
requiring garbage collection in most application cases. 

It is targeted to be on an equal abstraction layer as the LLVM machine,
but is in fact implemented on top of it using continuation passing style
encoding each reforth instruction as a LLVM toplevel function. This allows
to fairly compare Reforth based languages with LLVM based languages, provided
that the LLVM inliner does a good job, which seems to be the case for quite
some time.

Basic Types:
------------

TODO: 
...

Compilation Units:
------------------

On an abstract level a Reforth program is given by a set of global
constants and variables. The program will have a "main" constant
that is evaluated automatically and represented by the toplelvel instructions.

TODO: 
 - Figure out if only constants are enough 
 - Figure out if decomposition in compilation units is good
 - Figure out what kind of shared library system would be best suited

Stacks Layout 
-------------

While using multiple stacks seems to be wasteful, using two stacks for forth 
did not in fact create substantial memory bloat for decades, so that adding
an other 2 stacks seems reasonable for contemporary hardware. However having
4 stacks allows to implement a trivial kind of fake-copying garbage collection 
semantics.by swapping stack pointers of stacks that grow into the same 
direction.  

In particular

 The stack layout can be described as, using two continuous memory areas, each
 containg to stacks growing in opposing directions. The stacks are called:

  - The arguments stack, for local data used as aguemnts. (grows up)
  - The results stack, for result data of an expression. (grows up)
  - The shunt stack for local scope surviving data (was named data stack). (grows down)
  - The call stack, for return addresses and temporary data (grows down)

  This layout can be visualised as: 

```
    +--------------     ------------+
    | Arguments ->  ...    <- Shunt | 
    +--------------     ------------+
    | Results ->    ...    <- Call  |
    +--------------     ------------+
```

The Register Set:
-----------------

Along with the necessary registers for tracking the stacks there
are addtional registers to track the state of the reforth machine:

The complete rgister set are:

  - asp: Argument stack pointer (grows up)
  - ssp: Shunt stack pointer (grows down)
  - rsp: Result stack pointer (grows up)
  - csp: Call stack pointer (grows down)
  - rvp: Return value pointer (call ABI)
  - cap: Current abstraction pointer (call ABI - closure data)
  - avp: Argument value pointer (call ABI argument pointer)
  - atp: Argument type pointer 

So aside from the stack pointers there are three registers associated with
a call ABI. 


Abstractions 
------------
An abstraction is function/procedure with one argument, whose value is stored in
the argument value pointer and whose run time type information is stored int
argument type pointer. The address to call is stored in the current abstraction
pointer. Holding the calls "goto" address in a register is necessary since
some stack organizing may be necessary right before the jump. 



Scopes and Computations: 
------------------------

To easily support polymorphism the call ABI must be the same for all types
of data. The addition of big numeric vectors uses the same ABI, as addition 
for machine words, where the latter is guaranteed to be reduced to actual 
target hardware instructions by the optimizer. However using the interface with 
big numerical data structures makes forth style handling with with a lot of 
data duplication (i.e. the dup instruction) inefficient.

Reforths basic instructions will get the variables by reference stored
as temporary data on the call stack. The stack frames containing the
variable names are called a scope and scope variables are simply accessed by
an index number. Index 0 always refers to the location of the return
data and index 1 refers to the argument data, higher indices refer to 
either previous captured arguments or local variables. After the arguments scope variables there can be additional
variables for local bound data resulting for example from higher level let 
expressions. 


Instruction Set:
----------------


### openScope m k ###

  - Semantics: 
     Open a scope with maximal scope variable id m and reading k arguments. 
  
  - Requirements: m <= k 
 
  - Notes: openScope 0 0 #



JIT Notes:
----------

NOTE: Discared for haskell prototype, see below

Reforth is low-level and compilation units based, so its not clear how JIT 
support should be implemented. However the support should provide to ability
to implement higher level languages with JIT support. The JIT further allows
easier regression test implementation. 

What would be the top level evaluation for this JIT?
   
The JIT must contain a representation of the program as set of global constants
and global vairables. Since it Refoth is a kind of lambda-calculus-machine 
assembly, only 3 semantic high level functionalities need to be suppoted, 
that are  

  - Defining global variables/constants
  - Abstraction, i.e. defining function constants
  - Application, function evaluation

The only JIT API interface requirement that has no reforth AST top level 
representation is application. 


The main problem for implementation is handling the additional state required
for the continuation passing style used to make code that can manage its own
stacks and call semantics. 

Form the system/LLVM "main" the bootstrap is the following

  1. Initialize registers to point to valid stack memeory
  2. Switch into CPS execution evaluating the "mcmain" abstraction
     providing a halt/exit continuation

If Reforth is used to implement a language with repl the interpreter needs
to perform step 1 once, and then evaluate some toplevel expression.
If mcmain is a updateable pointer to an abstraction step 2 can be interated
and the code for entering CPS mode needs only be compiled once.
In addtion some place is required to save the Reforth register set. 

### Current Problems ###
There is a confusion in the code at the moment regarding continuations.
The current continuation i.e. the address of the *next* "Instruction"
is called next continuation. Since I did not distinguish between
instructions, labels, and continuations.
That is, if we use the traditional current continuation as
in:
   callcc foo
   bar

then the code address beginning with bar is the current continuation of the
callcc line, not the next continuation.   

In that regard a continuation is an outgoing instruction address.
It would still be be possible to call all instructions addresses continuations,
but then, when compiling the callcc as above, the continuations
being generated is the previous continuation. That naming will also make
the source confusing. 
 
In that regard the source should switch naming to current instruction pointer 
and next instruction pointer. 

Abstractions should be named closures as they represent
an abstraction applied to an tangible variable,  i.e.  
for a given v the value of
   (x -> ... ) v
and the ( x -> ... ) is traditionally called the abstraction in lambda calculus

That is a closure is:     
  1. An instruction pointer followed by three null ptrs 
  2. An instruction pointer ... (TODO) 


### Problems with LLVMs MCJIT ###

By design, and hint in the llvm-haskell documentation the MCJIT can handle
one module at a time. However llvm-hs does not use the internal module 
representation so that internal module can not readily be modified.
There does not seem to be an api to merge modules, so the module of
the execution engine has do be "redone" for each update. The llvm-hs 
Kaleidoskope tutorial actually creates a fresh execution engine with an
updated module for each iteration - playing it save with internal object
construction/destruction schemes.

This is slow but enough for a PoC JIT. Reforth can be redone in C/C++ if 
sufficiently matured anyway. 

However the memory and register set can not part of a module since it will
be discarded. 

Thats not a good idea. Suppose there is a expression with side effects
returning a closure (i.e. a function pointer into that module) then
the side effect action needs to be repeated to avoid dangling pointers ... 
...
So the compiled modules and execution enginies have to be kept,
wich requires me to use internal API, and raises inter-module object linking
issues.

The jit may have to wait until C/C++ rewrite. 



