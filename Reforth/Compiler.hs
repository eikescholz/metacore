
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Compiler where

import qualified Data.ByteString.Char8 as BS 
import Data.Word
import Data.String
import Foreign.Ptr
import Foreign.C.String
import Foreign.Marshal.Alloc
import Data.List
import System.Exit
import Control.Monad.State.Lazy
import System.Process

import qualified LLVM.IRBuilder.Instruction as I   
import qualified LLVM.Internal.FFI.DataLayout as FFI
import qualified LLVM.Internal.FFI.Target as FFI
import qualified LLVM.Internal.FFI.PtrHierarchy as FFI
import qualified LLVM.AST as AST
import qualified LLVM.Transforms as Tr
import qualified LLVM.AST.Constant as C
import LLVM.AST.FunctionAttribute as FA
import LLVM.AST hiding (function,functionAttributes)
import LLVM.AST.Type as AST
import LLVM.AST.Global
import LLVM.AST.ParameterAttribute
import LLVM.Internal.Target
import LLVM.Internal.Coding
import LLVM.Internal.DecodeAST
import LLVM.Internal.EncodeAST
import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.PassManager
import LLVM.Transforms
import LLVM.Context
import LLVM.Module
import LLVM.Analysis

import AST


data RfCompilerState = RfCompilerState {
    targetDataLayout :: Ptr FFI.DataLayout
  , uniqueGlobalNameNum :: Integer
  , debugMode :: DebugMode
  , wordType :: Type
  , curTermContName :: String
  , curDefIdx :: Integer
  , curDefs :: [RfDef]
  , curInstrIdx:: [Integer] 
  , curInstrs :: [RfInstr] -- head is current instruction
  , curAbstrName :: String 
  }

-- MC for micro-code. Reforth generates CPS  and the llvm code in these
-- functions are its micro-code. Register pinning is used to motivate
-- to keep the reforth machine registers in target machine registers.
-- TODO: Should use ghc calling convention, that passes up to 10 args
-- in register if I remember correctly.  
data MCBuilderState = MCBuilderState { -- Runtime State - Registers
                         asp :: Operand
                       , ssp :: Operand
                       , rsp :: Operand
                       , csp :: Operand
                       , rvp :: Operand
                       , cap :: Operand
                       , avp :: Operand
                       , atp :: Operand
                         -- Compile Time State 
                       , nextCont :: C.Constant -- compile time known
                       }



-- llvm-hs has ridiculous generic interface. we won't need that
-- however we need some target machine information in the
-- state. Also monads are scary, especially things like the following nested 
-- state abstraction. 

type LLVMIO = StateT RfCompilerState IO 

type RfBuilder = ModuleBuilderT LLVMIO

type LLVMBuilder = IRBuilderT RfBuilder

type MCBuilder = StateT MCBuilderState LLVMBuilder 


-- TODO: Merge this with opt into a flags list
data DebugMode = NoDebug
               | InternalDebug


type LLVMBinOpType = Operand -> Operand -> LLVMBuilder Operand
type LLVMUnaryOpType = Operand -> LLVMBuilder Operand


-- Abstraction for now is (continuation,captured arg,outerAbstr)
-- pointers as (ptr i3) to reduce llvm output type clutter
-- firt cont, then prev capture, then arg
-- 
-- [ continaution addr 
-- , prevAbstractionAddr
-- , argAddr
-- , argTyAddr
-- ]
--


rfAbstrType = ArrayType 4 (ptr i8)

{- LLVM has no built in tagged union type,

A type value is a 4-word/ptr value, starting with
an adt tag and then payload:


( 0, () , ()   , ()     ) : Unit Type 
( 1, () , ()   , ()     ) : Word Type 
( 2, dom, codom, ()     ) : Abstraction Type 
( 3, ty , tbl  , ()     ) : Interface Type
( 4, n  , tbl   , valTy ) : Reference Type ... maybe ... TODO
 ...

-}

rfTypeValueType = ArrayType 4 (ptr i8)



-- all registers are general pointers, the types are meant
-- to represent the primary usage, and thus reduce LLVM bitcast clutter
-- however the continuation is reqresented as (ptr i8) since the type
-- cont type can not contain itself... 

rfContParas :: [(Type,ParameterName,[ParameterAttribute])]
rfContParas = [ (ptr i8,"asp",[NoAlias]) -- argument stack pointer
              , (ptr i8,"ssp",[NoAlias]) -- data stack pointer
              , (ptr i8,"rsp",[NoAlias]) -- result stack pointer
              , (ptr (ptr i8),"csp",[NoAlias]) -- call stack pointer
              , (ptr i8,"rvp",[NoAlias]) -- result value pointer
              , (ptr i8,"cap",[]) -- current abstraction pointer 
              , (ptr i8,"avp",[]) -- argument value pointer
              , (ptr i8,"atp",[]) -- argument type pointer 
              ]

-- the next continuation register is used to chain instructions that
-- are implemented as continations into blocks 
-- the defered continuation register is used to implement the
-- apply ... cont pair for tail-calls 

rfContArg = map (\(t,p,_)-> (t,p)) rfContParas 
rfContType = ptr $ FunctionType AST.void (fst <$> rfContArg) False

-- This is missing ... I should use my fresh git-hub account to help the llvm-hs
-- author a bit ... at least if such stuff accumulates

foreign import ccall unsafe "LLVMIntPtrType" getIntPtrType :: Ptr FFI.DataLayout -> IO (Ptr FFI.Type)

-- a prettyprinter 
ppllvm :: AST.Module -> IO BS.ByteString
ppllvm ast = do  
  withContext $ \context ->
    withModuleFromAST context ast $ \m -> do
      llstr <- moduleLLVMAssembly m
      return $ llstr



getCompSt :: RfBuilder RfCompilerState
getCompSt = lift $ get

putCompSt :: RfCompilerState -> RfBuilder ()
putCompSt s = lift $ put s


curIPName :: RfBuilder String                         
curIPName = do
  st <- getCompSt
  case curInstrs st of 
    [] -> return $ curTermContName st
    (i:is) -> return $ show (curDefIdx st) 
                      ++ concat (intersperse "." (map show (reverse 
                                                             (curInstrIdx st))))
                      ++" : "++ curAbstrName st
                      ++" : "++ show i                                              

ipByName -- vm continuation,  
  :: String -- Linker Symbol Name
  -> RfBuilder C.Constant -- result cont
ipByName nam  = do
  let label = fromString nam
  wordType <- getWordType
  pure $ C.GlobalReference rfContType label
                                            

curDef :: RfBuilder RfDef
curDef = do comSt <- getCompSt
            return $ head $ curDefs comSt

curIP :: RfBuilder C.Constant  
curIP = do comSt <- getCompSt 
           nam <- curIPName 
           ipByName nam

           

-- does this already exist somewhere ?
tail'   [] = []
tail' (i:is) = is

nextInstrState :: RfCompilerState -> RfCompilerState
nextInstrState st 
  = st { curInstrIdx = head (curInstrIdx st) + 1 : tail (curInstrIdx st) 
       , curInstrs =  tail' (curInstrs st) }
       

nextDefState :: RfCompilerState -> RfCompilerState
nextDefState st 
  = st { curDefIdx = curDefIdx st + 1  
       , curDefs =  tail' (curDefs st)
       , curInstrIdx = [0]
       , curInstrs = is 
       , curAbstrName = n }
  where
    (n,is) = case tail' (curDefs st) of
               (DefAbstr n is :_) -> (n,is)
               _                  -> ("",[])    

withNextInstrState :: RfBuilder a -> RfBuilder a
withNextInstrState build = do
  st <- lift $ get
  lift $ put $ nextInstrState st
  a <- build
  lift $ put st
  return a



llvmBinOpByName :: String -> LLVMBinOpType
llvmBinOpByName "add"  = I.add
llvmBinOpByName "sub"  = I.sub
llvmBinOpByName "mul"  = I.mul
llvmBinOpByName "udiv" = I.udiv
llvmBinOpByName "umod" = I.urem 
llvmBinOpByName "sdiv" = I.sdiv
llvmBinOpByName "smod" = I.srem
llvmBinOpByName "and"  = I.and
llvmBinOpByName "or"   = I.or
llvmBinOpByName "xor"  = I.xor


llvmUnaryOpByName :: String -> LLVMUnaryOpType
llvmUnaryOpByName s = error "No Unary Op names implemented (needed?)"


llvmBaseType :: String -> LLVMIO Type
llvmBaseType "Word" = get >>= \ st -> return $ wordType st
llvmBaseType "Byte" = return i8
llvmBaseType "Bool" = return i1


getWordType :: RfBuilder Type
getWordType  
 = do st <- getCompSt 
      let dl = targetDataLayout (st :: RfCompilerState ) :: Ptr FFI.DataLayout
      tyPtr <- lift $ lift $ getIntPtrType dl
      lift $ lift (runDecodeAST (decodeM tyPtr) :: IO Type)

getAllocSizeAsWord64 ty
 = do st <- getCompSt :: RfBuilder RfCompilerState 
      let dl = targetDataLayout (st :: RfCompilerState ) :: Ptr FFI.DataLayout
      -- TODO: no fresh context for this,  do this once and save the
      -- result in the compiler state
      sz <- lift $ lift $ withContext $ \ ctx -> do  
                     let astEnc = encodeM ty :: EncodeAST (Ptr FFI.Type)  
                     tyPtr <- runEncodeAST ctx astEnc :: IO(Ptr FFI.Type)
                     sz <- FFI.getTypeAllocSize dl tyPtr :: IO Word64
                     return sz 
      return sz

mkWordConst :: Integer -> RfBuilder C.Constant     
mkWordConst i 
 = do wty <- getWordType
      wsz <- getAllocSizeAsWord64 wty
      return $ C.Int (fromInteger (8*toInteger wsz)) (fromInteger $ toInteger i)  

getAllocSizeOf :: Type -> RfBuilder C.Constant   
getAllocSizeOf ty 
 = do sz <- getAllocSizeAsWord64 ty  
      mkWordConst (toInteger sz)


uniqueGlobalNamePrefix = "____"

getUniqueGlobalName :: LLVMIO Name
getUniqueGlobalName = do
  st <- get
  let i = uniqueGlobalNameNum st
  put $ st { uniqueGlobalNameNum = i+1 }
  return $ fromString $ uniqueGlobalNamePrefix ++ show i 

getDebugMode :: LLVMIO DebugMode
getDebugMode = do get >>= \ st -> return $ debugMode st




emitCurEntryPointDef :: MCBuilder ()
                     -> RfBuilder C.Constant -- result cont
emitCurEntryPointDef body = do
  nam <- curIPName
  nxtCnt <- withNextInstrState curIP
  wordType <- getWordType
  let label = fromString nam
  let tys = fst <$> rfContArg
  st <- get
  (paramNames, blocks) <- runIRBuilderT emptyIRBuilder $ do
    paramNames <- forM rfContArg $ \(_, paramName) -> case paramName of
      NoParameterName -> fresh
      ParameterName p -> fresh `named` p
    let bodyArgs = zipWith LocalReference tys paramNames
    let (asp:ssp:rsp:csp:rvp:cap:avp:atp:_) = bodyArgs
    let st0 = MCBuilderState asp ssp rsp csp rvp cap avp atp nxtCnt
    runStateT body st0
    return paramNames
  let
    def = GlobalDefinition functionDefaults
      { name        = label
      , parameters  = (zipWith (\(t,_,ps) n -> Parameter t n ps) 
                               rfContParas paramNames, False)
      , returnType  = AST.void
      , basicBlocks = blocks
      , functionAttributes = [Right FA.AlwaysInline]
      }
  emitDefn def
  pure $ C.GlobalReference rfContType label



-- the following is a somewhat like a StateT with "reversed side effect order"
--  Reason: 
--   Its easier to build the next instruction to have the constination
--   for the first instruction ... 


emitCurEntryPointDecl :: RfBuilder C.Constant -- result cont
emitCurEntryPointDecl = do
  nam <- curIPName
  wordType <- getWordType
  let label = fromString nam
  let tys = fst <$> rfContArg
  paramNames <- forM rfContArg $ \(_, paramName) -> case paramName of
    NoParameterName -> error "rfContDecl para names fresh precondition failed"
    ParameterName p -> return $ Name p
  let
    def = GlobalDefinition functionDefaults
      { name        = label
      , parameters  = (zipWith (\(t,_,ps) n -> Parameter t n ps) 
                               rfContParas paramNames, False)
      , returnType  = wordType
      , basicBlocks = []
      , functionAttributes = [Right FA.AlwaysInline]
      }
  emitDefn def
  pure $ C.GlobalReference rfContType label



 

passes :: PassSetSpec
passes = defaultPassSetSpec 
           { transforms = [  
--               CorrelatedValuePropagation,
 --              DeadCodeElimination,
--               InductionVariableSimplify, 
--               InstructionCombining,
--               DemoteRegisterToMemory, 
--               EarlyCommonSubexpressionElimination,
--               SimplifyControlFlowGraph,
--               GlobalDeadCodeElimination,
--               ConstantMerge,
--               Reassociate,
--               ArgumentPromotion,
--               InterproceduralConstantPropagation,
--               InterproceduralSparseConditionalConstantPropagation,


               -- mandatory optimizations (order matters ! ...)
               DeadStoreElimination, -- required to set up analysis ! (but why) 
               LoopVectorize True True,
               ConstantPropagation,
               PromoteMemoryToRegister,
               TailCallElimination,
               Tr.FunctionAttributes, 
               FunctionInlining 10000,
               Tr.AlwaysInline True,
               LoopClosedSingleStaticAssignment,
               LoopInvariantCodeMotion,
               LoopDeletion,
               LoopIdiom ,
               LoopInstructionSimplify,
               LoopRotate,
               LoopStrengthReduce,
               LoopUnroll Nothing Nothing (Just True),
               LoopUnswitch True, -- parameter is optimizeForSize
               GlobalValueNumbering False, -- mandatory
               --MemcpyOptimization, -- this makes things worse
               DeadStoreElimination  -- do actual elim

            ]
          }



optimize :: AST.Module -> IO AST.Module
optimize mod = do
  withContext $ \context ->
    withModuleFromAST context mod $ \m ->
      withPassManager passes $ \pm -> do
        verify m
        runPassManager pm m
        optmod <- moduleAST m
        return optmod


runBuilderWithTargetMachine :: DebugMode -> LLVMIO a -> TargetMachine -> IO a 
runBuilderWithTargetMachine dm builder (TargetMachine tm)  = do 
  dlString0 <- FFI.getTargetMachineDataLayout tm
  dlString1 <- decodeM dlString0 :: IO String
  dlString2 <- newCString dlString1 :: IO CString
  dl <- FFI.createDataLayout dlString2
  tyPtr <- getIntPtrType dl
  wordTy <- runDecodeAST (decodeM tyPtr) :: IO Type
  mod <- evalStateT builder (RfCompilerState dl 0 dm wordTy "Halt" 0 [] [0] [] "")
  FFI.disposeDataLayout dl
  free dlString2
  return mod

testBuild dm mk = do 
  let llvmBuilder = buildModuleT "test" mk :: LLVMIO AST.Module 
  mod <- withHostTargetMachineDefault $ runBuilderWithTargetMachine dm llvmBuilder 
  modStr <- ppllvm mod
  BS.putStrLn modStr
 
testOptiBuild dm mk = do
  let llvmBuilder = buildModuleT "test" mk :: LLVMIO AST.Module 
  mod <- withHostTargetMachineDefault $ runBuilderWithTargetMachine dm llvmBuilder 
  optMod <- optimize mod 
  modStr <- ppllvm optMod
  BS.putStrLn modStr
 
             
 
--llcCmd = "llc-8 --x86-asm-syntax=intel "
llcCmd = "llc-9 -asm-verbose"
asCmd  = "as"
clangCmd  = "clang-9" -- used for final linking 



llvmCompileForHost :: String -- module name
                -> Bool -- optimize ?
                -> DebugMode
                -> ModuleBuilderT LLVMIO a
                -> IO String
llvmCompileForHost name opt dm builder  
  = withHostTargetMachineDefault $ llvmCompile name opt dm builder 
       

llvmCompile :: String -- module name
                -> Bool -- optimize ?
                -> DebugMode 
                -> ModuleBuilderT LLVMIO a
                -> TargetMachine
                -> IO String -- results in path to executable 
llvmCompile name opt dm builder tm = do
  let llvmBuilder = buildModuleT (fromString name) builder :: LLVMIO AST.Module 
  mod <- runBuilderWithTargetMachine dm llvmBuilder tm
  modOpt <- if opt then optimize mod else return mod
  modStr <- ppllvm modOpt 
  let fileBase = "tmp/" ++ name
      llvmFile = fileBase ++ ".ll"
      asFile  = fileBase ++ ".s"
      objFile = fileBase ++ ".o"
  BS.writeFile llvmFile modStr
  callCommand (llcCmd    ++ " " ++ llvmFile)
  callCommand (asCmd     ++ " " ++ asFile ++ " -o " ++ objFile)
  callCommand (clangCmd  ++ " " ++ objFile  ++ " -o " ++ fileBase)
  return fileBase

-- For automating tests essentially 
llvmCompileAndRun :: String -- module name
                        -> Bool -- optimize ?
                        -> DebugMode
                        -> String -- Stdin 
                        -> ModuleBuilderT LLVMIO a
                        -> IO (ExitCode,String,String)
llvmCompileAndRun name opt dm stdin builder  = do 
  execPath <- llvmCompileForHost name opt dm builder 
  readCreateProcessWithExitCode (proc execPath []) stdin 

