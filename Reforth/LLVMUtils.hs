{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ForeignFunctionInterface #-}



-- Some utillity interface functions for llvm-hs 

module LLVMUtils where -- better name RfCompilerBase 

import Data.String

--import qualified Data.Text.Lazy.IO as T  

import qualified Data.ByteString.Char8 as BS

import LLVM.AST.FunctionAttribute as FA
import LLVM.AST.Global
import LLVM.Prelude
--import LLVM.Pretty  -- from the llvm-hs-pretty package
import LLVM.AST hiding (function,functionAttributes)
import qualified LLVM.AST as AST
import LLVM.AST.Type as AST
import qualified LLVM.AST.Constant as C
import AST


import LLVM.IRBuilder.Constant
import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import qualified LLVM.IRBuilder.Instruction as I

import LLVM.Context
import LLVM.Module as Mod
import LLVM.Target 

import LLVM.PassManager
import LLVM.Transforms
import qualified LLVM.Transforms as Tr
import LLVM.Analysis
import LLVM.AST.Linkage

import Control.Monad.State.Lazy

import qualified LLVM.Internal.FFI.DataLayout as FFI
import qualified LLVM.Internal.FFI.Target as FFI
import qualified LLVM.Internal.FFI.PtrHierarchy as FFI
import LLVM.Internal.Target
import LLVM.Internal.Coding
import LLVM.Internal.DecodeAST
import LLVM.Internal.EncodeAST

import LLVM.AST.ParameterAttribute

import Foreign.Ptr
import Foreign.C.String
import Foreign.Marshal.Alloc
import Data.List
import System.Exit


-- some copy/pase/adapt helpers from the llvm-hs-pure codebase 
-- TODO: Submit 





inlineFunction
  :: MonadModuleBuilder m
  => Name  -- ^ Function name
  -> [(Type, ParameterName)]  -- ^ Parameter types and name suggestions
  -> Type  -- ^ Return type
  -> ([Operand] -> IRBuilderT m ())  -- ^ Function body builder
  -> m Operand
inlineFunction label argtys retty body = do
  let tys = fst <$> argtys
  (paramNames, blocks) <- runIRBuilderT emptyIRBuilder $ do
    paramNames <- forM argtys $ \(_, paramName) -> case paramName of
      NoParameterName -> fresh
      ParameterName p -> fresh `named` p
    body $ zipWith LocalReference tys paramNames
    return paramNames
  let
    def = GlobalDefinition functionDefaults
      { name        = label
      , parameters  = (zipWith (\ty nm -> Parameter ty nm []) tys paramNames, False)
      , returnType  = retty
      , basicBlocks = blocks
      , functionAttributes = [Right FA.AlwaysInline]
      }
    funty = ptr $ FunctionType retty (fst <$> argtys) False
  emitDefn def
  pure $ ConstantOperand $ C.GlobalReference funty label


externInline
  :: MonadModuleBuilder m
  => Name   -- ^ Definition name
  -> [Type] -- ^ Parameter types
  -> Type   -- ^ Type
  -> m Operand
externInline nm argtys retty = do
  emitDefn $ GlobalDefinition functionDefaults
    { name        = nm
    , linkage     = External
    , parameters  = ([Parameter ty (mkName "") [] | ty <- argtys], False)
    , functionAttributes = [Right FA.AlwaysInline]
    , returnType  = retty
    }
  let funty = ptr $ FunctionType retty argtys False
  pure $ ConstantOperand $ C.GlobalReference funty nm




globalConst
  :: MonadModuleBuilder m
  => Name       -- ^ Variable name
  -> Type       -- ^ Type FIXME: elinate with typeof 
  -> C.Constant -- ^ Initializer
  -> m Operand
globalConst nm ty initVal = do
  emitDefn $ GlobalDefinition globalVariableDefaults
    { name                  = nm
    , LLVM.AST.Global.type' = ty
    , linkage               = Internal
    , initializer           = Just initVal
    , isConstant            = True
    }
  pure $ ConstantOperand $ C.GlobalReference (ptr ty) nm


externGlobal :: MonadModuleBuilder m
       => Name       
       -> Type       
       -> m Operand
externGlobal nm ty = do 
  emitDefn $ GlobalDefinition globalVariableDefaults
    { name                  = nm
    , LLVM.AST.Global.type' = ty
    , linkage               = External
    , initializer           = Nothing
    }
  pure $ ConstantOperand $ C.GlobalReference (ptr ty) nm
