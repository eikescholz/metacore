module BuiltIns where
import AST


baseBinOp opNam tyNam 
  =  DefAbstr (opNam++" : "++tyNam++" -> "++tyNam++" -> "++tyNam) [
       Suspend 
      ,BeginScope 3
      ,BindDst
      ,BindArgs 2
      ,LLVMBinOp opNam tyNam 0 1 2
      ,EndScope 3
      ,Yield
      ]


baseNumOps tyNam 
  = [ baseBinOp "add" tyNam
    , baseBinOp "sub" tyNam
    , baseBinOp "mul" tyNam
    , baseBinOp "sdiv" tyNam
    , baseBinOp "smod" tyNam
    , baseBinOp "udiv" tyNam
    , baseBinOp "umod" tyNam
    ] ++ baseLogicOps tyNam -- llvm logic is bit wise bool is 1-bit number

baseLogicOps tyNam
  = [ baseBinOp "and" tyNam
    , baseBinOp "or"  tyNam
    , baseBinOp "xor" tyNam
    ]
    

builtinConstants :: [RfDef]
builtinConstants 
  =  baseNumOps "Word"
  ++ baseNumOps "Byte"
  ++ baseLogicOps "Bool"   
 
   