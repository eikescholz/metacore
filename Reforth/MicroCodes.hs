{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
module MicroCodes where

import LLVM.IRBuilder.Module
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant
import LLVM.AST.Typed

import qualified LLVM.AST.Constant as C
import qualified LLVM.AST.Operand as O
import qualified LLVM.AST.IntegerPredicate as IP

import LLVM.AST hiding (function,functionAttributes)

import LLVM.AST.Global
import LLVM.AST.FunctionAttribute as FA
import LLVM.Prelude
import LLVM.AST.ParameterAttribute
import LLVM.AST.Type as AST

import Control.Monad.State.Lazy

import Data.List 
import Data.String

import qualified LLVM.Internal.FFI.DataLayout as FFI

import LLVMUtils
import AST
import Compiler
import Debug.Trace



mcBuilderInitState nextCont =
  let n0 = ConstantOperand (C.IntToPtr (C.Int 64 0 ) (ptr i8))
      n1 = ConstantOperand (C.IntToPtr (C.Int 64 0 ) (ptr (ptr i8)))
   in MCBuilderState n0 n0 n0 n1 n0 n0 n0 n0 nextCont



rfValueAddrMC :: RfValue -> MCBuilder Operand
rfValueAddrMC v = do
  wordType <- getWordTypeMC  
  nullWord <- mkWordConstMC 0
  let fs = fromString
      aty = ptr rfAbstrType
      wty = ptr wordType
      tty = ptr rfTypeValueType
 
  v' <- case v of
           (Unit) ->
             return $ O.ConstantOperand $ C.IntToPtr nullWord (ptr i8)            
           (Abstr s) -> 
             return $ O.ConstantOperand $ C.GlobalReference aty (fs s)
           (Word s) ->
             return $ O.ConstantOperand $ C.GlobalReference wty (fs s) 
           (TypeConst s) ->
             return $ O.ConstantOperand $ C.GlobalReference tty (fs s) 
           (ScopeVar i) -> 
             scopeVarAddrMC i
  bitcast (v') (ptr i8)


rfValueTypeAddrMC :: RfValue -> MCBuilder Operand
rfValueTypeAddrMC v = do
  nullWord <- lift $ lift $ mkWordConst 0
 
  let fs = fromString
      tty = ptr rfTypeValueType
 
  v' <- case v of 
          (Unit) ->
             return $ O.ConstantOperand $ C.IntToPtr nullWord (ptr i8)            
          (Abstr s) -> 
            return $ O.ConstantOperand $ C.GlobalReference tty (fs "Abstr") 
          (Word s) ->
            return $ O.ConstantOperand $ C.GlobalReference tty (fs "Word") 
          (TypeConst s) ->
            return $ O.ConstantOperand $ C.GlobalReference tty (fs "Type") 
          (ScopeVar i) -> 
            scopeVarTypeAddrMC i
  bitcast (v') (ptr i8)



-- like push tmp but without bitcast clutter
pushContMC :: Operand -> MCBuilder ()
pushContMC c = do
  st <- get 
  csp1 <- gep (csp st) [int64 (-1)] `named` "csp"
  store csp1 0 c 
  put $ st { csp = csp1 }




popContMC :: MCBuilder Operand
popContMC = do 
  st <- get
  cont <- load (csp st) 0 `named` "popedCont"
  store (csp st) 0 (O.ConstantOperand (C.Undef (ptr i8)))
  csp1 <- gep (csp st) [int64 (1)] `named` "csp"
  put $ st { csp = csp1 }
  return cont



gotoMC :: Operand -> MCBuilder ()
gotoMC cont = do
  st <- get
  f  <- if (typeOf cont == rfContType) 
          then return cont
          else bitcast cont rfContType `named` "nextCont"
  call f [ (asp st,[NoAlias])
         , (ssp st,[NoAlias])
         , (rsp st,[NoAlias])
         , (csp st,[NoAlias])
         , (rvp st,[])
         , (cap st,[])
         , (avp st,[])
         , (atp st,[])
         ] `named` "exitCode"
  return ()



curIPNameMC :: MCBuilder String
curIPNameMC = lift $ lift $ curIPName

nextInstrMC :: MCBuilder () 
nextInstrMC = do
  nextIP <- lift $ lift $ withNextInstrState curIP
  gotoMC $ O.ConstantOperand $ nextIP




getValueSizeOfMC :: Operand -> MCBuilder Operand
getValueSizeOfMC typeValueAddr = do
  wordType <- getWordTypeMC 
  let funty = ptr $ FunctionType wordType [ptr i8] False  
      funName = "valueSizeOf"
      valueSizeOf = ConstantOperand $ C.GlobalReference funty funName 
  call valueSizeOf [(typeValueAddr,[])]

markMemUndefMC :: Operand -- Addr
               -> Operand -- Number of bytes
               -> MCBuilder Operand
markMemUndefMC addr len = do
  wordType <- getWordTypeMC 
  let funty = ptr $ FunctionType AST.void [ptr i8,wordType] False  
      funName = "markMemUndef"
      markMemUndef = ConstantOperand $ C.GlobalReference funty funName 
  call markMemUndef [(addr,[]),(len,[])]




-- save The frame addrs as first addr of the frame
scopeFrameAddrAddrMC :: MCBuilder  Operand
scopeFrameAddrAddrMC = do
  st <- get  
  gep (csp st) [int64 0] `named` (fromString ("svFrameAddrAddr"))


scopeVarAddrAddrMC :: ScopeVarId 
               -> MCBuilder  Operand
scopeVarAddrAddrMC i = do
  st <- get  
  gep (csp st) [int64 (2*i)] `named` (fromString ("sv"++show i++"AddrAddr"))

scopeVarTypeAddrAddrMC :: ScopeVarId 
               -> MCBuilder  Operand
scopeVarTypeAddrAddrMC 0 = error "the return addr has no provided type" 
scopeVarTypeAddrAddrMC i = do
  st <- get  
  let offset = int64 (2*i + 1 )
  gep (csp st) [offset] `named` (fromString ("svType"++show i++"AddrAddr"))


scopeVarAddrMC :: ScopeVarId 
           -> MCBuilder  Operand
scopeVarAddrMC i = do
  addr <- scopeVarAddrAddrMC i
  load addr 0 `named` (fromString ("sv"++show i++"Addr"))
  
scopeVarTypeAddrMC :: ScopeVarId 
           -> MCBuilder  Operand
scopeVarTypeAddrMC i = do
  addr <- scopeVarTypeAddrAddrMC i
  load addr 0 `named` (fromString ("svType"++show i++"Addr"))


bindValueMC :: ScopeVarId -- scopeVarId to bind
           -> Operand -- addr of the value
           -> MCBuilder ()
bindValueMC i valAddr  = do
   valAddrAddr <- scopeVarAddrAddrMC i 
   store valAddrAddr 0 valAddr
 

bindTypeMC :: ScopeVarId -- scopeVarId to bind
           -> Operand -- addr of the type
           -> MCBuilder ()
bindTypeMC i typAddr = do
   typAddrAddr <- scopeVarTypeAddrAddrMC i 
   store typAddrAddr 0 typAddr
 
bindMC :: ScopeVarId -- scopeVarId to bind
           -> Operand -- addr of the value
           -> Operand -- addr of the type
           -> MCBuilder ()
bindMC i valAddr typAddr = do
   bindValueMC i valAddr
   bindTypeMC i typAddr

tailevalMC :: MCBuilder ()
tailevalMC = do 
  st <- get
  cap' <- bitcast (cap st) (ptr (ptr i8)) `named` "cap"
  newCont <- load cap' 0  `named` "newCont"
  gotoMC newCont

internalEvalMC :: Operand -> MCBuilder ()
internalEvalMC retCont = do  
  retCont' <- bitcast retCont (ptr i8) `named` "retCont"
  pushContMC retCont'       
  tailevalMC 

evalMC :: MCBuilder ()
evalMC = do
  retCont <- lift$ lift $ withNextInstrState curIP
  internalEvalMC $ ConstantOperand retCont






applyMC :: ScopeVarId 
        -> ScopeVarId
        -> ScopeVarId
        -> MCBuilder ()
applyMC rvp abs arg = do 
  rvpAddr <- scopeVarAddrMC rvp `named` "rvpAddr"
  absAddr <- scopeVarAddrMC abs `named` "absAddr"
  argAddr <- scopeVarAddrMC arg `named` "argAddr"
  typAddr <- scopeVarTypeAddrMC arg `named` "typAddr"
  modify $ \ st -> st {  rvp = rvpAddr, cap = absAddr, 
                         avp = argAddr, atp = typAddr } 
 



llvmBinOpMC :: String -- a name to emitted
          -> Type   -- need to cast back
          -> (Operand -> Operand -> IRBuilderT (ModuleBuilderT LLVMIO) Operand)
          -> ScopeVarId 
          -> ScopeVarId
          -> ScopeVarId
          -> MCBuilder ()
llvmBinOpMC nam ty op  dst arg1 arg2 = do 
   arg1' <- scopeVarAddrMC arg1
   arg2' <- scopeVarAddrMC arg2
   dst'  <- scopeVarAddrMC dst
   v1Addr <- bitcast arg1' (ptr ty) `named` "v1Addr"
   v2Addr <- bitcast arg2' (ptr ty) `named` "v2Addr"
   v3Addr <- bitcast dst'  (ptr ty) `named` "v3Addr"
   v1 <- load v1Addr 0  `named` "val1"
   v2 <- load v2Addr 0  `named` "val2"
   v3 <- lift $ op v1 v2  `named` "val3"
   store v3Addr 0 v3 



   
bindArgsMC :: ScopeVarId -> ScopeVarId -> MCBuilder ()
bindArgsMC curArg maxArg = do
  st <- get
  if curArg > 1
    then do
      bindMC curArg (avp st) (atp st)  
      let prefix = "arg"++show curArg++"Abstr"
      argAbstrAddr <- bitcast (cap st) (ptr (ptr i8)) 
                       `named` fromString (prefix++"Addr")
      -- load update for acp  
      argAbstrPrevAbstrAddrAddr 
        <- gep argAbstrAddr [int64 1] 
            `named` fromString (prefix++"PrevAbstrAddrAddr")
      argAbstrPrevAbstrAddr 
        <- load argAbstrPrevAbstrAddrAddr 0 
             `named` fromString (prefix++"PrevAbstrAddr")
      -- load update for avp 
      argAbstrPrevArgAddrAddr 
        <- gep argAbstrAddr [int64 2] 
             `named` fromString (prefix++"PrevArgAddrAddr")
      argAbstrPrevArgAddr 
        <- load argAbstrPrevArgAddrAddr 0
             `named` fromString (prefix++"PrevArgAddr")
     -- load update for atp 
      argAbstrPrevArgTypeAddrAddr 
        <- gep argAbstrAddr [int64 3] 
             `named` fromString (prefix++"PrevArgTypeAddrAddr")
      argAbstrPrevArgTypeAddr 
        <- load argAbstrPrevArgTypeAddrAddr 0
             `named` fromString (prefix++"PrevArgTypeAddr")
      put $ st { cap = argAbstrPrevAbstrAddr, 
                 avp = argAbstrPrevArgAddr,
                 atp = argAbstrPrevArgTypeAddr } 
      bindArgsMC (curArg-1) maxArg
    else do 
      bindMC 1 (avp st) (atp st) -- most innedValue :: String -> ScopeVarId -> RfValue -> CompData-> RfBuilder C.Constan



debugPrintfMC :: String -> [Operand] -> MCBuilder ()
debugPrintfMC fmtStr vaArgs = do 
  dm <- lift $ lift $ lift $ getDebugMode
  case dm of
    InternalDebug -> errPrintfMC fmtStr vaArgs >> return ()
    _             -> return()

debugPrintLnMC :: String -> MCBuilder ()
debugPrintLnMC s = do 
  dm <- lift $ lift $ lift $ getDebugMode
  case dm of
    InternalDebug -> errPrintLnMC s >> return ()
    _             -> return()


errPrintLnMC :: String -> MCBuilder Operand
errPrintLnMC s = do
  globStrNam <- lift $ lift $ lift $ getUniqueGlobalName
  s' <- globalStringPtr s globStrNam
  errPrintfMC "%s\n" [ConstantOperand s']
 
errPrintfMC :: String -> [Operand] -> MCBuilder Operand
errPrintfMC fmtStr vaArgs = do 
  let funty = ptr $ FunctionType i32 [i32,ptr i8] True  
      funName = mkName $ "dprintf"
      dprintf = ConstantOperand $ C.GlobalReference funty funName
  let errfd = int32 2
  globStrNam <- lift $ lift $ lift $ getUniqueGlobalName
  s <- globalStringPtr fmtStr globStrNam
  call dprintf ( (errfd,[]):(ConstantOperand s,[]) : map (\arg->(arg,[])) vaArgs)



getWordTypeMC :: MCBuilder Type
getWordTypeMC  =  lift $ lift $ getWordType
 
mkWordConstMC n = lift $ lift $ mkWordConst n

getAllocSizeOfMC :: Type -> MCBuilder C.Constant
getAllocSizeOfMC ty = lift $ lift $ getAllocSizeOf ty  





--mkEntryPointMC :: Integer -> ModuleBuilderT LLVMIO ()
initialEntryPoint :: Integer -> String -> RfBuilder ()
initialEntryPoint rfStackSize moduleName = do

  abort <- extern "abort" [] VoidType
  exit  <- extern "exit" [i32] VoidType

  externVarArgs "dprintf" [i32,ptr i8] i32

  wordType <- getWordType
 
  wordTypeSize <- getAllocSizeOf wordType
  unitTypeSize <- mkWordConst 0
  abstrTypeSize <- getAllocSizeOf rfAbstrType

  zeroWord <- mkWordConst 0

  unitTypeTag <- mkWordConst 0  
  wordTypeTag <- mkWordConst 1
  abstrTypeTag <- mkWordConst 2

  wordValueZero <- mkWordConst 0
  wordValueOne  <- mkWordConst 1
 
  -- Define initial type constants
  
  unitTypeVal <- globalConst "Unit" rfTypeValueType
                                     (C.Array (ptr i8) 
                                        [C.IntToPtr unitTypeTag (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8) ] )


  wordTypeVal <- globalConst "Word" rfTypeValueType
                                     (C.Array (ptr i8) 
                                        [C.IntToPtr wordTypeTag (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0) (ptr i8)] )

  abstrTypeVal <- globalConst "Abstr" rfTypeValueType
                                     (C.Array (ptr i8) 
                                        [C.IntToPtr abstrTypeTag (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0)  (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0)  (ptr i8)
                                        ,C.IntToPtr (C.Int 64 0)  (ptr i8)] )



  -- TODO: Make a posix compat layer, use mmap and "tricks" to give
  --       memory back to the system, compat layer is needed to transform
  --       the posix signal system into a more clean form of io handling. 


  let rfMemType = (ArrayType (fromInteger (4*rfStackSize)) i8)


  rfMem <- global "rfMem" rfMemType (C.Undef rfMemType)

{- 
  -- a place to store CPS registers for JIT and maybe extern calls
  rfRegisters <- global "rfRegisters" rfJITRegisterSetType 
                                       (C.Undef rfJITRegisterSetType)
-}

  -- global references are pointers to the provided value
  let mainAbstr = ConstantOperand $ C.GlobalReference (ptr rfAbstrType) 
                                                       (fromString moduleName)

  valueSizeOf <- function "valueSizeOf" [(ptr i8,"typeValAddr")] wordType $ 
     \ [typeValAddr] -> mdo 
        block `named` "entry"; do
          tagAddr <- bitcast typeValAddr  (ptr wordType)
          tag <- load tagAddr 0 `named` "tag"
          switch tag (Name "OfInvalidType") -- escape 
                      [ (unitTypeTag,  Name "OfUnit"),
                        (wordTypeTag,  Name "OfWord"),
                        (abstrTypeTag, Name "OfAbstr")]     
        emitBlockStart  (Name  "OfInvalidType"); do -- should be unreachable
          call abort [] 
          unreachable
        emitBlockStart (Name "OfUnit"); do
          ret $ O.ConstantOperand $ unitTypeSize
        emitBlockStart (Name "OfWord"); do
          ret $ O.ConstantOperand $ wordTypeSize
        emitBlockStart (Name "OfAbstr"); do
          ret $ O.ConstantOperand $ abstrTypeSize          


-- a memcopy, with proper machine word lentgh size, also
-- the memcpy intrinsics confuse some opt passes as it seems 

  inlineFunction "rawmemcpy" [ (ptr i8,"dst"),
                         (ptr i8,"src"),
                         (wordType,"len")   ] AST.void $ 
     \ [dst,src,len] -> mdo 

        block `named` "entry"; do
          done <- icmp IP.EQ len (O.ConstantOperand wordValueZero)
          condBr done (Name "Done") (Name "CopyByte") 


        emitBlockStart (Name "Done"); do 
          retVoid

        emitBlockStart (Name "CopyByte"); do

          byte <- load src 0
          store dst 0 byte
          dst' <- gep dst [O.ConstantOperand wordValueOne]
          src' <- gep src [O.ConstantOperand wordValueOne]
          len' <- sub len (O.ConstantOperand wordValueOne)
   
          let fty = ptr $ FunctionType AST.void [ptr i8,ptr i8,wordType] False  
              fName = mkName $ "rawmemcpy"
              thismemcpy = ConstantOperand $ C.GlobalReference fty fName 
          call thismemcpy [(dst',[]), (src',[]), (len',[])]
          retVoid


  -- used for dead storage elimination, this should not create runtime code 
  inlineFunction "markMemUndef" [ (ptr i8,"dst"),
                                  (wordType,"len")   ] AST.void $ 
     \ [dst,len] -> mdo 

        block `named` "entry"; do
          done <- icmp IP.EQ len (O.ConstantOperand wordValueZero)
          condBr done (Name "Done") (Name "SetByteUndef")

        emitBlockStart (Name "Done"); do 
          retVoid

        emitBlockStart (Name "SetByteUndef"); do

          store dst 0 (O.ConstantOperand (C.Undef i8))
          dst' <- gep dst [O.ConstantOperand wordValueOne]
          len' <- sub len (O.ConstantOperand wordValueOne)
   
          let fty = ptr $ FunctionType AST.void [ptr i8,wordType] False  
              fName = mkName $ "markMemUndef"
              thisFun = ConstantOperand $ C.GlobalReference fty fName 
          call thisFun [(dst',[]), (len',[])]
          retVoid



 
  haltCont <- function "Halt" rfContArg AST.void $ 
    \ [asp,ssp,rsp,csp,rvp,cap,avp,atp] -> mdo 
      entry <- block `named` "entry"; do
        exitCodeAddr <- bitcast rvp  (ptr i64) `named` "exitCodeAddr"
        exitCode <- load exitCodeAddr  0  `named` "exitCode"
        store exitCodeAddr 0 (O.ConstantOperand (C.Undef i64)) -- mark free

        -- drop undef cont from evalAbst initState
        csp1 <- gep csp [int64 (1)] `named` "csp"
        store csp1 0 (O.ConstantOperand (C.Undef (ptr i8)))

        ec0 <- inttoptr exitCode (ptr i8)
        exitCode' <- ptrtoint ec0 i32

        call exit [(exitCode',[])]

        retVoid 

 
  -- the LLVM entry point for compilers 
  function "main" [] i64 $ \ [] -> mdo 
    entry <- block `named` "entry"; do
      -- compute initial registers 
      let beginOffset = int64 0
      let midOffset = int64 (2*rfStackSize)
      let endOffset = int64 (4*rfStackSize)
      mem <- bitcast rfMem (ptr i8)
      asp0 <- gep mem [beginOffset] `named` "asp"
      ssp0 <- gep mem [midOffset]  `named` "ssp"
      rsp0 <- gep mem [midOffset] `named` "rsp"
      endp <- gep mem [endOffset]
      csp0 <- bitcast endp (ptr (ptr i8)) `named` "csp"

      rvp0 <- inttoptr beginOffset (ptr i8) -- set null
      cap0 <- inttoptr beginOffset (ptr i8) -- set null
      avp0 <- inttoptr beginOffset (ptr i8) -- set null
      atp0 <- inttoptr beginOffset (ptr i8) -- set null

      avp0 <- inttoptr (int64 0) (ptr i8)
      atp0 <- inttoptr (int64 0) (ptr i8)

      cap0 <- bitcast mainAbstr (ptr i8) -- next eval calls entry (named after module) 
      
      rvp2 <- bitcast rsp0 (ptr i64) -- use result stack for exit code
      rsp3 <- gep rvp2 [int64 1]  -- allocate exit code word
      rsp4 <- bitcast rsp3 (ptr i8) -- points to new free mem    
     
      store rvp2 0 (int64 (-1)) -- init retval (-1 for debugging) 
 
      let initState 
           = MCBuilderState asp0 ssp0  rsp4 csp0  -- initial stack registers
                     rsp0 cap0  avp0 atp0  -- initial abstraction   
                       (C.Undef rfContType)  -- not used
          
      evalStateT (internalEvalMC haltCont) initState

      store rvp2 0 (O.ConstantOperand (C.Undef i64))
      
      ret (int64 0) -- will not return here "Halt" terminates with exit
 
  return ()

