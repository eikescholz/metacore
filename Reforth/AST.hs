--  A Reforth AST module

module AST where
import qualified Data.Map.Lazy as Map
import qualified LLVM.AST.Constant as C
import LLVM.AST.Type
import LLVM.AST.Operand
import Data.Either 
import Data.Word
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module


type ScopeVarId = Integer 

type RfModule = [RfDef]

data RfValue = Abstr String
             | Word String
             | Unit 
             | TypeConst String
             | ScopeVar Integer  
  deriving (Eq)

instance Show RfValue where
  show (Abstr s)     = s
  show (Word s)      = s
  show (ScopeVar i)  = show i  
  show (TypeConst s) = s
  show (Unit)        = "()"


-- A string with the whole purpose of not showing quotes with show  
newtype KW = KW String

instance Show KW where
   show (KW s) = s


type RfTypeValue = RfValue 

data RfDef 
  = DefWord  String Integer       
  | DefAbstr String [RfInstr]

data RfInstr
  = BeginScope Integer
  | EndScope   Integer
  | Yield 
  | Taileval
  | Suspend  
  | BindDst
  | BindDstType RfValue
  | BindValue ScopeVarId RfValue
  | BindType ScopeVarId RfValue 
  | BindArgs Integer
  | Bind ScopeVarId RfValue
  | BindResultAlloc ScopeVarId RfValue 
  | DeallocResult RfValue   
  | Copy ScopeVarId ScopeVarId
  | Apply ScopeVarId ScopeVarId ScopeVarId
  | Eval
  | Switch ScopeVarId [(Integer,[RfInstr])] [RfInstr]
  --- Not actual instructions -- used to build predefined abstractions
  | LLVMBinOp String String ScopeVarId ScopeVarId ScopeVarId
  | LLVMUnaryOp String String ScopeVarId ScopeVarId 

-- We do not want to have the full body in the name
-- of the switch instruction, so we use our own instance
-- TODO: rename, since breaks usual id == show . read iirc

instance Show RfInstr where
  show (BeginScope i)        = "BeginScope "++show i 
  show (EndScope i)          = "EndScope "++show i
  show (Yield)               = "Yield"
  show (Taileval)            = "Taileval"
  show (Suspend)             = "Suspend"
  show (BindDst)             = "BindDst"
  show (BindDstType i)       = "BindDstType "++show i
  show (BindValue i v)       = "BindValue "++show i++" "++show v
  show (BindType i v)        = "BindType "++show i++" "++show v
  show (BindArgs i)          = "BindArg "++show i
  show (Bind i v)            = "Bind "++show i++" "++show v
  show (BindResultAlloc i v) = "BindResultAlloc "++show i++" "++show v
  show (DeallocResult v)     = "DeallocResult "++show v
  show (Copy i j)            = "Copy "++show i++" "++show j
  show (Apply y f x)         = "Apply "++show y++" "++show f++" "++show x
  show (Eval)                = "Eval"
  show (Switch i _ _)        = "Switch "++show i
  show (LLVMBinOp n t y a b) 
    = "LLVM "++n++" "++t++" "++(show y)++" "++(show a)++" "++(show b)
  show (LLVMUnaryOp n t y a) 
    = "LLVM "++n++" "++t++" "++(show y)++" "++(show a)

-- IP is for instruction pointer (as index into an array)
data CompState = CompState { }




