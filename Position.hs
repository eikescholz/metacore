{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
module Position where
import Data.Data
import Data.Maybe
import Data.Generics.Uniplate.Operations
import Data.Generics.Uniplate.Data

data Pos = Pos (Int,Int,String) -- (Line,Column,File)
 deriving (Show,Eq,Data,Typeable)         

type SubStr = (Pos,String)


genericPos v 
  = case childrenBi v of
       (p:_) -> p :: Pos
       []    -> error ("Error: applied generic pos to something that has no Pos member. Constructor: " ++ show (toConstr  v))

{-
genericPos v = case catMaybes (gmapQ cast v :: [Maybe Pos]) of
                 (p:_) -> p
                 []    -> error "Error: applied generic pos to something that has no Pos member"

genericSubStrPos v 
  = case catMaybes (gmapQ cast v :: [Maybe SubStr]) of
      ((p,s):_) -> p
      []    -> error $ "Error: applied generic pos to something that has no Pos member. Constructor: "++ (show $ toConstr  v)
-}



class (Data a,Typeable a) => HasPos a where
  pos :: a -> Pos
  pos = genericPos

newPos :: String -> Pos
newPos f = (Pos (1,1,f))

newLn :: Pos -> Pos
newLn (Pos (l,c,f)) = Pos (l+1,1,f)

incCol :: Pos -> Pos 
incCol (Pos (l,c,f)) = Pos (l,c+1,f)

addCol :: Pos -> Int -> Pos
addCol (Pos (l,c,f)) n = Pos (l,c+n,f) 

getLn :: Pos -> Int
getLn (Pos (l,c,f)) = l

getCol :: Pos -> Int
getCol (Pos (l,c,f)) = c

eofPos = Pos (0,0,"")

lnCl l c = Pos (l,c,"") -- For creating test cases mostly 

instance HasPos Pos where
 pos = id

 
subStrStrs :: [SubStr] -> [String]
subStrStrs es = map (\ (_,s) -> s ) es

strfy (Pos (0,0,"")) = "EoF"
strfy (Pos (l,c,f))  = f ++ ":" ++ show l ++ ":" ++ show c 

col t = getCol (pos t)
ln  t = getLn  (pos t)  

perror p s = error ((show p) ++ ": " ++ s)      

