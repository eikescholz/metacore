module RunTests where
import TestBase
import LayoutTests
import ScannerTests
import LexerTests
import ParserTests

runAllTests = do runTestSuite (
                      layoutTests 
                   ++ scannerTests 
                   ++ lexerTests
                   ++ parserTests
                  )


