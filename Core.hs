{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{- The core Language 
 
   First IR language generated from meta processor / type checking
   does some desugaring most note-able

   do let foo = bar 
      bla
      fasel 

is read as
 
do let foo = bar
    in do bla 
          fasel 


 -}

module Core where
import qualified Data.Map as MAP
import Position
import qualified IdentMap as IM
import qualified Data.Word as DW
import Grammar
import Control.Monad.Trans.State.Lazy
import MetaCore
import Data.Data
import Data.Generics.Uniplate.Data

type Region = String
type ModPath = [String]


data TyConSort 
  = InterfaceCon
  | SubtypeCon
  | StructCon
  | UnionCon
  deriving (Show,Data,Typeable) 

type Type = MExpr -- Actually only a subset. 

data CoreInfo = CoreInfo { position :: Pos 
                         , coreType :: Type }
  deriving (Show,Data,Typeable)

data Cases = Cases [ ( [Pat]         -- the patterns 
                     , [ ( Maybe Expr -- the condition guards
                         , Expr )])]    -- result expressions.
                   CoreInfo    
  deriving (Show,Data,Typeable)

data Decl = Decl Pos String Type CoreInfo
  deriving (Show,Data,Typeable)

type WhereDecl = Decl

data Bind = Bind     Pat Expr CoreInfo
  deriving (Show,Data,Typeable)

data Expr = App Expr Expr CoreInfo
          | AtMeth Expr String CoreInfo
          | Match Expr Cases CoreInfo
          | Tuple [Expr] CoreInfo
          | Array [Expr] CoreInfo -- e.g. {1,2,3,4}
          | Word  DW.Word CoreInfo
          | Var   IM.UIdent CoreInfo
          | Con   IM.UIdent CoreInfo
          | Abstr IM.UIdent Type Region Expr CoreInfo
          | Unit  CoreInfo
          | Do [Stmt] CoreInfo
          -- the binds are for decls and the where-scope-decls
          | Let       Region [Decl] [WhereDecl] [Bind] Expr CoreInfo
          | LetGlobal Region [Decl] [WhereDecl] [Bind] Expr CoreInfo
          | LetShunt  Region [Decl] [WhereDecl] [Bind] Expr CoreInfo
   deriving (Show,Data,Typeable)


data Pat = PatApp Expr Expr CoreInfo
         | PatTuple [Expr] CoreInfo
         | PatArray [Expr] CoreInfo -- e.g. {1,2,3,4}
         | PatWord  DW.Word CoreInfo
         | PatVar   IM.UIdent CoreInfo
         | PatCon   IM.UIdent CoreInfo
         | PatUnit  CoreInfo
  deriving (Show,Data,Typeable)

data Stmt = Expr Expr        
  deriving (Show,Data,Typeable)


coreInfo a = let cs = childrenBi a :: [CoreInfo]
              in head cs
      