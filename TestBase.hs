module TestBase where
-- since this will pe ported to metacore itself no unittesting library is used
-- and as in the Parser Combinators case somthing sufficient is provided ...
-- so Trying to siple *spect type testing suite
data Test      = It String (IO TestResult)
data TestCase  = Describe String [Test]

type TestSuite = [TestCase]

data TestResult = TestSuccess
                | TestFailure String

maxcols   = 80 -- in memory of the old termnals ...

runTestSuite :: TestSuite -> IO ()
runTestSuite testCases = do 
   let m = countTests testCases
   putStrLn ""
   n <- run testCases
   putStrLn ("\n" ++ take (maxcols-1) (repeat '-'))
   if (n==0) 
     then putStrLn ("\nALL OK ("++show n++" Failures, "++show (m-n)++" Successes)\n") 
     else putStrLn ("\nFAILED ("++show n++" Failures, "++show (m-n)++" Successes)\n")
  where
    run []     = return 0
    run (Describe s ts:tcs) = do 
      putStrLn s 
      n <- runTests ts 
      m <- run tcs
      return (n+m)
    countTests []        = 0
    countTests (Describe _ ts:cts) = length ts + countTests cts
 

runTests [] = do putStrLn ""
                 return 0
runTests (It s test:ts) = do
  let prefix    = "    "++(take (maxcols -length(ok) -6) s)++" "
      ok        = "....... OK"
      failed    = "... FAILED"
      dotcount  = maxcols - length(prefix) - length(failed) -1 -- -1 for the newline char
      dots      = take dotcount (repeat '.')
  putStr (prefix++dots)   
  success <- test
  case success of
    TestSuccess   -> putStrLn ok >> runTests ts
    TestFailure s -> do putStrLn failed 
                        putStrLn ("\n"++s)
                        n <- runTests ts 
                        return (n+1)

evalsTo x y = do 
  a <- x
  b <- y 
  if (a == b)
    then return TestSuccess
    else return (TestFailure ("Condition `evalsTo` failed!"
                                      ++ "\n\n LHS: "++show a
                                      ++ "\n\n RHS: "++show b))
 
