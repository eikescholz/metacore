{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TypeSynonymInstances #-}
module CoreNumbers where
import Position
import Data.Word
import Data.Typeable
import Data.Data
import Data.Char
import Debug.Trace


-- Value Representations, no suffix analysis is done in the scanner and does not
-- need to be represented, in this structure
-- consider inclusion of predefined word in type floats up to 64Byte, i.e. 
-- a common "safe" random nonce (birthday theorem), or
-- the interaces for Word8 and Word as alias to the target architecure word 
-- are predefined. Other interfaces must included to be used for example as System::Word32
-- (To avoid binary bloat)
-- Signed integers are removed from specification, since they seem to trigger 
-- integer overun logic problems. A word as modulus class has natural negative representants
-- but no natural odering. 


data CoreNumber = CNWord8  Word8  
                | CNWord16 Word16  --Minimal Supported Word size
                | CNWord32 Word32
                | CNWord64 Word64
                | CNWord128 Integer 
                | CNWord256 Integer -- Maximal supportable default word Size
                | CNWord512 Integer -- a safe nonce              
                | CNFloat32  Float   
                | CNFloat64  Double
                | CNFloat128 Rational -- 
                | CNFloat256 Rational -- maximal supportable float size
                | CNInteger  Integer  -- postfix L (python style for long number)
                | CNRational Rational -- either like 2L/5L or like 0.4L 
  deriving (Show,Eq,Data,Typeable) 

 

scanCoreNumber :: (String -> CoreNumber) -- default word, WordPtr of target arch
               -> (String -> CoreNumber) -- default float, longest hardware suppoted floating point
               -> Pos -> String -> (CoreNumber,Pos,String)
scanCoreNumber mkWord mkFloat p cs 
  = case span isDigit cs of 
      (str,'.':c':cs') | isDigit c' 
        ->  let (str',cs'') = span isDigit (c':cs') 
                str'' = str++"."++str' 
             in case cs'' of 
                  ('L':        cs''') -> (CNRational (read str''), addCol p (length str+1),cs''')
                  ('f':'3':'2':cs''') -> (CNFloat32  (read str''), addCol p (length str+3),cs''')
                  ('f':'6':'4':cs''') -> (CNFloat64  (read str''), addCol p (length str+3),cs''')
                  cs'''               -> (mkFloat    str''       , addCol p (length str  ),cs''')
      (str,cs')  
        -> case cs' of 
            ('L':        cs''') -> (CNInteger (read str), addCol p (length str+1),cs''')
            ('w':'8':    cs''') -> (CNWord8   (read str), addCol p (length str+3),cs''')
            ('w':'1':'6':cs''') -> (CNWord16  (read str), addCol p (length str+3),cs''')                
            ('w':'3':'2':cs''') -> (CNWord32  (read str), addCol p (length str+3),cs''')
            ('w':'6':'4':cs''') -> (CNWord64  (read str), addCol p (length str+3),cs''')
            cs'''               -> (mkWord    str       , addCol p (length str  ),cs''')


  


     