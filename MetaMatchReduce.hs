{- 
  Replacement for MetaEval and MetaPatternmatch. 

  Thinking of the meta-level as primarily a lambda calculus is 
  confusing, it differs in to many details. 
 
  It is a kind of template processor that does matching
  and reduction expressions. Reduction is a 
  sfinae combination of substitution and constant merging.
  (For substition failures are detected by match,
   patterns that don't match fail on substitution)

  
  match O I 
 
     Where O is a pattern of an ought-type and I is an is-type.
    
     if not throwing an error the state contains bindings so that 
     so that using the bindings 
       x : I implies x : O   

  X' <- reduce X :
    
     apply bindings and simplfify with sfnai and attach typing
     information of reduced expressions. 

For example type checking of 

"""
 let fdom : A => B -- location/ought type
     farg : X => Y -- (pseudo) value/is type 
   where
     fdom = farg -- every x in A must be an x in X 
                 -- and every y in Y must be an y in B
"""

  is done with (pseudocode)

  match (A => B) (X => Y) = do 
      match X A      -- value used in fdom makes A the is type the argument.
      Y' <- reduce Y -- compute type result (using the bindings for X)
      B' <- reduce B -- compute type result of B = ((A => B) A)
      match B' Y'    -- test if result types are compatible. 
       
  under the assumption that variable collisions are alpha conversed away.    
  (TODO: Check if domain should be reduced ? )

    match ($a,$a) (NonZero $b,$b) 
      internally creates the constraints of the form
      oght-type := is-type, in particular. 

      $a := $b
      $a := NonZero $b

      so that the type for $a containing all possible is-values is 
      $a := $b$ `cup` NonZero $b

     (That actually seems to be a form of a principal type)

-}

module MetaMatchReduce where
import MetaCore
import qualified Data.Map as Map
import qualified IdentMap as IM
import qualified Data.Set as Set
import Control.Monad.Trans.State.Lazy
import Control.Monad.Trans.Class
import Control.Monad.Except
import MetaAlphaConversion
import PPrint
import Position
import Data.Data hiding (typeOf)
import Data.Generics hiding (typeOf)
import Data.Generics.Uniplate.Operations
import Text.PrettyPrint
import qualified Text.PrettyPrint as PP
import StructuralTypeOperators

-- only valid on reduced expressions
-- extracts the type computed by reduce
typeOf :: SExpr -> MType 
typeOf (MVar _ _ (OfTy t)) = t
typeOf (MCon _ _ (OfTy t)) = t
typeOf (MArray _ _ (OfTy t)) = t

typeOf _ = error "INERNAL ERROR: Invalid use of typeOf or unimplemented case"

-- raw match monad uses the current principal type of an identifier as state. 
type RMM = StateT (Map.Map IM.ValueIdent MExpr) EMT 

match :: MPat -> MExpr -> EMT (Map.Map IM.ValueIdent MExpr)
match a b = execStateT (match' a b) Map.empty
    
  where

    updatePrincipal :: IM.ValueIdent -> MExpr -> RMM () 
    updatePrincipal k v = do 
      st  <- get
      st' <- case Map.lookup k st of
               Just v' -> do v'' <- lift $ majorTypeOf v' v
                             return $ Map.insert k v'' st
               Nothing -> return $ Map.insert k v st
      put st'         


    updateMetaVal :: (IM.ValueIdent,MExpr) -> RMM () 
    updateMetaVal (k,v) = do 
      st <- lift (lift get)
      let cs  = metaVals st
          cs' = case Map.lookup k cs of
                  Just vs -> Map.insert k (v:vs) cs
                  Nothing -> Map.insert k [v] cs
      lift (lift (put ( st { metaVals = cs' } )))


    match' :: MPat -> MExpr -> RMM ()
    match' a b = do 
      rawMatch a b
      binds <- get   
      mapM updateMetaVal (Map.toList binds)  
      return ()

    rawMatch :: MPat -> MExpr -> RMM ()
    
    rawMatch (MMetaVar p i) v = updatePrincipal i v
    
    rawMatch (MMetaAbstr a b) (MMetaAbstr x y) = do 
      match' x a   -- save and restore states should not be required since alpha conversion solve any variable capture problems ... 
      y' <- lift $ reduce y
      b' <- lift $ reduce b
      rawMatch b' y'
    
    rawMatch (MArray p [] _)     (MArray _ [] _)    = return ()
    rawMatch (MArray p (v:vs) _) (MArray q (w:ws) _) = do
      rawMatch v w
      rawMatch (MArray p vs UnTy) (MArray q ws UnTy)
    
    rawMatch (MVar _ i _) (MVar _ i' _) | i == i' = return ()
    rawMatch (MCon _ i _) (MCon _ i' _) | i == i' = return ()
    rawMatch (MNum _ i) (MNum _ i')     | i == i' = return () 
    rawMatch _ _ = error "unimplemented"
    
    

reduce :: MExpr -> EMT MExpr
--------------------------------------------------------------------------------
-- actual reduction cases
--------------------------------------------------------------------------------
reduce (MApp (MMetaVar p i) b tp) = do 
  b' <- sfinaeEval i b
  reduce b'  

reduce (MApp (MMetaAbstr pat codom) arg tp) = do 
  evalMetaAbstrApp (pat,codom) arg

-- TODO: Add Constant Merging Cases ... 

reduce (MApp a b tp) = do 
  a'  <- reduce a
  b'  <- reduce b
  -- no further reduction possible, all cases handled above 
  case typeOf a' of 
    MMetaAbstr pat codom -> do
      rty <- evalMetaAbstrApp (pat,codom) (typeOf b') 
      return $ MApp a' b' (OfTy rty)
    _ -> throwError (MErr (text "FIXME: (not a function type)"))   
    


--------------------------------------------------------------------------------
-- reduce - add type annotation cases 
--------------------------------------------------------------------------------
reduce (MVar p i _) = do
  t <- getContextTypeOf i
  return $ MVar p i (OfTy t)

reduce (MCon p i _) = do
  t <- getContextTypeOf i
  return $ MCon p i (OfTy t)    


-- TODO: add missing cases. 

--------------------------------------------------------------------------------
-- reduce - descend and scope update cases
--------------------------------------------------------------------------------
reduce e = descendM reduce e




sfinaeEval :: IM.ValueIdent -> MExpr -> EMT MExpr
sfinaeEval f arg = do
  
  vs <- getContextMetaValuesOf f
  reduceNext vs arg PP.empty

 where
  
  reduceNext :: [MExpr] -> MExpr -> Doc -> EMT MExpr
  reduceNext (v:vs) arg e = do
    case v of 
      (MMetaAbstr pat codom) 
         -> evalMetaAbstrApp (pat,codom) arg 
            `catchError` 
                  \ e' -> case e' of 
                            MErr e''-> reduceNext vs arg (e $$ e'')
                            e''     -> throwError e'' 
      _  -> reduceNext vs arg (e $$ pprint v <+> text " is not an meta abstraction" )  
  
  
evalMetaAbstrApp :: (MExpr,MExpr) -> MExpr -> EMT MExpr
evalMetaAbstrApp (pat,codom) arg = 
  do st <- lift $ get
     match pat arg 
     codom' <- reduce codom
     lift $ put st
     return codom'


getContextMetaValuesOf :: IM.ValueIdent -> EMT [MExpr]
getContextMetaValuesOf i = do
  st <- lift $ get 
  case Map.lookup i (metaVals st) of 
    Just vs -> return vs
    Nothing -> throwError $ MErr (pprint (pos i) <+> text "Undefined identifier "
                                                 <+> pprint i)



getContextTypeOf :: IM.ValueIdent -> EMT MType
getContextTypeOf i = do
  st <- lift $ get 
  case Map.lookup i (coreTypes st) of 
    Just t -> return t
    Nothing -> throwError $ MErr (pprint (pos i) <+> text "Undefined identifier "
                                                 <+> pprint i)                                                