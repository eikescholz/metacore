module ParserTests where
import Position
import Scanner
import Layout
import Control.Monad.Trans.State.Lazy
import Scanner 
import TestBase
import Data.List
import Lexer
import Parser
import Grammar
import MetaCore
import qualified IdentMap as IM

testParse :: String -> IO [PExpr]
testParse str = do (m:ms) <- lexModStr (newPos "MockMod",str) [["MockMod"]] []
                   --putStrLn $ "Mod: " ++ show (m:ms)
                   return (snd (parseMod m))

testParseFile :: String -> IO [PExpr]
testParseFile path = do 
  str <- readFile path
  let xchgSep ('/':xs) = ':':':':xchgSep xs
      xchgSep (x:xs) = x:xchgSep xs
      xchgSep []     = [] 
      modName = xchgSep $ reverse $ drop 3 $ reverse path -- assuming .mc suffix
  (m:ms) <- lexModStr (newPos modName,str) [[modName]] []
  --putStrLn $ "Mod: " ++ show (m:ms)
  return (snd (parseMod m))


parsesTo x y = do 
  a <- x
  b <- y 
  if (a == b)
    then return TestSuccess
    else return (TestFailure ("Condition `parsesTo` failed!"
                                      ++ "\n\n LHS: [ "++pp a 
                                      ++ "\n\n RHS: [ "++pp b  ))
 where
   pp ts = concat (intersperse     ",\n          " (map show ts)) ++ "]\n"

p0 = Pos (0,0,"MockMod")


mkMVar str = MVar p0 (IM.Ident str) UnTy
mkMCon str = MCon p0 (IM.Ident str) UnTy
mkAtMeth expr str = MAtMethod p0 expr str UnTy
mkDfltWord n = MNum p0 (defaultTargetWord n) 

parserTests = [

  Describe "Parser"
    [ It "parses foo"  
        (testParse "foo" `parsesTo` return 
          [ mkMVar ["foo"]
           ] ),
      It "parses (foo)"  
        (testParse "(foo)" `parsesTo` return 
          [ mkMVar ["foo"] ] ),   

      It "parses (foo,bar)"  
        (testParse "(foo,bar)" `parsesTo` return 
         [ MTuple p0 [mkMVar ["foo"],mkMVar ["bar"]]]), 
        
      It "parses (foo,bar,bla)"  
        (testParse "(foo,bar,bal)" `parsesTo` return 
         [ MTuple p0 
            [ mkMVar ["foo"], mkMVar ["bar"], mkMVar ["bal"]] ] ),

      It "parses xs.foo.bar.bla"  
        (testParse "xs.foo.bar.bla" `parsesTo` return 
        [ mkAtMeth 
            (mkAtMeth 
               (mkAtMeth (mkMVar ["xs"]) ".foo") 
               ".bar" )
            ".bla" ]),

      It "parses xs[i][j][k]"  
        (testParse "xs[i][j][k]" `parsesTo` return 
         [ MAt (MAt (MAt (mkMVar ["xs"]) 
                         (mkMVar ["i"])
                         UnTy ) 
                    (mkMVar ["j"])
                    UnTy ) 
               (mkMVar ["k"])
               UnTy ] ),

      It "parses *xs[i](j)[k]"  
        (testParse "*xs[i](j)[k]" `parsesTo` return 
        [ mkAtMeth ( MAt (MApp (MAt (mkMVar ["xs"]) 
                                    (mkMVar ["i"] )
                                    UnTy) 
                               (mkMVar ["j"])
                               UnTy) 
                         (mkMVar ["k"])
                         UnTy) 
                    ".prefix*" ] ),      

      It "parses *xs[i].foo"
        (testParse "*xs[i].foo" `parsesTo` return 
        [ mkAtMeth (mkAtMeth  (MAt (mkMVar ["xs"]) 
                                    (mkMVar ["i"])
                                    UnTy) 
                               ".foo" ) 
                    ".prefix*" ] ),  
      
      
                    
      It "parses *xs[i].foo[j]"
        (testParse "*xs[i].foo[j]" `parsesTo` return                     
        [ mkAtMeth 
            (MAt 
              (mkAtMeth 
                (MAt 
                  (mkMVar ["xs"]) 
                  (mkMVar ["i"])
                  UnTy) 
                ".foo") 
              (mkMVar ["j"])
              UnTy) 
            ".prefix*" ] ),
              
        

      It "parses *mod1::mod2::mod3::xs[i].foo"
        (testParse "*mod1::mod2::mod3::xs[i].foo" `parsesTo` return 
        [ mkAtMeth (mkAtMeth  (MAt (mkMVar ["mod1","mod2","mod3","xs"]) 
                                   (mkMVar ["i"])
                                   UnTy) 
                               ".foo") 
                    ".prefix*" ]),
     

      It "parses f x y"  
        (testParse "f x y" `parsesTo` return 
        [ MApp (MApp (mkMVar ["f"]) 
                     (mkMVar ["x"])
                     UnTy) 
               (mkMVar ["y"])
               UnTy ] ),
       
      It "parses a ++ b"  
          (testParse "a ++ b" `parsesTo` return 
          [ MApp (mkAtMeth (mkMVar ["a"]) 
                           "infix++") 
                 (mkMVar ["b"]) 
                 UnTy ]),
      
      
      It "parses 1 + 2"  
        (testParse "1 + 2" `parsesTo` return 
        [ MApp (mkAtMeth (mkDfltWord 1) 
                         "infix+")
               (mkDfltWord 2) 
               UnTy 
        ]  ),
       
      It "parses a + b * c"  
        (testParse "a + b * c" `parsesTo` return 
        [ MApp (mkAtMeth (mkMVar ["a"]) 
                         ".infix+") 
               (MApp (mkAtMeth  (mkMVar ["b"]) 
                                ".infix*") 
                     (mkMVar ["c"])
                     UnTy)
               UnTy ] ),
      
      It "parses a * b + c"  
        (testParse "a * b + c" `parsesTo` return
        [ MApp (mkAtMeth (MApp (mkAtMeth (mkMVar ["a"]) 
                                         "infix*")
                               (mkMVar ["b"])
                               UnTy) 
                          ".infix+") 
               (mkMVar ["c"]) 
               UnTy]
        ),
      
      It "parses x -> f x | y -> f y | z -> f z"  
      (testParse "x -> f x | y -> f y | z -> f z" `parsesTo` return 
      [ MAlt (MAbstractoid (mkMVar ["x"]) 
                     (MApp (mkMVar ["f"]) 
                           (mkMVar ["x"])
                           UnTy))
             (MAlt (MAbstractoid (mkMVar ["y"]) 
                                 (MApp (mkMVar ["f"]) 
                                       (mkMVar ["y"])
                                       UnTy)) 
                   (MAbstractoid (mkMVar ["z"]) 
                                 (MApp (mkMVar ["f"]) 
                                       (mkMVar ["z"]) 
                                       UnTy))) ]),
                                 
        
        It "parses x -> f x | y & x == 1 -> f y & x == y -> f 1"  
        (testParse "x -> f x | y & x == 1 -> f y & x == y -> f 1" 
          `parsesTo` return 
        [ MAlt (MAbstractoid (mkMVar ["x"]) 
                             (MApp (mkMVar ["f"]) 
                                   (mkMVar ["x"])
                                   UnTy)) 
               (MConj (mkMVar ["y"]) 
                      (MConj (MAbstractoid 
                                (MApp (mkAtMeth 
                                         (mkMVar ["x"]) 
                                         "i.nfix=="
                                      )   
                                      (mkDfltWord 1)
                                      UnTy
                                ) 
                                (MApp (mkMVar ["f"]) 
                                      (mkMVar ["y"])
                                      UnTy 
                                )
                              )           
                              (MAbstractoid 
                                (MApp (mkAtMeth 
                                        (mkMVar ["x"]) 
                                        "infix==" 
                                       ) 
                                      (mkMVar ["y"])
                                      UnTy
                                ) 
                                (MApp (mkMVar ["f"]) 
                                      (mkDfltWord 1)
                                      UnTy 
                                )
                              )
                        )
               ) ] ),

        It "parses foo ? (0 , y ? (Just x)) -> g x y | x -> h x"  
        (testParse "foo ? (0 , y ? (Just x)) -> g x y | x -> h x" 
           `parsesTo` return 
           [ MMatches p0 
                  (mkMVar ["foo"]) 
                  (MAlt (MAbstractoid 
                            (MTuple p0 
                               [(mkDfltWord 0)
                               ,MMatches p0 
                                  (mkMVar ["y"]) 
                                  (MApp (mkMCon ["Just"])
                                        (mkMVar ["x"])
                                        UnTy
                                  )
                                  UnTy
                               ]
                            ) 
                            (MApp (MApp (mkMVar ["g"]) 
                                        (mkMVar ["x"])
                                        UnTy
                                  ) 
                                  (mkMVar ["y"])
                                  UnTy
                            )
                        )
                        (MAbstractoid 
                           (mkMVar ["x"]) 
                           (MApp (mkMVar ["h"]) 
                                 (mkMVar ["x"])
                                 UnTy
                           )
                        )
                  )
                  UnTy
           ] ),
        
        It "parses file 'test/fac1.mc'"  
        (testParseFile "test/fac1.mc" `parsesTo` return 
        [ MLet p0 Nothing 
           [MDefVar 
              (mkMVar ["fac"]) 
              (MMetaAbstr 
                 (mkMCon ["Word"]) 
                 (mkMCon ["Word"])
              ) 
              (MAlt 
                 (MAbstractoid 
                    (mkDfltWord 0) 
                    (mkDfltWord 1)
                 ) 
                 (MAbstractoid 
                   (mkMVar ["x"]) 
                   (MApp 
                     (mkAtMeth (mkMVar ["x"]) 
                               ".infix*"
                     ) 
                     (MApp 
                       (mkMVar ["fac"]) 
                       (MApp 
                         (mkAtMeth 
                           (mkMVar ["x"]) 
                           ".infix-"
                         ) 
                         (mkDfltWord 1)
                         UnTy
                       )
                       UnTy
                     )
                    UnTy
                   )
                 )
              ) 
           ] 
           [] 
           Nothing ] ),
        
        It "parses file 'test/fac2.mc'"  
        (testParseFile "test/fac2.mc" `parsesTo` return 
        [ MLet (Pos (3,1,"test::fac2")) 
            Nothing 
            [ MDeclVar 
                (mkMVar ["fac"]) 
                (MMetaAbstr 
                   (mkMCon ["Word"]) 
                   (mkMCon ["Word"])
                )
            ] 
            [MBindVar 
                (mkMVar ["fac"]) 
                (MAlt 
                  (MAbstractoid 
                     (mkDfltWord 0 )
                     (mkDfltWord 1 )
                  ) 
                  (MAbstractoid 
                    (mkMVar ["x"]) 
                    (MApp 
                      (mkAtMeth 
                        (mkMVar ["x"]) 
                        ".infix*"
                      ) 
                      (MApp 
                        (mkMVar ["fac"]) 
                        (MApp 
                          (mkAtMeth 
                             (mkMVar ["x"]) 
                             ".infix-"
                          ) 
                          (mkDfltWord 1)
                          UnTy
                        )
                        UnTy  
                      )
                      UnTy
                    )
                  )
                )
            ] 
            Nothing
        ]),

        It "parses file 'test/fac3.mc'"  
        (testParseFile "test/fac3.mc" `parsesTo` return 
        [ MLet (Pos (4,1,"test::fac3")) 
           Nothing 
            [MDeclVar 
              (mkMVar ["fac"]) 
              (MMetaAbstr 
                (mkMCon ["Word"]) 
                (mkMCon ["Word"])
              )
            ] 
            [ MBindVar 
                (mkMVar ["fac"]) 
                (MAbstractoid 
                  (mkMVar ["t0"]) 
                  (MMatches p0 
                    (mkMVar ["t0"]) 
                    (MAlt 
                      (MAbstractoid 
                        (mkDfltWord 0) 
                        (mkDfltWord 1)
                      ) 
                      (MAbstractoid 
                        (mkMVar ["x"]) 
                        (MLet p0 
                          Nothing 
                          [ MDeclVar 
                             (mkMVar ["n0"]) 
                             (mkMCon ["Word"])
                          , MDeclVar 
                              (mkMVar ["t1"]) 
                              (MMetaAbstr 
                                (mkMCon ["Word"]) 
                                (mkMCon ["Word"])
                              )
                          , MDeclVar 
                              (mkMVar ["t2"]) 
                              (mkMCon ["Word"])
                          , MDeclVar 
                              (mkMVar ["t3"]) 
                              (mkMCon ["Word"])
                          , MDeclVar 
                              (mkMVar ["t4"]) 
                              (MMetaAbstr 
                                (mkMCon ["Word"]) 
                                (mkMCon ["Word"])
                              )
                          ] 
                          [ MBindVar 
                             (mkMVar ["n0"]) 
                             (mkDfltWord 1)
                          , MBindVar 
                             (mkMVar ["t1"]) 
                             (mkAtMeth 
                               (mkMVar ["n0"]) 
                               ".infix-"
                             )
                          , MBindVar
                              (mkMVar ["t2"]) 
                              (MApp 
                                (mkMVar ["t1"]) 
                                (mkMVar ["x"])
                                UnTy
                              )
                          , MBindVar 
                              (mkMVar ["t3"]) 
                              (MApp 
                                (mkMVar ["fac"]) 
                                (mkMVar ["t2"])
                                UnTy
                              )
                          ,  MBindVar 
                               (mkMVar ["t4"]) 
                               (mkAtMeth 
                                 (mkMVar ["x"]) 
                                 ".infix*"
                               )
                          ] 
                          (Just 
                            (MApp 
                              (mkMVar ["t4"]) 
                              (mkMVar ["t2"])
                              UnTy
                            )
                          )
                        )
                      )
                    )
                    UnTy
                  )
                )
            ]  
           Nothing]
        )     
     ]

  ]


