module Parser where
import Lexer
import Grammar
import MetaCore



type ParsedModule = (ModuleHead,[PExpr])

parseMod :: LexedMod -> ParsedModule
parseMod (h,ts) = (h, snd (parseModule ts) )   


