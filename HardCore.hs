module HardCore where


    type Type = MExpr -- Actually only a subset. 


data Val = Word DW.Word 
         | Var  Ident 
         | Meth Ident
         | Con  Ident
         | Lambda String Type Region Expr
         | Unit 
    deriving (Eq,Show)

data Decl = DeclVar  String Type 
          | DeclMut  String Type -- mutable 
          | DeclMeth String Type 
    deriving (Eq,Show)

data Bind = BindVar  String Expr 
          | BindMeth String Expr
  deriving (Eq,Show)

data Expr = Let Region [Decl] [Bind] Expr
          | App String String    -- lambda/function application
          | Eval String [String] -- procedure execution
          | AtMeth String String 
          | MkTuple [String]
          | Val Val
          | Case String [(Pat,           -- the patterns 
                            [(Maybe Expr -- the condition guards
                             ,Expr)]]    -- result expressions.   
   deriving(Eq,Show)

data Pat = PatCon String
         | PatApp Pat Pat
         | PatWord Int
         | PatVar String 
   deriving(Eq,Show)

data Stmt = Expr Expr
  deriving(Eq,Show)
