-- just a  generic interface for adt
-- that have additional infomation attachted to each constructor 

module HasInfo where

class HaseInfo a i | a -> i where
  infoOf :: a -> i