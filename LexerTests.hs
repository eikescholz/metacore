module LexerTests where
import Position
import Scanner
import Layout
import Control.Monad.Trans.State.Lazy
import Scanner 
import TestBase
import Data.List
import Lexer

testLex str = do lexModStr (newPos "MockMod",str) [["MockMod"]] []


lexesTo x y = do 
  a <- x
  b <- y 
  if (a == b)
    then return TestSuccess
    else return (TestFailure ("Condition `lexesTo` failed!"
                                      ++ "\n\n LHS: [ "++modspp a 
                                      ++ "\n\n RHS: [ "++modspp b  ))
 where
   modspp :: [LexedMod] -> String
   modspp ts = concat (intersperse ",\n        " (map modpp ts)) ++ "]\n"
   modpp (h,ts) = "(" ++ show h ++ ",\n\n         [" ++ pp ts ++")\n"
 
   pp ts = concat (intersperse     ",\n          " (map show ts)) ++ "]\n"

mkMockMod :: [Token] -> [LexedMod]
mkMockMod ts = [((["MockMod"],[],defaultAssocs),ts)]

   
lexerTests = [
  Describe "Lexer"
    [ It "lexes (foo)"  
        (testLex "(foo)" `lexesTo` return (mkMockMod 
          [ SymTok (Pos (1,1,"MockMod"),"("),
            VarTok (Pos (1,2,"MockMod"),"foo"),
            SymTok (Pos (1,5,"MockMod"),")")] )),

      It "lexes (a + b * c)"  
        (testLex "(a + b * c)" `lexesTo` return (mkMockMod 
         [SymTok (Pos (1,1,"MockMod"),"("),
          VarTok (Pos (1,2,"MockMod"),"a"),
          InfixTok (Pos (1,4,"MockMod"),"+") (AssocLeft 6),
          VarTok (Pos (1,6,"MockMod"),"b"),
          InfixTok (Pos (1,8,"MockMod"),"*") (AssocLeft 7),
          VarTok (Pos (1,10,"MockMod"),"c"),
          SymTok (Pos (1,11,"MockMod"),")")] ))
          
     ]
   ]


