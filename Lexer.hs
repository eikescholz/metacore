module Lexer where
import Position
import Scanner
import Layout
import qualified Data.Map as Map
import Grammar 
import Data.List
import Control.Exception
import MetaCore

-- The lexer loads the module header, that is imports and precedence defs
-- and applies associativities. which requires 


type ModName = [String] 

type ModuleHead = (ModName,[ModName],Map.Map String Assoc)

type LexedMod = (ModuleHead,[Token]) 


defaultAssocs :: Map.Map String Assoc
defaultAssocs 
  = Map.fromList [
      ("*",AssocLeft 7),
      ("/",AssocLeft 7),
      ("+",AssocLeft 6),
      ("-",AssocLeft 6),              
      ("==",AssocLeft 4) ];


importedModNames :: ModuleHead -> [ModName]
importedModNames (_,ns,_) = ns

findLexedMod :: ModName -> [LexedMod] -> Maybe LexedMod
findLexedMod n ms = find (\((n',ns,as),ts)-> n == n') ms


-- TODO: Add system libarary search paths using environ (reason for IO monad) 
modSearchPaths :: ModName -> IO [String]
modSearchPaths ns = return [concat (intersperse "/" ns ) ++ ".mc"]



splitModuleHeader :: ModuleHeader -> ([ModName],Map.Map String Assoc)
splitModuleHeader h = let (is,h') = exIm h
                          as      = exAs h'
                        in (is,as) 
  where

    exIm :: ModuleHeader -> ([ModName],AssocSpecs)
    exIm (Import n h) = let (ns,as) = exIm h in ((map snd n):ns,as)
    exIm (Precs as)   = ([],as)

    exAs :: AssocSpecs -> Map.Map String Assoc
    exAs (Prec a (n:ns) as) = let msa = exAs as in Map.insert n a msa
    exAs (Prec a [] _)      = Map.empty
    exAs NoSpec             = Map.empty



mergeAssocs :: [LexedMod] -> Map.Map String Assoc
mergeAssocs []                = defaultAssocs
mergeAssocs (((_,_,as),_):ms) = let as' = mergeAssocs ms in Map.union as as'
   

applyAssocs :: [Token] -> Map.Map String Assoc -> [Token]
applyAssocs [] _ = []
applyAssocs (InfixTok (p,s) a:ts) as 
                 = case Map.lookup s as of
                     Just a' -> InfixTok (p,s) a':applyAssocs ts as
                     Nothing -> InfixTok (p,s) a :applyAssocs ts as 
applyAssocs (t:ts) as = t: applyAssocs ts as

-- lexMod 
-- returns a list of LexedModuls in order for compilation
-- will result in n^2 complexity for number of modules
-- which really should not be an issue for first releases
-- ans can be using an additional Data.Map 

readMod :: ModName -> IO (Maybe SubStr)
readMod n = do ps <- modSearchPaths n
               tryReadMod ps
 where
   tryReadMod :: [String] -> IO (Maybe SubStr)
   tryReadMod []     = return Nothing 
   tryReadMod (p:ps) = do estr <- try (readFile p)  
                          case estr :: Either IOException String of
                            Left  e -> tryReadMod ps
                            Right s -> return (Just (newPos p,s))


lex :: ModName -> IO [LexedMod]
lex n = lexMods [n] [] []  
  

lexMods :: [ModName]      -- Names of modules to Lex
           -> [ModName]  -- cur. module import path for cycle detection
           -> [LexedMod]
           -> IO [LexedMod] 
lexMods []     ns' ms = return []
lexMods (n:ns) ns' ms = do ms0 <- case findLexedMod n ms of
                                    Just m  -> return [m]  
                                    Nothing -> lexMod n ns' ms 
                           ms' <- lexMods ns (n:ns') ms
                           return (ms0++ms')
                                  
lexMod :: ModName -- module to lex
          -> [ModName] -- cur. mod. import path for cycle detection
          -> [LexedMod] -- allready read modules
          -> IO [LexedMod]

lexMod n ns ms 
  = if n `elem` ns 
      then error ("Cyclic or repeaded import of module " ++ show n )
      else do rm <- readMod n
              case rm of
                Nothing -> error ("No implementation of "
                                   ++ show n ++ "found" )
                Just str -> lexModStr str (n:ns) ms

lexModStr :: SubStr -- modlue as sting
          -> [ModName] -- cur. mod. import path for cycle detection
          -> [LexedMod] -- allready read modules
          -> IO [LexedMod]


lexModStr (p,str) (n:ns) ms = do 
  let ls0 = scan p str
      ls1 = layout ls0
      hdr = parseModuleHeader ls1
      (ns',as') = splitModuleHeader hdr
  ms' <- lexMods ns' ns ms 
  let as = Map.union as' (mergeAssocs ms')
      m  = ((n,ns',as), applyAssocs ls1 as )
  return ( m : (ms'++ms) )




