
WARNING: PRE ALPHA CODE - CURRENTLY A PUBLIC BACKUP
===================================================

You can take a look, but, for the moment do not expect that anything
is consistent or working. Im beginning to try to clean this document up
so that is more then a reminder to me where I stopped. I don't have time
to work constantly on the project, and this document is more a note to myself. 

This code is the last of a sequence of several 
prototypes developed over soon two decades. While the features and concepts 
are stabilizing the composition of everything is not finished. 

The documentation is also used to conceptualize ideas before implementation
and is currently usually out of sync with the implementation. It should
provide an outline to the ideas and concepts however. 

This compiler code is licensed under GPL3. Later upcoming default and standard
libraries will probably be BSD/MIT style licensed, to allow painless 
commercial adoption. 


The Metacore Programming Language
=================================

  - TODO: Rename data stack to shunt stack everywhere

Overview
--------

### Meta Objectives: ###

  - Get a working prototype finished
  - Prototype must be small in code size (hard to miss with Haskell)

### Design Objectives ###

  - Correctness (i.e. no undefined behavior)
  - Speed. 
    Its for 
      - real-time,  high level systems programming, 
      - high performance computing / real-time numerics
      - demo coding.
  - High Readability 
  - Micro controller friendly, i.e. System programming. Smallest system 
    is size is probably arduino style (atmega328p).  
  - Real-Time System Capabilities ( to avoid feature bloat by design ) 
  - C/Tex philosophy of "once it's done it will not change"
  - Parallelism using communicating processes 
  - Guaranteed tail call optimization under well defined conditions.
  - No Runtime system relative to its abstract target machine. 
     The abstract target machine will try to get some legacy design-issues with
     signals, interrupts and concurrency solved. That will require an runtime 
     system on most common operating systems providing a compatibility-layer.  
  - No garbage collector. 
  - Scripting Language Layout, but top level is special (defines modules)
  - Should be easily be use-able as extension/scripting/console language 
    for applications.  
    (This implies no unsafe features especially no ffi. Foreign function access 
     is done by providing compiled modules via an interface of the meta-core 
     implementation, that is not a language extension. An ffi is unsafe and a 
     save use of a language should not require to "restrict" its features. 
     Its up to the implementer and usage context of metacore how an foreign 
     functions can be accessed. In any case the result will be imported as a 
     kind of precompiled module. )
  - Simple module system (Go/Haskell style)
  - Integrated Test System (Once primary language design is stable)


### Design Decisions: ###

  - No C style pointer arithmetic. 
  - Per default const value based, as if in C everything were const by default.
  - Use Haskell like partial applications. Simplifies a lot for the base languge.
  - A 4 stack layout, motivated by forth, but 2 stacks are not enough.  
  - No general overloading. A name refers to exact one entity. 
  - Partial application and closure support
  - Call by name semantics
  - Capture by reference
  - Return as value construction. (To Avoid unnecessary copies and initializations) 
  - Process based parallelism (If not I'll run into Go's problem of the interaction
    of dropping privileges and concurrent routines. ) 
  - No complete type inference.
  - No first-class methods. 
  - Require weak referential transparency.
  - Abstraction values are crated via (a@r : t ->  e)
  - Type Abstractions via ( a@r => t)
  - Meta Abstractions via ( a => t)
  - Type Abstractions are a special case of meta abstractions. Unlike in Idris
    its not possible to execute type abstractions at runtime. They have
    value representations, though. 

Design Decision Rationales:
---------------------------

### Partial Application Support: ###
    
    Partial application yields a canonical way to construct closures. They
    are translated via continuation passing style into LLVM instructions,
    that allow LLVM to perform constant propagation with them. The target
    IR that translates to LLVM is called reforth, since its its motivated
    by forth, but optimized as IR for a functional language. Also it has
    4 stacks, to be able to get a GC free hard-realtime semantics. 

### No Complete Type Inference: ###

    Type inference is used to be able to omit type specification where they 
    clutter the code with, to a sentient being obvious, type annotations. So 
    being able to infers types for any given expression is a nice feature but
    not required for a good language. Type elision rules for most common cases 
    are sufficient. Also as Haskell experience will tell you, tiny errors in 
    big expresions will create type error messages, where it is not clear 
    from what mistake in the code an error originates. These kind of errors 
    broke my flow  of beeing locked on the problem severl times. I tried to 
    solve a problem and the error made me think about the internals of the 
    type-inference system insted. In the end adding Type signatures then helps 
    to find the errer in Hasekll, they could as well be required. Experience 
    thus indicates that a too-mighty type-inference system is likely to reduce 
    productivity. The Go language examplifies that from the "other" side.  

 
### On Numerals: ###

    Metacore use suffixes to annotate types of numerals. This way we can avoid 
    using complicated semantics like Haskell type classes or c style coercion 
    rules.
    
    In paricular 1o, 2o 3o ... are byte (octet) values 1w, 2w, ... are word values,
    where a word is hosts addressing space width, at least 16Bit. 1,2,3,.. 
    is short for word values, "abc" is a string, 1.0f is a floating point Number, 
    biggest naturally supported by the machine.

    Arbitrary precision integers will use a suffix like python i.e. 1L 2L
    (Suffixes might change, depending on weather I'll introduce a signed/
     unsigned distinction - which I found irritatig. Modulo classes should not
    look like they have a natural ordering, but they have natural negative
    representants. Imho many integer overflow errors stem from this unnatural
    (from an abstract Mathematics perspective)
    introduction of signed types. The language makes the programmer think that 
    ints are behave like integers, which is not allway the case, especially
    it is not true that for all a>0 and b>0 it holds that a+b>0. 

    For example a byte has natural negative numbers -1,-2,.., -255
    where "-1 == 255 mod 256", "-2 == 254 mod 256" etc. Where natural means
    that they behave equilaent under addition, for example 
    "x - 1 == x+255 mod 256". The "uninuitive" part is that "-255 == 1 mod 256", 
    which is formally correct for modulus classes, but not correct for
    singend bytes, which do not have values "smaller" then -127.

    If possible metacore will try not to impose mathematical structures 
    on data, that does not have this structure. Floating points may be 
    an exeption, the only natrual structure they have are that they are
    a monoid. 


### Scaling And Parallelism: ###

    If the abstract target architecture should scale with the total number of
    cores. Physics limits useful architecture choices since there is an upper 
    limit (speed of light for a perfect machine) for the communication speed 
    between components. Basically having multiple cores sharing the same 
    memory does not scale well. Increasing cores and memory and will introduce 
    ever increasing latencies ... So a shared memory architecture is not a good 
    choice for the abstract target architecture since adding cores and/or 
    memory will make these components (all of them) slower. 

    Multiple cores with shared memory for numerics can be used by providing 
    parallel vector primitives as a standard module, maybe some simple 
    map-reduce framework. 
    
 
### On Events And Interrupts: ###

    Having signals/interrupts introduces the can of worms from hell called 
    async-signal-safety. The problems are quite similar to the race condition 
    problems for multithreaded programs. However, for communicating parallel 
    processes these problems are reduced to the used massage passing interface
    and need to be solved there depending on the implementation. 
  
    The abstract target architecture will therefore not have classical 
    interrupt handling only blocking IO waiting for some event. An additional 
    process on a dedicated (maybe abstract) core many be used to emulate 
    classical interrupt responsiveness. Using multiple such dedicated cores  
    might be interesting for hardware that needs very low reaction latency and 
    short interrupt dead times.  

    Taking this together with the scaling for parallel problems yields that the
    abstract target machine does not have classical interrupts just blocking 
    io ports and message passing between cores/nodes.

    So complexity shifts from interrupt handling stuff to the message passing
    stuff. If stdin, stdout and stderr io streams are defined we
    proably add an stdev, i.e. standars events, like SIGTERM, SIGSTOP, ...
    etc.

    The IO api might provide interuptable routines, i.e. for things like 
    a user pressing ctrl+c - but this will result in an error return.
    There will nothing that re-introduces the problem of async-signal-safety.

### High Level Systems Coding ###

    Metacore will not provide any unsafe semantics, but will allow to link
    to modules written in C or assembly. 
    The idea is to split systems programming as done on the C level, into
    high level code and stuff that can in principle be implemented in 
    assembly and inported into metacore as a module. Having seen an 
    compact forth implementation in assembly that seems to be a viable 
    and usefull distintion. Support of external extension modules can
    be deactivated allowing metacore to be used as safe extension language.

### Integrated Test System ###
   
    While writing tests may seem to be a waste of time for the impatient and
    inexperienced. The experience is that doint it properly will save time
    in total for non-trivial projects. Combined with high level systems
    programming it may be possible to compile any program into the program
    itself and a separate test-suite. For cross-compilation the later can
    may be usefull to test the hardware (and low level system apis) on the
    target hardware. All with an integrated tool-chain. 

    (TODO: Will be done after the language design is stable and tested enough)

### Runtime argument type information ###

    Metacore has runtime type informations build in. This seems like overhead,
    however for a proper ABI it is not really. Assume there is 
    a function taking an argument x of type t creating a result f(x), in
    metacore written as
    ```
      (x : t) -> f x 
    ```
    Then there are two cases t is a non polymorphic type,
    or not. 
  
    In the first case the type correspondst to constant runtime type information
    and any code overhead will be eliminated by a kind of constant propagation.
    
    Only a register for passing the type is reserved by the ABI but not used, 
    which is usually does not cost any speed. Even the arduino has (atmega328p) 
    32 registers, which is plenty, if you are old enough to have experience with
    the original x86 instruction set or older low-end processors.  
    
    The other case it is a polymorphic type, then when running the application
    the data in the argument still has a specific non polymorphic type, i.e.
    is a member or a set of values. In that case at least the length of 
    the type values needs to be passed for copying, so passing a pointer to 
    runtime type information is as expansive and allows to implement a lot
    of further features, at no run-time speed cost.

    However having the type argument allows, Go style interface types,
    to be integrated into the ABI with no extra costs. The common method
    dispatch syntax is used to access such interfaces, so that methods are
    not a special case of partial applicable functions. i.e. you can
    not write 
    ```
      (map .length someListOfLists) 
    ```
    but have to construct a closure first 
    ```
      (map ((xs : List t) -> xs.length ) someListOfLists) 
    ```
    That may not correspond to briefest forms of syntax contemporary functional
    programming languages could offer, but is tolerable. 
    

### Weak Referential Transparency ###   

    Weak referential transparency means that once a name is assigned the value 
    never changes. This requires that mutable variables refer to locations not
    the contained vales. Reading mutable values is an expression yielding a value.
    Under weak referential transparency the read may have side-effects thus
    return a different value each time it is read. 
    Weak referential transpacrency captures the property of Haskell's pureness 
    that seems to be essential for many applications but allows to avoid much 
    of the formal clutter introduced by Haskells rigor pureness.  

### Module names are not file names ###

    Metacore should be useabe as safe extension language, in that 
    regard modules may map to some build in interfaces and not
    file names. The compiler can thus be configured on how 
    module names map to files. The separator used in module namespaces
    is '::' like in C++. 

### User defined operators but no user defined precedence ###

    Haskell style precedence overloading is not supported, it makes the
    lexer and parser and module compilation logic unnecessarily complex.
    Metacore will used a C++ style prefixed set of precedence rules, 
    that are so generic that they apply to user defined overloadable
    operators as well. 
    
    Support of user defined operators is a trade of design decision between
    the following conflicting fields
    
      - Simple Semantics
      - Flexibility
      - Usability

    Flexible precedence definitions as in Haskell are actually very useful to
    write good combinator libraries. However precedence is a syntax property,
    and lager projects might want to use a lot of different third party
    libraries that might have conflicting precedence definitions, for the
    same operators. This makes the semantics of the written code depended on 
    changes of the imported libraries. It thus has an usability scaling problem.  
    
    The introduction of separate precedence modules would improve that situation
    but not solve it. Also makes the semantics complexer by adding a complete
    sublanguage for precedence definitions. 
    
    A better approach would be to start with the standard precedence ordering
    for <,>,+,-,*,/, etc and add a lexicographic order. For derived ops like
    <+>,<->,<*>,</> etc. This however adds an idefinite number of precedence
    levels to the language, that are not easily expressible as LALR(1) grammar.
    
    On the other hand C++ style operator overloading solves the problem by only 
    allowing overloading for a predefined syntax. The C++ approach seems to be 
    not flexible enough, but is still sufficient for many applications. In 
    practice only a little more flexibility then for the c++ syntax seems to be 
    required. 
    
    The generic rule will be to use the max precedence of the single operator 
    symbols, with maybe some additional pattern rules, for example for
    +=, -= etc, that are jet to be decided. 
    
    The associativity must be derived as well. That will be done with the 
    following algorithms. All supported single associativity symbols get an 
    associativity value of -1 for left 0, non-assoc, 1 for right, then these 
    are summed, if the result negative the operator is left associative, if its 
    zero it is non associative, if it is 1 it is right associative. 
    
    This should create a viable compromise between usability and flexibility. 

Definitions:
------------

- TODO: That section still is ontologically improper.
        Good names are hard and nomenclature seems not to be very coherent
        along different lines of computer science and mathematical logic. 

### Value: ###

A value is a specific byte-sequence of finite length. 

### Type ###
 
A type is a syntax expression that given a context binding all free type
variables defines a set of values. That is unlike mathematical set/type theory
where types are some kind of general value sets. However it follows the 
distinction in the sub-subject "model theory" of mathematical logic, 
properly distinguishing the elements of a formal language and what it refers to.
Types are entities of the formal language not the sets of values they refer to.     

Types are thus not necessarily equal if their value sets are equal. Which is
quite natural for many computer language application cases.

### Tangible Type ###

Type with no free type variables, i.e. a type defining a value set. 

### Abstract Type ###

A type with unbound free type variables. 

### Identifier ###

A string identifying a meta value, a core value or a region.
    

### Sort ###
   
Sorts are the type of entities an identifier refers to. 
That is a meta value, a core value, a region or a module name space.   


### Tag: ###
   
An identifier beginning with an upper case letter. Will be matched against
in lambda expressions. Called constructors in Haskell, but are actually
the tags of tagged unions which are called algebraic data types in
Haskell.  


### Variable: ###

A variable is a identifier starting with a lower case latter, or operator 
abbreviation of it. E.g. + for infix+op. Variables are bound by lambda, local 
and let expressions see below. 


### Instance ###

The implementation of a interface for a given abstract type.


### Method ###

A identifier beginning with a '.' character mapping providing the 
abstractions interfaces are build from.  



Basic Language Architecture:
----------------------------

MetaCore is based on a marriage of C++ template mechanisms implemented as an
untyped lambda calculus with an close to the metal functional programming
language. 

 That is, it is: 
 - A meta language that evaluates itself to the core langauge
 - A core close to the metal functional language 

The meta language is explicitly is an "untyped" lambda calculus whose 
"constants" are core expressions and essentially is a mighty macro language 
the evaluated during the type checking process. A subset of this 
macro language is translated to runtime type information. 

The core language is a close to the metal lambda calculus, using 4 stacks 
and different let variations to manage stack frames for all stacks manually.
This allows to omit a garbage collector and to define proper hard real-time
semantics. 

The 4 stack layout emerged from experiments using a forth
like immediate representation. Forth is old and while two stacks may seem 
wasteful to the C programmer, that is not and has not been the case in 
practice for decades. 

The "metal" in "close-to-the-metal" is an abstract stack machine called Reforth
that translates into LLVM code using continuation passing style. To estimate
the emitted assembly for Reforth code is about as hard as estimating it
for C or simpler C++ code.

Prior Language Influences:
  - Haskell,Clean,ML,Lisp : For functional style. 
  - C,C++,Fortran : As thumb-measure for close to the metal semantics.
  - C++ : Meta semantics as type-system template programming language
  - Go : Interfaces/Classes and panic/defer 
  - Forth : For using a multi-stack machine to get rid for years seemingly
            indispensable garbage collection conflicting with my real-time  
            design objectives. 
            Also for abstract target machine complexity after looking at small
            but complete forth implementation in plain x86 assembly. 
  - Python : Large L suffix for long integers (if included) 
  - Idris : For dropping my internal resistance against using runtime type
            information.
        
### Modules ###

Each source file corresponds to a module. A module has some optional 
header line commands to import sub modules.

An example module:

```
#!/bin/mci 
# Header comments can be used to define a shebang to the metacore interpreter
import 
  sys::map
  foo::bar # comment

# The header ends with the first line that does not contain an import 

let y : Word = f 3 

```
Note that the interpreter "/bin/mci" will not be part of first prototypes. 

For a normal import as in the above example, the top-level symbols of the
module will be imported unqualified and qualified. The unqualified import
can be suppressed and the module path abbreviated with for example

```
import
  a::b::c 
  f::c
  as Foo
```


TODO: Include version information and requirements. A version 3.4.6 should be 
      read as API_VERSION.API_EXTENSION_VERSION.PATCH_LEVEL

      That is simple but will force you to upgrade the api version if design
      bugs require non-downwards compatible api changes. 
      
      The api-extension-version are down-wards compatible extensions to an api.

      Patchlevel is clear. 

      Will annoy marketing in some cases, but for long term maintance of 
      infrstructure codebases this is too important. 

      maybe somthing like
      ```
      import
        a::b::c 3.4.6+
        f::c
        as Foo
      ```
      This will import the latest API_VERSION 3 instance of module a::b::c
      Technically this will reject to import an a::b::c if only a 4.x.y 
      is installed, since the API might be incompatible. 


### Type System ###

A type of an expression in metacore is always a direct construction of
the type of its sub-expressions. Aside of some type elision rules as
syntax sugar types are synthesized attributes. 

The following elision rules are defined. 

  1. Function definition type argument elision. 
     ```
     f : X -> Y = x -> f x 
     ```
     is short for
     ```
     f : X -> Y = (x : X) -> f x 
     ```
  2. TODO

There is a peculiarity regarding the type of polymorphic 
functions like 
```
  f : (Int,$a) => $a
```
the domain will introduces the type variable.  MetaCore does not have type 
inference but type computations. The function type is a function from
meta expressions to meta expressions. 

If the codomain of a function contains a Type-variable, the function can
not return, since the return value is not well defined. 

This is useful for a Haskell like error or a go like panic function,
that would have the type  

error : String -> $a


### Base Types ###

The basic machine values are 

  - Byte    : 8Bits for modulus 256 class. 
  - Word    : machine word modulus class.
  - Float   : the longest machine floating number supported.

References may have existential qualified word values referred by a type t:
  
  - [n1,...,nN] t

[]t is the simplest form of reference. 
Note that [a][b] T is [a]([b] T)] and thus different
from [a,b] T . The type [a][b] T has one level of indirection more then
[a,b] T.

Arrays: 
  
  - t[n]

where n is a nonzero Word i.e. Int[256] of the name of a Nz{Word}.

Function types are abstractions of the meta language, that is
  
  - arg => result

The core language will support a very limited subset of these abstractions
as valid type values. (The details are to be worked out.)
There is a builtin new-type definitions 

```
    type String  = [n] Byte[n]   
```

Strings are not null terminated so that strings and sub-strings are the
the same. Strings are "big" in the sense that they are two words, the size
in bytes and a pointer to the data. But that simply seems to be the most natural
definition, since Sub-Strings are have the same structure.   

Let n be a non zeroWord then

```
    %n     
```

is the integer modulus n class. These classes primary used is for array 
access, thus restriction to word size n. Every modulus class 
thus requires a word to store. 

On simple hardware there will be no efficient hardware division and modulo 
operations. However bit-wise-and is usually present and allows to do
calculate 2^n modulos very fast.    

For speed there is a variant of the modulus type with special syntax
```
    %2^n   
```        
is the integer modulus (2^n) class. This reserves the symbols '^' for
that syntax. It may seem excessive to add a special syntax for this case
however these size classes arise naturally from generic "divide and conquer"
algorithms, where the data is divided into equal sizes. Typical fast Fourier
transform algorithms are an example. In that regard this is a useful
type. 



Note: 
    C Style ints are wired, they are a modulo class but do not provide all of 
    the negative numbers, to "provide" an "ordering" that breaks the triangle 
    inequality. As a bonus this causes a lot of security sensitive
    integer and buffer overflows. Signed ints will proably not be part of
    metacore for that reason. For that reason will there is  no natural 
    '<' and '>' on words and int only == and !=. 


### Basic Constructors ###

Numeral types are fixed by the optional suffix, to make the type identifiable
on the lexical level. 

Examples:

```
    134L   : Integer

    12.34L : Floating  

    1234o  : Byte     # a Byte value o for octet 
    'a'    : Byte     # Ascii encoding
    '\xFF' : Byte     # For conveniencce 

    123   : Word

    123.4 : Float    # The machines floating point word

    "This Is A string" # utf-8 encoded string.  
```

Since b is a hex numeral it can not be used as suffix for bytes,
instead 'o' for octet is used. Strings are supposed to be utf-8 encoded.
However, the core semantics is agnostic about that. 

Metacore identifiers are restricted to ASCII characters, in favor of embedded
systems where character tables uses too many resources.

(Evaluate: Use llvm type suffices in stead 17i8 for a byte value of 17 etc?) 

Prefixes: 
```
    0xCAFFEBABE : Hex Word
    0o666       : Octal Word     
```

### Basic Abstractions ###
 
Core abstractions the general form

```
  (x@r : t) -> f x 
```

where x is an argument pattern r is a region pattern and t is a type
pattern, that binds to type values of the arguments. After a lot of 
experiments it has been found that passing type information values 
(by reference) is 
negligible overhead since most type values are constants. For small 
abstractions llvm optimization passes will remove them and for complexer 
abstractions, the runtime cost is negligible. 

### Abstractions with Patterns and Guards ###

An abstraction can be defined using pattern guards. But this is syntactic
sugar to elide the conditional operator. In particular

```
f = pattern1 & guard1 -> result1.1
             & guard2 -> result1.2
             ...
  | pattern2 & guard3 -> result2.1
             & guard4 -> result2.2
             ...
  | ... 
  ...
```

is short for

```
f = ( expr -> do expr ? pattern1 & guard1 -> result1.1
                                 & guard2 -> result1.2
                                   ... 
                      | pattern2 & guard1 -> result1.1
                                 & guard2 -> result1.2
                                  ... 
                      | ...             
                      ...   
                     ... )
```
for more details see the description on the conditional operator.  


### Basic Application ###

Metacore uses a simple partial application syntax, i.e.:

```
  f x
```

this will evaluate the abstraction f with argument x applied.

All applications are call-by-name, i.e. provided by a pointer without 
copying the value of the argument. 


### Method Calls ###

Methods calls have different dispatch semantics then functions. They are
selected by a dispatch table associated with a type of the value they
are applied to. The common method call infix '.' syntax is used, e.g. 

```
  xs.foo 
```

Unlike function abstractions, methods are not first class values. They only are 
meaningful when applied to a value. 

Note: 
The initial designs aimed at implementing method calls as generic function
calls, to have first class methods. However, that turned out to be a 
sophisticated simplification and has been dropped. Making them their own
class of enities is in total simpler and clearer.  


### Conditionals ###

"One conditional operator to bind and discriminate them all"

It has the following syntax:

```
expr ? pattern1 & guard1 -> result1.1
                & guard2 -> result1.2
               ... 
     | pattern2 & guard1 -> result2.1
                & guard2 -> result2.2
                ... 
     ...
```

The guards are expressions that check boolean conditions, like Haskell's
pattern guards.

The analog to erlangs multi-if can be done in many ways, for example:

```
 () ? () & guard1 -> result1
         & guard2 -> result2
         & guard3 -> result3
         ...
```

And "if ... then ... else" syntax is

```
 e ? True -> thenCase 
   | False -> elseCase 
```

The ? can be nested to be used like a Haskell style "as" pattern, e.g.:

```
 foo ? (0 , y ? (Just x)) -> ...
```

The above matches y 'as' (Just x). Haskell uses '@' that is a region annotation
in Metacore.



### Interface Type ###

Interface Types are a mixture of C++ abstract classes, Haskell type classes, go interfaces. Interface types provide an interface to an
underlying structural type.  Their instances provide type aliases with an attached constant method dispatch table. 

```
interface Foo $a where
   foo : X 
   bar : Foo $a => Y
```
that will declare the type (Foo a) with, for a given value x, the interface:
 - x.foo : Foo $a => X
 - x.bar : Foo $a => Foo $a => Y

Interfaces can extend other interfaces. 

```
interface Foo $a for {:Bar,Bur:} where
  ... 
```
defines an interface Foo that inherits all methods from interface
Bar. 

Instantiation of the interface for the above example is done like: 

```
instance Foo BarType where
   x.foo = ...  # instead of a "this" keyword x is bound directly  
   x.bar = ...  #  but not as ( x -> ... ) that is abstraction syntax
```

To provide o(1) method dispatch, the methods must be constants and thus
depend on constants, so that instances create a fixed lookup table at 
compile time. 

Operators can be instantiated as direct expressions

```
instance Concatable BarType where
   x ++ y = ...    
```

which is actually syntax sugar for the somewhat awkward notation

```
instance Concatable BarType where
   x.infix++ =  y ->  ...    
```

There will be built in classes like:

```
interface HasZero a where # stuff that has a zero element
  zero : a
```

Instance declarations may have type variables as in Haskell to create
a set of actual type class instances with one declaration. A instance of
a type class is always is always a (sub-)type of the parameter. The runtime
value of an instance type has an constant attached method dispatch table. 

### Multi Parameter Interfaces ###

Interfaces are provided by the type information of a subtype, this
implies the use of one parameter, that defines the supertype as
data structure without interface definitions. However it can be
extended multiparameter type classes:

```
   interface (Foo $p1 ... $an) $t where
     ... 
```
defines parmetrized interface type that defiens a subset
on $t.

#### Nameless Interfaces ####

A type {: I0,...,In :} where I0,...,In are interface names 
is an nameless interfaces that combines all methods 
from I0, ..., In into one entity. I0,...In must not
contain methods of the same name, and thus not contain
duplicates.

Two nameless interfaces are equal, if they define the same set of sub-interfaces names. (I.e. equality after lexicographical ordering)

For the ABI is given by a jump-table derived
from a lexicographical ordering of the contained interfaces. 
Note the in the ABI the interface type is derived from
the mattern domain of the function, so that the ABI tables
are consistent. 

Further Nameless interfaces will be used by the type decuction
to find a principal type.



#### Rationale: ####

The Go-style implicit instantiation was considered but dropped. 
It seems to be a sophisticated simplification. The compiler code will include instance representations anyway and exposing them to the programmer is then clearer.

Further, the programmer, when defining a method, might accidentally 
update an interface he does not want to update, since all classes sharing the same type of projection will get implicitly updated. That does not seem to be such a good idea and could lead to subtle incompatibilities in large code bases. 


#### Subtypes and Predicates ####

Predicates are interfaces that define a restriction on the set of values 

```
interface Foo $a for Bar where
   Foo = x -> someBoolOf x  # someBoolOf can use interfaces I0 ... IN 
   foo : X # optional method 
```

The interface name Foo is also the predicate name and (Foo $a) is the subtype of $a so that for all x : (Foo $a) it holds that ((someBoolOf x) == True).

An builtin example is:

```
interface NonZero $a for HasZero where  
   NonZero = x -> x != x.zero 
```

It possible to patter-match against a Subtype, as in
```
foo = (x : NonZero $a) -> something
    | (x : $a )        -> somethingElse
```
The array constructor {...} can in a type can be used to construct
a nameless interface type ({I0,...,In} $a) that is a set of
interfaces which directly translates to the ABI method dispatch lookup table via lexicographic ordering and duplicate removal.  

To keep things simple it is assume that interfaces do not commute, i.e.
```
S (I a) != I (S a)
```

### Specializations ###


#### Core Abstraction Specializations ####  

There is a single type declaration for every name that is used by the
type checker. In that sense the core language does not support ad-hoc
overloading like specializations.  

However, since the call ABI of an application always provides a 
pointer to a type representation of the argument, it is possible to pattern-match
the type as well as the value in abstractions, provided that the
result is compatible with the declared type. For example

```
let foo : $a => Bool
      = x : Int -> ...
      | y : Bla -> ...
    ...
  in ... 
```
is valid code fragment, if Bla is a type.

Matching the type at run-time induces a bit of run-time cost. However, if short 
functions are inlined and/or constant propagated properly that speed impact is 
negligible. 
And it is possible to add an attribute inline, that in conjumtion with
the mandatory constant propagation will optimized run-time type check costs
away, if the argument type is a constant.    

#### Meta Abstraction Specializations ####

The meta processor is basically untyped and specialization is not really 
meaningful 

Meta abstractions do not support a ternary operator they are given by

```
meta f = pattern => result
```

where if pattern does not match, the previous binding will be tried.
That is a simple SFINE semantics. Its only an error if all available
substitutions fail. 
  
For example a literal if syntax can be implemented as a meta abstraction

```
   meta $if = $cond then $t else $d => $cond ? True -> $t | False -> $d
```
  
Meta evaluations are done by 
```
  $( expr ) 
```

as in for example 

```
  meta addConstWord = a => b => $( $a + $b )
```
this will reduce to a number or fail.
If it should be optional optimization, the following code
will fall back to an unreduced expression: 
```
  meta addConstWord = a => b => $a + $b       # SFINE fallback
  meta addConstWord = a => b => $( $a + $b )  # try to evaluate
```

### Variable declarations and memory management  ###


For dynamic memory allocation metacore uses four stacks:

  1. The argument stack, used to store argument data.
  2. The result stack to which results are constructed.
  3. The shunt stack used as an intermediary memory location for allocations
     that should survive function/method return scopes,
  4. The call stack for return addresses. 

The stack layout with the corresponding growth directions can be visualized
as:
```
    +--------------     ------------+
    | Arguments ->  ...    <- Shunt | 
    +--------------     ------------+
    | Results ->    ...    <- Call  |
    +--------------     ------------+
```

The argument and result stack can be swapped to exchange roles, since
the grow in the same direction. The same holds for the shunt and data stack. 

#### The global expression ####

Global variables have fixed addresses at run-time, they will not consume
any memory if captured in closures. The for value bindings the
syntax is analog to the let expression below

```
global
      v0 : TYPE0
      v1 : TYPE1
      ...
      vK : TYPEM
      d0 : TYPE0 = EXPR0
      d1 : TYPE1 = EXPR1
      ...
      dM : TYPEM = EXPRM
   where
      v0 = TYPE0
      v1 = TYPE1
      ...
      vK = TYPEM
      l0 : TYPE0 = LOCAL_EXPR1
      l1 : TYPE1 = LOCAL_EXPR1
      ...
      lN : TYPEM = LOCAL_EXPRM
 in EXPR
```
where the bindings in the where part are utility variables and not visible in 
EXPR. 
The global instruction has additonal syntax for declaring, unions, structs,
and interfaces. See below. 
 
#### The let Expression ####

There several variants to define variables that are different on where
on the Stacks the data is allocated. They all have analog syntax like: 

```
let@R 
      v0 : TYPE0
      v1 : TYPE1
      ...
      vK : TYPEM
      d0 : TYPE0 = EXPR0
      d1 : TYPE1 = EXPR1
      ...
      dM : TYPEM = EXPRM
   where
      v0 = TYPE0
      v1 = TYPE1
      ...
      vK = TYPEM
      l0 : TYPE0 = LOCAL_EXPR1
      l1 : TYPE1 = LOCAL_EXPR1
      ...
      lN : TYPEM = LOCAL_EXPRM
 in EXPR
```
with to create data on the arguments/results stack. 
The values l0 ... lm are local implementation helpers not visible in the 
'in'-expression, but are visible in EXPR* bindings. The @R denotes
a region name that is used for region checking to eliminate the possibility
of "dangling" pointers. 

TODO:
The sizes of all types can be computes as constants of using variables names
that are defined in outer bindings, since they will not change due to
weak referential transparency. So then no frame pointers as in the usual
C stack frames need to be saved or managed. Additional dynamic allocations
have to be made using the shunt stack.

Lets assume that before the evaluation of the let expression, the stack
has the following layout: 

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
```    

   - Then ASP and RSP are swapped. This will spread data out over
     two memory areas for runtime nestings of let expressions. 

```
        RSP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       ASP=rsp0                                                      CSP=csp0
```


   - Allocate region R on the results stack to store n0 ... nN

```
                             RSP=rsp1                                  SSP=ssp0
          |<--------R--------->|                                         |
    +-----+--------------------+----     --------------------------------+---+
    | ... |v0..vk,d0..dM,l0..lN|     ...                                 | . | 
    +----++--------------------+----     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |       
       ASP=rsp0                                                      CSP=csp0
```
     The local bindings are on the same region, they are just not visible
     in the final expression evaluation, which is useful for defining module
     interfaces and avoiding name clashes of helper functions. Locality in
     this context is not about memory management.  
     If declared values need run-time type information construction,
     like for example of allocating array depending on an variable size,
     this type information will also be stored in R. 

   - Then the expressions EXPR* and LOCAL_EXPR* and results are stored in region 
     R. The evaluation is in order of the bindings ('=') not
     necessarily in the blocks with first EXPR* then LOCAL_EXPR*. They can
     be mixed. The order is arbitrary. 
     We assume that the evaluation will create shunt values s0..sP so that
     the memory layout is then:
```
                             RSP=rsp1                           SSP=ssp1
          |<--------R--------->|                                  |
    +-----+--------------------+----     -------------------------+------+---+
 | ... |v0..vk,d0..dM,l0..lN|     ...                                 | . | 
    +----++--------------------+----     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |       
       ASP=rsp0                                                      CSP=csp0
```
     The local bindings are on the same region, they are just not visible
     in the final expression evaluation, which is useful for defining module
     interfaces and avoiding name clashes of helper functions. Locality in
     this context is not about memory management.  

   - Then the expressions EXPR* and LOCAL_EXPR* and results are stored in region 
     R. The evaluation is in order of the bindings ('=') not
     necessarily in the blocks with first EXPR* then LOCAL_EXPR*. They can
     be mixed. The order is arbitrary. 
     We assume that the evaluation will create shunt values s0..sP so that
     the memory layout is then:
```
                             RSP=rsp1                           SSP=ssp1
          |<--------R--------->|                                  |
    +-----+--------------------+----     -------------------------+------+---+
    | ... |v0..vk,d0..dM,l0..lN|     ...                                 | . | 
    +----++--------------------+----     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |       
       ASP=rsp0                                                      CSP=csp0
```
     The local bindings are on the same region, they are just not visible
     in the final expression evaluation, which is useful for defining module
     interfaces and avoiding name clashes of helper functions. Locality in
     this context is not about memory management.  

   - Then the expressions EXPR* and LOCAL_EXPR* and results are stored in region 
     R. The evaluation is in order of the bindings ('=') not
     necessarily in the blocks with first EXPR* then LOCAL_EXPR*. They can
     be mixed. The order is arbitrary. 
     We assume that the evaluation will create shunt values s0..sP so that
     the memory layout is then:
```
                             RSP=rsp1                           SSP=ssp1
          |<--------R--------->|                                  |
    +-----+--------------------+----     -------------------------+------+---+
    | ... |v0..vk,d0..dM,l0..lN|     ...                          |s0..sP| . | 
    +----++--------------------+----     -------------------------+----+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |       
       ASP=rsp0                                                      CSP=csp0
```
   - Then ASP and RSP are swapped back, turning results into arguments. 

```
         ASP=asp0                                                SSP=ssp1
          |                                                       |
    +-----+--------------------+----     -------------------------+------+---+
    | ... |v0..vk,d0..dM,l0..lN|     ...                          |s0..sP| . | 
    +----++--------------------+----     -------------------------+----+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |       
       RSP=rsp0                                                      CSP=csp0
```
     The let bindings are now the arguments available for evaluationg EXPR
     and properly stored on the argument stack.  
   

   - The memory for the result r is allocated on the result stack
     and EXPR is evaluated storing its result in r. 

```
                             ASP=rsp1                           SSP=ssp1
          |<--------R--------->|                                  |
    +-----+-------------------------     -------------------------+------+---+
    | ... |v0..vk,d0..dM,l0..lN|     ...                          |s0..sP| . | 
    +----+++-------------------+----     -------------------------+----+-+---+
    | .. |r|                         ...                               | ... |
    +----+-+------------------------     ------------------------------+-----+
           |                                                           |       
          RSP=rsp1                                                   CSP=csp0
```
   - Freeing region R. 

```
         ASP=asp0                                                 SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |s0..sP| . | 
    +----+++------------------------     -------------------------+----+-+---+
    | .. |r|                         ...                               | ... |
    +----+-+------------------------     ------------------------------+-----+
           |                                                           |       
          RSP=rsp1                                                   CSP=csp0
```
    
The above let expression is actually an syntax abbreviation for:
```
let local@R
    ... 
```
so that an syntax sugar form 
```
let shunt
    ... 
```
can be introduced, that removes the need for handling the references
created by explicit "shunting" (see below). 

#### Implicit let expressions ####

Complexer expressions, for example, a seqence of applications
```
 f x y z
```
are reduced to simple expressions inserting let expressions to make the
memory handling clear. The above express




... TODO: explain further

DEPRECATED(?)



The second variant is 
```
let shunt@R
    ...
```
where R is a region name where applicable.  

There might be a fourth variant 
``` 
let temp ...
```
which will be decided when the exact stack handling and value copying
semantics is tinkered out.  

#### The shunt region expression ####

The rational of shunting will be explained below. For C programmers the
shunt expression is like a malloc-free enclosed block, where the allocated
memory is used as a stack.  

The shunt region expression has the syntax

```
 (@R Expr )
```

Unlike region of the let expression, the shunt stack regions do not have
a defined size. It is required to save stack frame information.

If r is the allocation for the result of EXPR, the stack while evaluating 
the above expression looks like

To define the memory semantics of the shunt expression lets assume, that
the initial memory layout is

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
```    
Then evaluating the expression has the following steps

  - Allocate the result memory

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |r|ssp0|                    ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                      CSP=csp0
```    
  - Allocate result memory and save the current shunt stack pointer 
```
        ASP=asp0                                                       SSP=ssp0
          |                                         ... -----R---------->|
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |r|ssp0|                    ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                      CSP=csp0
```    

  - Evaluate Expr. If we assume that this creates the shunt values
    s0..sP, the memory looks like 

```
        ASP=asp0                                                SSP=ssp1
          |                                         ... -----R----|----->|
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |s0..sP| . | 
    +----++-------------------------     -------------------------+----+-+---+
    | .. |r|ssp0|                          ...                         | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
```    
  - Remove the stored shunt stack pointer and free shunt area 

and after evaluating the complete expression the stack is
```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |r|                         ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
```    

#### Manual garbage collection ####

The shunt and the code stack allow to define a manually triggered copying
garbage collection. The corresponding expression is 

```
  collect@R EXPR
```
which will use a stack swap and deep copy combination in the following way.
TODO: Add changed global mutable variables to the root set for the mark-
      and seep copy, if region checking can be done for that case.  

Let the initial memory layout be: 

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
``` 
The memory semantics of the collect expression are: 

The first step is swapping the code and shunt stack

 - Swap shunt and call stack for simple mark
   and sweep deep copy implementations and for good nesting behavior. 
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       SSP=csp0
``` 
 - allocate the result of EXPR on the result stack and save the current
   shunt stack pointer. This allocates the region R
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++------------------------     ------------------------------+-+---+
    | .. |r|csp0|                    ...                               | ... |
    +----+-+------------------------     ------------------------------+-----+
                |                                    ...-----R-------->|
              RSP=rsp1                                               SSP=csp0
``` 
 - Evaluate EXPR that creates the shunt values s0..sP.

```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++------------------------     -----------------------+------+-+---+
    | .. |r|csp0|                    ...                        |s0..sP| ... |
    +----+-+----+-------------------     -----------------------+------+-----+
                |                                   ...------R--|----->|
              RSP=rsp1                                        SSP=ssp1
``` 
 - Swap code and shunt stack 
```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++------------------------     ------------------+-----------+-+---+
    | .. |r|csp0|                    ...                   | s0 ... sP | ... |
    +----+-+----+-------------------     ------------------+-----------+-----+
                |                                   ...----|-R-------->|         
              RSP=rsp1                                     CSP=ssp1
``` 
 - With r as root value, do a mark and sweep copy of each value. Marking can 
   be done by overwriting the addresses of the copied values with the address 
   of the copy, then the value of pointer pointing to the current shunt stack, 
   which is not region R anymore, has been copied. Let c0...cQ be the data 
   that is reachable though r and thus has been copied. Then the memory layout
   after this step looks like.     
```
        ASP=asp0                                                  SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++------------------------     ------------------+------+----+-+---+
    | .. |r|csp0|                    ...                   | s0 ... sP | ... |
    +----+-+----+-------------------     ------------------+-----------+-----+
                |                                   ...----|-R-------->|         
              RSP=rsp1                                     CSP=ssp1
``` 
  - Dealocate the shunt region R

```
        ASP=asp0                                                SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++------------------------     -------------------------+----+-+---+
    | .. |r|                         ...                               | ... |
    +----+-+------------------------     ------------------------------+-----+
           |                                                           |         
         RSP=rsp1                                                     CSP=csp0
``` 


Notes on the run-time performance: Mark and sweep copy is linear complexity,
and and the this kind of collection can not worthen the complexity of the
algorithms generating the garbage, it has to generate the values copied anyway.
TODO: A bit flag may be better then using region information, its faster 
to check and may handle global variabels more efficient.  

The above collect instruction almost makes a copying garbage collector,
however with one caveat, if we where to have a loop over the collect
expression it will fill the outer shunt stack scope, what is missing 
is a way to manually swap the roles of shunt and call stack. 
For example the following construction will not properly garbage collect:

```
let tailCallLoop = st -> do collect@R stepFun st
                            tailCalLoop st
```
It will only collect garbage that goes becomes unreachable immediately. 
One values have been deep-copied they will not be deallocated, since
they "left" the temporary shunt regions R. 
Trying to do something clever with the call ABI seems not be viable.
The most simple solution is an additional memory management command,
or an extension of the collect statement making it proper loop.

```
let someLoop = st -> collect@R stepFun st until st.done
                          
```
however that has the problem that for odd number of iterations
there must either be an additional copy, so that regions are consistent
address spaces. 
The looping form of collect the memory sections used for the argument
and shunt stack will alternate with each step, creating the basic
copying gc semantics, on the current "part" of the data. 
This is scheme is very efficient and has been tested in a custom C++ sparse 
matrix multiplication code - for which however the deep copies where 
simple memory copies. This scheme creates nice memory locality on "sparse" data. 

The semantics of the loop is a bit elaborate, to keep region names
consistent, there are slightly different semantics for odd
and even iterations. 

- loop prelude 
- loop with either,
   - odd step or break
   - even step or break
- loop cleanup
   - if coming from odd step, perform an additional copy 
   - clean up the helper region R
 

- The prelude steps start with a memory layout of the form

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----++-------------------------     ------------------------------+-+---+
    | .. |                           ...                               | ... |
    +----+--------------------------     ------------------------------+-----+
         |                                                             |
       RSP=rsp0                                                       CSP=csp0
``` 
  - In the prelude memory r for the return value, i for a step counter
    as well as the current SSP and CSP are stored. i is inialized to 0, i.e.
    the loop will start with an even step. 

```
        ASP=asp0                                                       SSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     ------------------------------+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        CSP=csp0
``` 
  - Before entering the loop steps SSP and CSP are swapped:

```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     ------------------------------+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        SSP=csp0
``` 
  - Then the even step 0 is executed. 

- The loop stage has to alternatives, even steps i=0,2,4,6,... and odd 
  steps i=1,3,5,7,...
  - For the even stage the stack layout is given as
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     --------------------+---------+-+---+
    | .. |r|i|ssp0|csp0|             ...                     |c'0..c'Q'| ... |
    +----+-+-+----+----+------------     --------------------+---------+-----+
                       |                    ...--------------|----R--->|
                      RSP=rsp4                              SSP=ssp1  csp0
``` 
    where c'0..c'Q' is a list of values deep-copied from the previous odd step.
    For the first step its an empty list.  
    after evaluating (stepFun st), which we assume to generate the shunt values
    s0..sP, the layout is:
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     --------+-----------+---------+-+---+
    | .. |r|i|ssp0|csp0|             ...         | s0 ... sP |c'0..c'Q'| ... |
    +----+-+-+----+----+------------     --------+-----------+---------+-----+
                       |                    ...--|-----------|----R--->|
                      RSP=rsp4                 SSP=ssp2     ssp1      csp0
``` 
  - Then a mark and sweep copy is performed, and region R is deallocated after
    that.
```
        ASP=asp0                                                 CSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++-+----+----+------------     -------------------------+----+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        SSP=csp0
``` 
  - After that csp and ssp are swapped: 
```
        ASP=asp0                                                 SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++-+----+----+------------     -------------------------+----+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        CSP=csp0
``` 


   - the until condition is checked if its true the loop is aborted and 
     the cleanup code is executed. If not the variable 'i' is increased by 1
     and an even step is executed

- For an odd step the memory layout is of the form
```
        ASP=asp0                                                 SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++-+----+----+------------     -------------------------+----+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        CSP=csp0
``` 
  - The first step is to evaluate (stepFun st) creating the shunt values
    s0 ... sP and resign a fresh region R. This results in the stack layout: 
```
        ASP=asp0                                       SSP=ssp1         ssp0
          |                                 ...---------|------------R-->|
    +-----+-------------------------     ---------------+---------+------+---+
    | ... |                          ...                |s0 ... sP|c0..cQ| . | 
    +----+++-+----+----+------------     ---------------+---------+----+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        CSP=csp0
``` 
   
  -  Then SSP and CSP are swapped, a mark-seep copy is performed, and R is 
     deallocated
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     --------------------+---------+-+---+
    | .. |r|i|ssp0|csp0|             ...                     |c'0..c'Q'| ... |
    +----+-+-+----+----+------------     --------------------+---------+-----+
                       |                                     |
                      RSP=rsp4                             SSP=ssp1
```
  - the until condition is checked if its true the loop is aborted and 
    the cleanup code is executed. If not the variable 'i' is increased by 1
    and an odd step is executed

- The cleanup stage works as 
   - check if i is even or odd
   - if i is odd, the stack layout is of the form
```
        ASP=asp0                                                       CSP=ssp0
          |                                                              |
    +-----+-------------------------     --------------------------------+---+
    | ... |                          ...                                 | . | 
    +----+++-+----+----+------------     --------------------+---------+-+---+
    | .. |r|i|ssp0|csp0|             ...                     |c'0..c'Q'| ... |
    +----+-+-+----+----+------------     --------------------+---------+-----+
                       |                                     |
                      RSP=rsp4                             SSP=ssp1
```        
   and the copied shunt values need to be copied back into the outer shunt 
   region, and CSP and SSP need to be swapped. This yield the stack layout
   of an break in an even step. 

   - For an even step the stack layout is: 
```
        ASP=asp0                                                 SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++-+----+----+------------     -------------------------+----+-+---+
    | .. |r|i|ssp0|csp0|             ...                               | ... |
    +----+-+-+----+----+------------     ------------------------------+-----+
                       |                                               |
                      RSP=rsp4                                        CSP=csp0
```  
  - The cleanup remaing is to dealloce everything but the result, yielding
```
        ASP=asp0                                                SSP=ssp1
          |                                                       |
    +-----+-------------------------     -------------------------+------+---+
    | ... |                          ...                          |c0..cQ| . | 
    +----+++------------------------     -------------------------+----+-+---+
    | .. |r|                         ...                               | ... |
    +----+-+------------------------     ------------------------------+-----+
           |                                                           |
          RSP=rsp1                                                    CSP=csp0
```  

Aside from an unused value elimination the '(collect@R EXPR)' is equivalent
to '(collect@R EXPR until True)'.

#### The shunt expression ####

The shunt instruction is used to allocate values on the shunt stack.
It returns a reference to the allocated value and has the following

```
shunt EXPR
```

Let r be the memory required to store the result of evaluating EXPR
and []r is the address of r, then
the memory layout after shunting is:
```
        ASP=asp0                                                    SSP=ssp1
          |                                                          |
    +-----+-------------------------     ----------------------------+---+---+
    | ... |                          ...                             | r | . | 
    +----+++------------------------     ----------------------------+-+-+---+
    | .. |[]r|                         ...                             | ... |
    +----+-+------------------------     ------------------------------+-----+
           |                                                           |
          RSP=rsp1                                                    CSP=csp0
```  

TODO: Shunts for existential bindings. 


##### Shunting as programming technique #####

Shunting is the method to implement a programmer controlled guided garbage
collection. It has clear runtime-costs and timing semantics and thus provides
hard real-time capabilities.  

The features of having explicit references, that translate to low level pointers 
with save manual memory management, implies the need for region inference
and checking system
that rejects code with dangling references. However, this will cause truble. 
For example the following code 
```
  y = f (g (f' x)) 
```
could be rejected by the region checker, if g returns a reference into the 
result generated by (f' x). The above is equivalent to 

```
 y = let@R i = let@Q h = f' x 
                in g h  
      in f i 
```
where we assume that g returns a pointer into region Q, and thus the code
will be rejected by the region checker.  

If the programmer however needs the above codes structure he can use
shunting to "pacify" the region checker. Shunting is introducing 
a region on the shunt stack, to hold the required results and
shunt the values whose pointers are taken into it: 

```
  (@R f x (g *(shunt f' x))) 
```

### Regions ###

A region inference and checking system is used to prohibit the creation of 
dangling pointers and avoid requiring traditional garbage collection.

A region is a non necessarily contiguous set of memory addresses. 

While there is a subset relation indicating a natural order, it is due
to the 4 stack layout impractical to used. 

The proper property to avoid creation of dangling pointers is the time
of deallocation which will always be defined by syntactic scopes. 

The shunt stack can be used to copy data out of regions that are to be
deallocated. Deallcoation of the shunted values is then handled with manual  collect instructions, providing manual garbage collection. 

For example copying a local value to the shunt stack, 
```
(@S   # add layout syntax?
  let@R
    foo = ...
    in shunt foo # copy foo@R out to region S and return a address to the copy
)
```

The creation and deletion of regions is bound to syntactical constructions,
the deallocation-time ordering is therefore a total order. While a simple worst case region annotation is sufficient for abstractions as in
```
   ( x@a : T ) -> something@r : Y 
```
and which has a type of

```
   ( T@a => Y@r ) 
```
it is not possible to decide in which directions pointers are references
are allowed in nested cases, for this additional syntax is needed:  


```
  ( x@a -> y@b(a<=b) -> z@c(a<=c,b<=c) -> ... )
```

Where (a<=b) reads a is deallocated before b, thus pointer from b to a 
are prohibited. 

Type information created at run-time like for dependent sized arrays,
must be stored in the same region as the array itself, to avoid dangling
argument-type pointers. 

For an closure type like
```
    T@a => Y@r 
```
where the codomain region is a free variable, the region can be elided.
```
   T@a => Y
```
which since @a is not used either can be elided too
```
   T => Y
```
.




### Unions ###

Unions are implemented as tagged unions also known as generalized algebraic 
data types are given by. For example: 

```
global union Lam : Type => Type where
         Lift : $a => Lam $a                        # lifted value
         Pair : Lam $a => Lam $b => Lam ($a, $b)    # product
         Lam  : (Lam $a => Lam $b) => Lam ($a->$b)  # lambda abstraction
         App  : Lam ($a=>$b) => Lam $a => Lam $b    # beta reduction 
         Fix  : Lam ($a=>$a) => Lam $a              # fixed point
        in ...
```

in particular (union ... where ...) is an expression of type type. 

### Structs ###

Structs are constructed as 

```
global struct Foo $a $b where 
         bar  : $a #  
         bla  : $b
         bla2 : $b
```
and will define corresponding methods (maybe reduced to classes)

The above will yield a type value Foo with a type
```
Foo : Type => Type => Type
```

#### Value Construction and Pattern Matching variants ####

Structures can be created in several ways. 

##### Direct Anonymous Per Name #####

```
  x = (Foo a b c) # x.bar == a && x.bla == b && x.bla2 == c
```

##### PostFix Block Construction #####

The postfix period indicates the start of a layout block, so that

```
  x = Foo. bar  = a 
           bla  = b
           bla2 = c
```

which is using the layout rule equivalent to  

```
  x = Foo.( bar = a ; bla = b ; bla2 = c )
```

##### PostFix Block Copy and Update Construction #####

Sometimes, a struct needs to be copied with only one some updates. 
This is common in cases where mutable variables are avoided. 

Creating a copy of a struct x with some one argument also uses
postfix '.'  , for example

```
   y = x. bla = foo 
```

is short for

```
   y = Foo. bar  = x.bar
            bla  = foo 
            bla2 = x.bar2
```

##### Design Comment #####

The above PostFix Copy and Update Construction should be useful for tuples
too, making tuples too. For that matter tuples places need method names,
that will just default to elem* starting with elem0, for example. 


```
   y = x. elem1  = foo 
           
```

#### Design Comment ####

Structs are entities of their own right the constructor and providing new-type 
like semantics. In theory they could be reduced to tuples and classes, but 
then a separate new-type would be needed making that a questionable 
simplification also for mutable variables extra class instances would have 
to be crated. 

Complexity is there in any case. (I banged my head long enough at making 
everything as simple as possible and reducing it to an absolute minimal base
functionally - See Appendix "The bane of sophisticated simplifications") 


### New Type ###

New types are introduced by using the type keyword type.

For example: 

```
  global type Foo a b = (a,b)
```

and value construction is done via

```
  let y = Foo (x,y)
```


### Nesting Type Declarations ###

Since type constructions correspond to global type value constructions
its possible to nest them, for example:

```
global 
   struct FooAST $x $y where
       tag : union FooASTTag : Type where 
               Foo : X => $y => FooASTTag
               Bar : $y => Y => FooASTTag
       pos : Pos
       foo : $x
       bar : $y
```

As syntax sugar the first member name of a structure can be
omitted, Yielding the following pattern matching syntax example: 

```
ast ? FooAst.( Foo a b ; pos = p) ->  ...
```


### Conditional Logic Symbols ###

The following logical operators are included as syntactic elements
with their own semantics: 

- && for "and" . The rhs is not evaluated if the lhs is false
- || for "or" . The rhs is not evaluated if the lhs is true

These are not implemented as functions, since functions would have to
evaluate both arguments before "deciding" the results. s the logical result.

#### Design Note ####

In the syntax '|' is used for function case alternatives not booleans, 
and '&' to introduce pattern guards.
   

Using the c-style && and || allows to make array comprehension like

```
  { f *x *y | x <- foo, y <- bar & isBla x && isbla y }
```
Where is for mutable updates. Consistency x and y must have load prefixes, 
if the value and not the mutable ref is the argument.

TODO: Tinker with variants and improve. The above overloads mutable
update semantics, maybe that can be improved.  

### Implicit Casts ###
Since every name points to a type declaration it is possible to to implicit 
casts. It is open if there will be an extensible api for that, but it could
be used to represent arrays as tuples and reserve the braces for record
update syntax.  


### Sequences and arrays ###:  

An sequences are the array type of metacore. To be more precise a sequence
of non-zero length is an array. 

Sequence use C style array syntax for type definition. The
three variants to declare an sequence are

```
  let xs : T[n]   # no expression for n allowed, just a variable name  
      ys : T[42]
      zs : T[2^n] # power of two size, for fast index moduo calculations. 
```
is an array of Ts of size n where n is a Word bound in an outer scope in
the first case and of type %wordBitLength in the last case.  

The typechecker will require that all indices are of the (dependent) 
type %n. This removes the requirement for runtime index checks, and 
makes array access save, however non-trivial index calculations are
expensive. (Note, the initial design had modern PC processors in mind,
however a lot of simple processors do not have hardware division
making this impractical.

Constructors: 

```
 {}                     # empty sequence
 {c0,v1,...,vN}         # nonempty sequence
 { ... for ... in ... } # comprehension
```

the comprehension uses the dynamic size return, like read so that
in a let expression the size must be bound. 

```
  let [n] xs = { ... for ... in ... } 
```

This adds a pointer indirection, but otherwise dynamic binds would only
be possible for the last element in declaration block of "let" and its variants.
This however will always put the data on current tmp scope, since the 
corresponding stack grows up, and filtered values can be added with
side effect order remaining intuitive while wasting no memory.
Shunting list comprehension will thus involve an additional copy.  
Arrays are non empty sequences, their type is thus given like
```
  let ys : xs[NonZero %n]
```
where NonZero is builtin predicate. The array access syntax but is 
only defined for if n : NonZero Word.
```
  xs[i]
```
where i : %n . Using modulus classes the typesystem ensures that there
are never invalid array indices. 
To allow other containters with the same syntax x[i] is short for (x.at i).
which is short of (.at x i) So that for example: 

```
f = environ["MY_OPTION"]
```
Good idea? (It is common in other languages ... but environ.at "MY_OPTION"
or something is just fine. )


### Dependent Types ###

Weak referential transparency allows a simple safe form of dependent types 
where the dependent parameter is a bound name, for example creating modulus 
classes for array indexes is done with

```
  global 
    interface Num t where
      infix%   : t => (n : NonZero t) => %n
      infix%2^ : t => (n : %wordBitSize) => %2^n
      ...
```
where ( (varName:type) => r) is the only form of dependent type supported.  

These types imply that

```  
x % m : %m  
x%2^m : %2^m  
```

The type checker will ensure that applications of (n:t) typed arguments are 
exactly a name. The classically typed modulo are `umod` and `smod` for signed
and unsigned ops.

The above operator is required for index computations that are used to access
arrays. This makes array access as fast as in like in C but save.
Only index calculations can be slower. However in many applications, for example
sparse matrix multiplication, the indices are precalculated and stored, making
this a tolerable trade of.   


### References and Mutable Variables ###

References in metacore are very idiosyncratic. They correspond to collage 
of pointers, constant references  and existential qualification in other
languages.

References are used for the following purposes. 

  1. Provide a save interface to low level pointers. 
  2. Implement mutable variables while maintaining weak referential transparency.  
  3. Allow some additional existential variable captures and abstractions. 

Metacore supports the following modes of creating  references
  
  1. Const. ref. to shunted value
  2. Const. ref. to temp value
  3. Mutable ref. to shunted value
  4. Mutable ref. to temp value.

having the ability to existential capture capture has the following
implications on the above cases.

 - For case : None, the type of the expression implies existential bindings. 
 - Otherwise: The existential binding must be defined. Since for example non
              existentially bound array sized will become dependent type 
              parameters    

to create references from an expression syntactically could be done
via
```
[] expr
```
however expressions must have a preallocated target region, so that could
default to the temp region (i.e. result stack allocation)

for the const ref generation the complete shunt syntax should be
```
[n1,...,nN] shunt valueToShuntCopy
``` 
for mutable variables the following syntax
```
[n1,...,nN]* shunt valueToShuntCopy
``` 
where shunt is part of the reference construction. Having a complexer
reference type allows to add an "uninitialized" attribute as well
so that
```
[n,m]* shunt uninit : Float[n,m] 
```
where uninit is a keyword and attribute name. 
Would allocate a value yield a reference value of type 
```
 ([n,m]* uninit Float[n,m]@r)
```
where r is the name of the current top shunt region. 
For uninitialized temp mutable the allocation would be
```
 ([n,m]* uninit : Float[n,m]@t)
```
where t is the  region name of the top temporaries region.
Loading values from references is done with a prefix *. Anything other
then updating the an uninit mutable reference is an attribute error and causes
compilation to fail. 
Further as syntax sugar it is possible to abbreviate
```
[] shunt foo 
```
with
```
 shunt foo
```
Note that the load operation .prefix* must always create a copy of the value
stored in them mutable as explained next: 


#### Mutable Memory Data Races ####

Aliasing mutable pointers can be a problem in languages for example: 

```
let union Bla where
      Bar : Bool => Word => Bla
      Foo : Word => Bool => Bla

y <- Bar True 3 
y <- Foo 3 (f y)  
```
If metacore would do a direct inplace memory update from left to right
in the above code, then when calling f the value of y would be corrupted.
It would be holding a word in a bool position and 3 is not a element of Bool.

However there is a stage translating everything to a form where each temporary 
has a name. In fact the above is equivalent to 
```
let temp x = f y 
y <- Foo 3 x  
```
and works just fine. Its just a slightly different evaluation order for 
side effects. 
Its not simple from top to down and left to right. But first from top to down 
and left to right name an shift all temporaries. Then evaluate from top to down 
and left to right. The case
```
y <- Foo y
```
this is correct as well. Due to weak referential transparency the value of y #
as references does not change only its content
will be updated with content having a mutable pointer to itself then. That
is also no region error. In a single scope region references do not need to 
form an acyclic graph.   
The final type of problem seems to be updates like
```
y <- Foo (f *y)
```
that would be a problem if metacores load operator did not load the value
to a fresh location. But it does and lets the optimizer remove unnecessary
copies. The basic continuation passing style implementation of the Reforth 
abstract target machine with LLVM depends on it, and it works very fine.   

Concurrent access might pose a problem but will be solved differently, maybe by 
an always copy on write semantics unless allocated as shared. 

To summarize Metacore does not need any restrictions on how many mutable
references can be created to avoid data races. Load and store are always 
atomic and a representation of a value is constructed completely before stored.
The rest is handled by constant propagation. This just makes the worst case
update time for a value longer in a real-time context, since in the worst case
a creation and copy operation and doubles potential memory usage. But for cases 
not handled by advanced optimizers like the once included in LLVM allowing the 
programmer to omit a copy is likely a potential data-race under some conditions.  
Later some cases will be defined where copy elimination is a mandatory compiler
optimization, which is sufficient and does not restrict the language flexibilty. 

### Tail Calls ###

For simplicity only abstractions with () can be tail-called. All relevant

```
let fac : Word -> Word
  where
    fac = n ->  
       let temp  
          i : []* Word # allocate a mutable word 
          r : []* Word # allocate a mutable for the result
          i = []* 1 # the allocation and initialization of the mutable variable
          r = []* 2
          fac' = () -> n != *i ? True  -> () 
                               | False -> do r <- r * (*i + 1) 
                                             i <- (*i + 1)
                                             tailcall fac'
       in do fac' ()
             *r # this yields value of the mutable and stores it to the
                # current destination address (of the function in this case) 
```
The alternating result/argument stack semantics makes reusing arguments for
a tail call bit tricky as optimization. But the above is sufficient to be able 
to construct anything. Might require some work on factorizing common patterns
into abstractions/macros. That fac' there is a for loop actually. 

Thats quite a lot code for a fac, but it is close to the metal.  

The do is not monadic, just a list of expressons that resetting the result 
and argument stack after each evaluation and
evaluates to the last expression. The implicit monad operators would "steal" the
tail-calls tail position and make things tricky and I was just looking for a 
excuse to remove monads completely.

... might add basic loop semantics as well then, decided to add a collect
loop as well

do ... until  

do while ...
   ... 


### Panic and Defer ###

Exception handling capabilities are given by a panic and defer keywords 
adapted from Go. A recover is not required but integrated into the defer
semantics. A deferred expressions must be of type (Maybe x) if it yields
a (Just foo) panic stops the corresponding function scope evaluates to
foo. For example

```
  do open fd "aPath" ? IOError _ -> errorhandle # fd is a mutable.
     defer p -> do close(fd) # p is the panic value 
                   Just foo  # the recovery value. 
     ...
```
the panic value is value of any type provided to panic.
If its () of type () its a normal cleanup defer, and the recovery value
is discarded. If p : AnyNotUnitType the surrounding function scope
will evaluate to the recovery value, and panicking stops
if the defered function does not yield a Maybe, it will treaded
as yielding nothing and unable to recover. That brief and correct
resource management
```
do open fd "aPath" ? IOError _ -> errorhandle # fd is a mutable.
   defer () -> close(fd) 
```  
One of panics important usages are out of memory panics. The old C-style
of assuming out of memory is common i.e. all allocations have to be checked
is usually not adequate and spams algorithm implementations with memory failure
logic.
In any case as for the above IO api examples common failure modes are should
be part of an algorithm, and not use the panic interface. In that regard I
absolutely agree with the Go designers.

The access to C- malloc/free should be done the same way. I.e. returning
a mutable of a pointer value. Free NULLs the pointer value, making double
frees impossible and the worst case scenario is memory leaks but no undefined
behavior.

Could define a variant of Maybe called RecoverT a with tags
Recover a and Continue. Making defer blocks more readable. 

#### Rationale  ####
First of all I do not like C++ style exception handling. Its too runtime
complex and binary bloaty. Technically it is a zero cost abstraction, though.
A kind of setjump based system has virtually no binary overhead, and
runtime overhead only where it is syntactically indicated.
Go's panic system is of the later variant, and completely sufficient.

If the design forgos recoverable panics or exception handling there is a 
problem with memory management that should be implicit. Otherwise like with 
c malloc every allocation could fail and needed to be checked. 
Thats out of question. There must at least be an "out of memory" panic/exception. 


### Basic IO interface ###
  
That needs a lot of further work. 
By example: 
```
  union IOError : Type where
    IODone  : IOError # no error Concition
    IOError : Word -> IOError # or something like this as error condition 
 
  readline :  () => [n] Byte[n]@shunt
 
  open : &FD => Path => IOError 
 
  read : &Byte[$n] => IOError  

  load_file : String => [n] Byte[n]@shunt 
```

that could be used as for example

```
 do n = readFromUser ()
    tmp *buf : Word[n] # implicitly construct the type-value of Word[n] in the let scope
    read buf ? IOError x -> do errorHandling
    processData *buf      
```

This implies that its not directly possible to return a locally allocated
buffer. Like in C  - where you can actually, but its undefined behavior.
For this the shunt stack can be used. It is not affected by closing the tmp 
scope.  

TBD:
Rename file descriptors to resource descriptors. There is unixs "everything is
a file" paradigm, but that nomenclature is not very useful imho. Like any
"everything is a Y" paradigm, because then knowing that an entity is a Y does 
not provide information content - aside that Y exists - maybe. 

One of my earliest problems with traditional object orientated paradigms, is
that the essence of an object is that it is data+methods, therefore everything 
that can exist on a Turing machine. 

Resource descriptor will properly  point to something that needs some 
finalization to give allocated resources back. 


Appendix:
=========

Weak Referential Transparency and Dependent Types:
--------------------------------------------------

  Prior experience showed that strict referential transparency is not a good 
  design decission if fast random array access is needed and the compiler 
  optimizer should be allowed to be relatively stupid for a good reference 
  implementation. Since premature optimization is the root of almost all evil, a
  weak kind of referential transparency is enforced. 

  Weak referential transparencey means that once a name is assigned the value 
  never changes. This requires mutables to refer to locations not the 
  containted vales.

  This is a necessary condition to have proper dependent types, i.e. named array
  sizes, which must of course be not muteable. 

  Mutables are thus implemented as locations. After writing stuff to a location
  its still the same location. So this is consistent with weak referential 
  transparency. Also makes resolving pointer aliasing easy in many cases. 

  ...





Commentary on Idris Concepts:
-----------------------------

### Total functions ### 

This guaranteed termination (and exception freeness). Not feasible, would
have to guarantee that there is enough memory for the operation to be performed. 
This is not locally decidable ... . So the assumption in Idris seems to be 
that memory allocations do not fail, need read up on that.   

Type abstractions should be total functions. That guarantees termination
of the type checker ... but that will not be enforced, if the compiler
terminated so did all type evaluations, if not no invalid program was
generated ... 


### Holes ###

Could be implemented. Will cause program failure when evaluated, but
still be useful in combination with tests. Defining test *will* be
a language feature! As I consider writing test the proper way to 
debugging. The test remain after a bug was found and make it harder
to introduce new ones. Further they then tell you if a seemingly small
change is really small, or invalidates a lot of tested assumptions. 

### First Class Types ##

Have been adapted with some differences.  
 

Some lessons from Go
--------------------

Writing some commercial go code and switching back to the ultra "dense" hasekll
code made me realize the following:

 - Very dense and compact code is hard to read especially when tired,
   go is not very dense or compact, which is easy. Haskell is extreme in both.
 - Having abstractions like monads makes the code hard to adopt since
    - all looks nice and clean
    - important internals are hidden and monadic computations may not be
      combined directly, for example since they may abstract different state
      types i.e. they hide too much
    - trying to refactor stuff yields complex type errors if the monadic
      sub-structure is complex.
    - in conclusion monads yield nice and clean code that may or may not be 
      reused without pain. The last case is an ... achievement. Intentionally
      creating a language allowing that would be a non-trivial design objective. 

 - Defer is a nice combinations of object destructor with exception semantics
   and transfers nicely to a real-time language. This features runtime behavior
   is clear and easy to integrate in analysis. 
 - Its not the "verbosity" of C that makes C (and to a lesser degree C++) 
   error prone - Go is verbose in comparison to Haskell but has none of 
   the problems of C (... may have to to with the lead developers ...)
 - Interfaces types are good way to provide abstract types.  
 - As I suspected type inference is not really important Go aimed for the
   about the same trade-off as I did. 
   

Core Four Stack Architecture:
-----------------------------

These are written down and are currently adapted to the new LLVM Reforth 
implementation)

Prior designs had a forth style two stack layout. That solved several 
problems, but

  1. Required moving function arguments by default
  2. Special syntax to reliably indicate that moves are unnecessary.

Coming form C, it might seem that using two stacks is memory bloaty, but 
forth with two stacks is used on embedded systems for over 40 years, which 
clearly indicates that using two stacks is in practice very memory efficient.

Given contemporary hardware adding a third stack to avoid the moving of 
functions arguments should be unproblematic for the same practical reasons, 
as adding a second one 40 Years ago. Reserving a memory area for the third 
stack, allows to implement a fourth stack growing in the opposite direction, 
without requiring any new memory section.

The emerging 4-Stack layout can have the following advantages:

  - Natural call by name semantics
  - Function Arguments are never copied out of the way
  - Proper Tail-calls
  - Manual hard real-time "garbage collection"
  - Dynamically sized values are easy to abandon)
  
It has the following caveats:
  - Mild restrictions to pointer usage
  - Medium restrictions on mutable pointer usages
    (mutable pointers are a bane for efficient garbage collection,
     and this is approach is too simple to deal with that)
  
Having a elaborate region system to explain is a big obstacle to beginners.
However regions on stack seem to be simple enough to be easily to
visualize to provide a simple regions system almost reducing the mild
restrictions to those prohibiting the creation of dangling pointers. 


### Definitions, Memory Scopes and Pointers ###

There are ??? ways to bind names to values. 
All the above namebinds may use complexer patterns then
simple names. 

 1. The tmp/where expression:

    For storing arguments data on the argument stack. 
  ```
    let temp@R    
      n0 : T0 = EXPR0
      n1 : T1 = EXPR1 
      ...
      nN : TN = EXPRN 
    in expr
  ```
    or equivalently 
  
  ```
     EXPR 
       where     
         n0 : T0 = EXPR0
         n1 : T1 = EXPR1 
         ...
         nN : TN = EXPRN 
  ```

 
  TODO: attribute requirements
 
  The rationale is that the 
  ```
    f (g (h z))
  ```
  can be written
  ```
    f x where x = (g y where y = h z) # for fresh variables x and y      
  ```
  so the innermost 'where' frees its local memory right after the evaluation 
  of (g y). So that evaluation complex expressions with big arguments is 
  memory effective. That is enforces high memory locality and reduced cache 
  misses. The cache will have two hot-spots one one the argument and one on
  the result stack, whose memory areas are continuously switching roles.   
  
  This leaves call and shunt stacks untouched, so shunted and 
  copied data can fall though.  
 

  2. The shunt-region scope expression

     Defines shunt stack scope 
    ```
      let shunt@R 
        n1 : t1 = EXPR1
        ...
        nN : tN = EXPRN # may bind dynamic sized value
      in EXPR
    ```       
   - Next the current CSP is saved. 
   - Then SSP and CSP are swapped. 
   - Allocate region R on the shunt stack to store n0 ... nN 
   - Then the expressions EXPR0 to EXPRN are evaluated yielding results 
     to the shunt stack (Reforth abstractions have a destination argument)
   - DSC and CSP are swapped back (turning shunted data to call stack data)
   - Then EXPR is evaluated 
   - The saved CSP is restored (freeing region R) 

   TODO: Attribute requirements.

   This leaves the argument and result stacks untouched, so result data can 
   fall through the scope.
   Note that when evaluating EXPR the shunted data is on the call stack,
   thus shunting there will add the data the the old shunt stack region. 

 
  3. The shunt expression: 
    
     Difference to 2 is that "in" is missing. (TODO: Conflict free grammar?)
     ```
     shunt EXPR
     ``` 
     just allocates the result value for expression to the shunt current 
     shunt stack to store the value of the expr eval.  

  4. The tmp expression:

     Difference is a missing "in" 
     ```
     temp EXPR
     ```
     result allocates a copy on the results stack, and returns 
     a pointer to it. Using this in the EXPR of (tmp ... in EXPR)
     will put this to the result of the parents result scope. 
     Actually analog to shunt. 

  5. The Shunt region allocation expression:
     
     ```
     (@R Expr)
     ```
     Begins a fresh region on the shunt stack, evaluates expression, then
     discards the region. 


  6. The collect expression 
     
     Create a shunt-region R, evaluate expression, deep-copy data reachable
     by EXPR to result stack, free shunt region. 
    
     ```
       collect@R EXPR
     ```
  7. The shunt collect expression 
     
     Somewhat analog to collect but will deep-copy data to the parent shunt
     stack. 
     ```
      shunt collect@R EXPR
     ```
      1. Save CSP
      2. Swap CSP and SSP 
      3. Create a shunt-region R
      4. evaluate expression,
      5. Swap CSP and SSP (shunted data is now on the call stack)
      6. deep-copy data of EXPR to prent shunt region
      7. restore CSP (frees R)


### On Parsing the abstractions ###

There is a problem with parsing abstractions like 
```
  x          -> f x 
| y & x == 1 -> f y 
    & x == y -> f 1
```
in a way that makes & and | reusable extension syntax for the meta stage.
It will yield
```
  (x          -> f x) 
| (y & (   (x == 1 -> f y) 
         & (x == y -> f 1))
```
which decouples the syntactic element form a consistent semantic meaning.
In the abstraction the lhs of -> is a pattern in one case and a logical
expression in the other. 
However I at least tried twice to make a conflict free LALR(1) grammar
that unambiguously parses a semantically meaningful ast and has reusable
syntax elements, and failed to find clear and simple solutions.

 => The syntax element ($d -> $c) is simply called abstractoid.   


### Chained Patterns ###
In principle functions with multiple arguments are curried,
i.e. have a form like 
```
  x -> y -> f x y
```
if there  are complex patterns inplace of x and of y that 
yields problems with the syntax. For example:
```
  x -> y -> f x y | z -> g 
```
is syntactialy 
```
  x -> (y -> f x y | z -> g) 
```
or
```
  (x -> y -> f x y) | z -> g 
```

The common syntax resolution is to enforce use of parenthesis i.e.
```
     x -> (y -> f x y)
```
That introduces parenthesis clutter. This is solved by adopting the
Haskell's multi argument currying style to abbreviate the above as
```
     x y -> f x y
```
This has its own problem in conjuction with argument tags
```
 Foo. a -> bla
```
is not a single argument match of (Foo a b) but short for
```
 Foo. -> (a -> bla)
```
and a type error. 
Anyway Haskell's solution to write 
```
(Foo. a) -> bla
```
is short and clear. I never run into practical 
problems with it. 

But it does not seem combine that
well with layout record matches.

... unless

```
 Foo. -> (a -> bla)
```
is made a semantically valid unpacking. 
Woule be an error for Tags with argument, 
otherwise, so that is possible
syntax shuggar. 

Just use parenthesis for the arguments. 




Prototype Compiler Implementation Notes
---------------------------------------

The Haskell Prototype should be sufficient to implement metacore as self
hosting Language. 

It contains a lot of IR representations to have some input and output
of internal translation stages be different types.

In particular, in order from parsing the IR Languages are

  1. MetaCore : The S-Expr equivalent of the meta processor and a superset
                of the language expressing type values. 

  2. Core     : The result of the meta processor, which involves type checking.

  3. HardCore : The result Rewriting core into a completely desugared,
                lambda-lifted and name-all-temporaries form. 

  4. Reforth : The formal target Low Level Virtual Stack Machine, "could" be 
               hardware, but is designed to be efficiently translated into
               efficient code for existing hardware. 

  5. LLVM : Actual state-of-the art target machine code gen.    


Actually Refoth and LLVM are roughly on the same abstraction level, but Reforth 
is specifically designed as backend for a functional language and LLVM for 
C/C++. Doing a "horizontal" translation from Reforth to LLVM is much more 
efficient then trying reimplement a Reforth compiler from scratch with the 
same functionality even for one target architecture. Continuation passing style
is of course demanding on the optimization capabilities of LLVM but it's a 
modern C++ backend and should by its nature be "used" to extreme constant 
propagation and optimization requirements. The only worry is complexity
since each CPS instrction will be blowen up and optimized separately. 
Still its proably easier to write a custom LLVM opt pass for that later. If
I get the prototype nice, hopefully in next 5 years, I can proably find someone
on the LLVM team to help me with that.

### Use of the MetaState ###

Its designed to generalize type computations to macro computations that
to type computations as a sub-functionality. First use will be to do
a happy parser generator equivalent as library, not separate tool.

Having introduced runtime type information that stage seems a bit overblown. 
Initially it should solve a lot of the problems that introducing sufficiently 
efficient runtime type information solved. 
Lets assume we remove the deshugar functionality since it makes the the metacore 
to core translation awkward inserting virtual desugar macro calls everywhere. 
Then still a BNF LALR(1) Grammar extention could be asily be implemented by
a $BNF macro working the following way

```
$BNF LALR1 let  

   Rule1 = foo bar   {ReductionCode}  
         | ...  
```
with the configurable operator precedences for operator combinator libraries 
that will yield very might extension capablities without need for dedicated 
desugar hooks which where initially considered. The implementation outline would
look like
```
meta $BNF = LALR1 let $body => $( $bnf::lalr1::codegen $body )
```
could be extended by
```
meta $BNF = LALR2 let $body => $( $bnf::lalr2::codegen $body )
```
probably need a $do that can save  $( ... ) parentheses for a sequence of
experssions. 

Beeing able to implement the above parser generator seems to be a good 
indicator that the meta stage has a minimal complete feature set. 

### Conditional Cases For Meta Expressions ###

The meta level does not have the & guards since they are pattern mached
```
  meta $foo = (Foo $var1 $var2 & $x == $y) => ($var1,$var2,$x,$y) 
```
will cause 
```
 $foo (Foo i h & a == b) 
```
to reduce to 
```
(i,h,a,b)
```
if the match fails next "older" meta binding for $foo will be tried. (Called 
that SFINE, but its the substitutions failures for pattern matches that may
fail. )

The above parenthesis are required, since
```
  meta $foo = Foo $var1 $var2 & $x == $y => ($var1,$var2,$x,$y) 
```
would be short for 
```
  meta $foo = Foo => ( $var1 => ( $var2 => (& => ... ))) 
```
which is still a valid meta program. Guards can therefore not be supported in a 
clear way on the meta level. It could be done, but would need some syntax 
trickery. It is therefore not considered for the first prototypes.  

### Computation of Argument Type Value ###

TODO: Rework:
For computing arugument type of an application constraint solving is 
too complex. For the following Reason: If types create specializations
like for example (Map Int) to IntMap i.e. the result is indeed a computed 
lambda expression i.e. Turing complete. A constraint for solver is too complex 
since it would have to do internal type program evaluations. I.e. if a variable 
binding is computed it has to be inserted everywhere else, then asl type meta
computations have to be performed. Then consistency checked again.
Even if there is a sufficient algorithms, the error reporting is difficult
and may fast become harder to interpret then Haskell type errors.
Also the most general formulation of the constraint problem seems to be 
equivalent to the halting problem. So any actual algorithm will have to 
introduce restrictions while "pretending" to solve the general problem. 

...

Further notes: Assume that for the meta-type level constructors are never
bind to meta variables, which are prefixed by an '$'. Then
'Foo $d' will never do any computation after a substitution. Avoiding
the above problem. In the above example Map must be defined $Map, shifting
the problem to "$Map $foo" which introduces a syntax difference. ... 
Anyway in that case "$Map Int" would need to match "IntMap" requiring
that meta evaluation must be performed before the type match. 
and "$Map $foo" must match "$Map Int"  it does, but requires meta evaluation
after the variable matching. It would be possible to disable the first case.

Thus $UpperCase is not bound in the pattern of  =>  but matched.

This indeed allows to collect constraints for the cost of adding meta 
constructors, which is ok.

...

matching
```
X => Y
```
against 
```
$a => $b
```
will yield the constraints
```
$a >= X
$b <= Y
```
since a function with a bigger domain can be used anywere a smaller domain
is required, and the results are subset of the expected i.e. domain type.

...

Further we may have that

```
if T $a <= B $a and $a <= $b then T $b <= B $b   (1)
```
... that if the precondition is
```
forall $a : T $a <= B $a
```
the above is clear. Type arguments map to locations restricting the possible
value/bit patterns that might appear there, that is above is ok for
the required types. Should be provable.
....


The solution is to not use any constraint solving algorithms but pattern match
the synthesized argument value type with a somewhat "impure" extended matching
semantics for meta-pattern constants that are predicates. In particular 
P1 (P2 t) will match as (P2 (P1 t)) if P1 and P2 are predicates. That makes 
the meta-state not purely syntactic, but tracking if Constants are Predicate 
constructors is not much overhead.

Using a syntax annotation like a prefix ?  to indicate predicates 
Predicates would reduce implementation complexity, considerably
e.g. (?NonZero Word)
Or postfix (NonZero? Word) but thats too similar to the rebinding pattern math
i.e. (Foo? (Bar x)) -> ...  and (foo ? (Bar x)) -> ... prefix seems better.

There is an other pecularity with meta-abstractions
$t => $t => $t is not equivalent $t => ($t' => $t' )
the first argument bind will update the pattern in the domain - otherwise
it would not generate the semantics needed for types.  

...

For first prototype keep it stupidly simple:
  - Prefix ? to syntactically identify predicates
  - Always assume ?P1 (?P2 $t) != ?P2 (?P1 $t) even if that practially
    only the case if the outer predicated can only be validly defined on
    a set satisfying the inner predication. 
  - The only predicate subset rule is (?P1 ...($Pn  $t) ...) <= $t
  - Use prefix ~ to indicate interfaces. 
  - For type matching assume that instantiation possible i.e.
     - $t == ~I $t for any interface.  
      
Slightly less simple, the same but use context instead of syntax prefixes.   
  - Ok, and can be extended to track predicate base types to allow predicate
    permutations. 
  - Pattern meta variables use uppercase like
     - $Foo = $t => SomeOdd (NonZero $t)
    and match 
     - $Foo Int => ...
    by not binding $Foo but evaluating $Foo Int and use the result as pattern.
     - 
...

OLD: 

Due to the existence of subtypes it the selection of the correct type value
to pass to a function in an application is not trivial.  

For example 
```
let f : Foo (Maybe Word          ) $b    => Bar $b
    x : Foo (Maybe (NonZero Word)) Bool 
 in f x 
```
then the type matching is a combination of a sub-class and subtype relation.

Further to use subtyping consistently with type abstractions as meta 
abstractions direct usage of the inferable argument type since
```
(Foo (Maybe Word) $b => Bar $b) (Foo (Maybe (NonZero Word)) Bool)
```
will not match as a meta abstraction pattern, since a Word is expected where
a (NonZero Word) is given. The correct type to apply for the type computation
is 
```
(Foo (Maybe Word) Bool)  
```    

But good argument type computation is even more complex than that
```
let f : Either $b $b    => $b
    x : Either (NonZero Word) Word  
 in f x 
```
creates two possible bindings 
```
  $b = Word
  $b = (NonZero Word)
```
which is a formal contradiction that needs to be resolved. A common way to
do that is to not collect type bindings but sub-type relation constrains.
I.e. the above would yield
```
  Word <= $b
  (NonZero Word) <= $b
```
where '<=' is the sub-type relation. 
since (NonZero Word) <= Word that constraints can be solve by
```
Word <= $b
```
Using a constraint solver approach further allows to handle complexities 
like
```
  Odd (NonZero Word) <= $t
  NonZero (Odd Word) <= $t
```
which are the same constraints readably. Can be solve by binding to the 
(expressions) principal type which in both cases is Word.

Matching polymorphic functions needs some additional care 
```
let f : $b => (($d,$b) => $b) => $b
    x : Word
    y : (Word,$c) => $c   
 in (f x) y
```
yields the following constraints
```
Word <= $b
Word <= $d
$c   >= $d # $c is in a domain that reverses the sub-type relation requirement 
```
this will need alpha conversions to make bound variables collision free before
gathering the constraints. 
Note that for to function types as sets we have that (A -> B) is a subtype
of (A' -> B') if (A' is a subtype of A) and (B is a subtype of B').
The domain relations are reversed the subtype domain must at least contain
all argument values of the supertype domain.  

A further example: 

```
let f : $b => (($b,$d) => $d) => $b
    x : Word
    y : (Word,$c) => $c   
 in (f x) y
```
will yield the following domain binds
```
Word <= $b
Word <= $b
$c   >= $d  
```

However the following seems not to work 

```
let f : $b => (($b,$d,NonZero $d) => $d) => $b
    x : Word
    y : (Word,$c,$c) => $c   
 in (f x) y
```
will yield domain binds
```
Word <= $b
Word <= $b
$c >= $d
$c >= NonZero $d 
```
the solution is (mutually) recursive
```
$b = Word
$d = NonZero $c
$c = $d
```
which is logically "correct" (example is bad) since predicates are idempotent but
sadly does not work with type abstractions as meta-abstractions.
The resulting type computations above yields

```
((Word,$d,NonZero $d) => $d) (Word,NonZero $d',NonZero $d')
```
which will not pattern match as a meta-abstraction. As pattern it would
imply that $d = NonZero $d but syntax expressions are trees and can not have
cycles. 

The simplest solution is to reject the program and force the programmer to
write a little helper that fixes the type properties. 
It does not look intuitive that the above example can be validly typed anyway.
This has the additional advantage that the rejection can be done while 
collecting type constraints. We just reject matching pattern (P $t) against 
value $t' if $t' != (P $t'')

Some more examples for what the constraint solver should solve

```
let f : ( ($b,Word) => Word) => $b
    x : (Foo (Bar $c) ,$c) => Word   
 in f x
```
will yields
```
$b <= Foo (Bar $c) 
$c >= Word
```
which indicates that some variable elimination steps may be required. 
...
...
Start algorithms with a simple direct pattern of argTy <= domTy .e.g for the 
above
```
(Foo (Bar $c) ,$c) <= ($b,Word)
```
then add a matching phase creating the above constraints. 
... The above situation should not be possible since the argument type
should not have free type variables 

### Notes ###

Subtypes are implemented as predicates. 
if $a <= $b then 
either $a == $b 
or  exists $a' and Precicates P1...Pn so that
     (P1 (P2 ... (Pn $a'))) == $a and $a' == $b  
     
Assume that all constraints are collected by one pattern match
then each inequalty has the form of either

$t <= Expr
Expr <= $t

i.e. there is type variable either on the lhs or on the rhs.
this leads too the following  




### Constraint Solving ###

There are some standard algorithms, but maybe something direct and simple is
sufficient. We want that the type checking rejectes as few programs as possible
using simple type rules. That implies domain types in a function application
should indicate a maximum value set under a derived type variable substituion. 

As we have seen in the above this can be done by infering constraints for
type variables. The general contraint problem for a set of type variables

```
 LeftBound_i_(n_i) <= $t_i <=  RightBound_i_(n_i) 
 LeftBound_i_(n_1) <= $t_i <=  RightBound_i_(n_1) 
 ...
 LeftBound_i_(n_i) <= $t_i <=  RightBound_i_(n_i) 
```
where the bounds may contain type variables. 
however if $t_i solves 
```
 MaxOfLeftBounds_i <= $t_i <=  MinOfRightBounds_i 
```
then it solves each of the above equations. 
An simple  solution algorithm then would be

1. Solve the subsets of constraints that do not have variables in their bounds
2. Substitute derived solutions
3. Repeat until all solved or ... (abort conditions?)

Aside form the question under what conditions the above terminates and is
sufficient, that will require some work to generate descriptive error messages. 


...

The argument type does not have free variables. thus the right hand sides
can only come from bound variables in a codomain in an argument type.
With (1) we can then use the max of the left bounds as solution of the
problem, then check the if the minRight bounds are satisfied, i.e.
it  is a solution, if not any  alternative is also not a soultion since
it is bigger. 

...
Further ... if two different relations are used 
<= for matching in codomain and >= for matching in domain the
for 
```
TypeExpr <= $a
```
by construction all type variables in TypeExpr are type variables
of the argument type and $a is a type variable of the codomain type. 
(does that help?)

...
computing maxima/minima
```
T $a <= $b
T (U $c) <= b
```
will require $a == U $c 
... meh, maybe using a state for variable binds and update if possible is
    more viable (and errors easier to generate) then constraint collection
    and solving ... 

Does not look like its evidently possible to do something so simple that it 
does not require a mathematical specification to guarantee termination and
correctness ...  

#### Some analysis ####

Given a system of n+1 equations
```
 MaxOfLeftBounds_i <= $t_i <=  MinOfRightBounds_i 
```
and let Bottom/Top be types that indicate that there is no (senible)
lower/upper type bound. Then let
```
t_0 :=  MinOfRightBounds_0 if MinOfRightBounds_0 != Top
     |  MaxOfLeftBounds_0  otherwise
```
then inserting the above equation for equations with i != 0 create a 
contradition only if the system has no solution and ... 

Proof.: 

First some elemental rules
1.
```
if A <= B, and $
```




#### Other Approach ###

Constraint solving is complex, so maybe its simple to
to a direct stateful matching from left to right and inner to outer.
This also should make error message generation easier.

The central problem is of course type checking and application (f x).

For that lets call the synthezised argument type of x the is-type 
and the codomain type of f the ought-type. To have a valid typing, it is
required that
```
IsType <= OughtType
```
the initial state of the type checker is an empty set of type constraints
of the type variables of the for
```
LeftBound_i_(n_1) <= $t_i <= RightBound_i_(n_1) 
```
where each inequality is exactly generated by a match against a variable. 
for example if the current match requires
```
$c <= $a
```
and the match states contains
```
FooPredicate $c <= $a
```
then the state is updated to 
```
$c <= $a
```

since we looking for maximal lower bounds (and minimal upper bounds).
An error message is generated if the maximal lower bound becomes bigger
then a minimal upper bound or if type matches are not in the type order
relation, i.e. do not match at all. 

Further this approach allows a intuitive implementation of complexer situations
for example, the current match requires that
```
Foo $c <= $a
```
and the matching state requires that
```
Foo $d <= $a
```
then this requires that $c <= $d or $d <= $c and corresponding state updates.
Since the algorithm is iterative simple backtracking is possible. Try to
solve with the fist, if it does not work with the second constraint. 
Since the algorithm will traverse trough a ast data 
is cycle free it terminates - either with detecting
an unsatisfiable situation of a final constraint set. 

Formally thats a kind of  "descend" method, where the solution set is
reduce by adding/updating constraints, checking all implications on 
each constraint update. So if it does not fail its  easy to select the
solutions as  the set of maximal lower bounds where given and minimal
upper bounds if the mas lower bound is not available. 

the final constraint set has the form 

```
 MaxLowerBound_0 <= $t_0 <=  MinUpperBound_0 
 MaxLowerBound_1 <= $t_1 <=  MinUpperBound_1
 ...
 MaxLowerBound_n <= $t_n <=  MinUpperBound_n
```

and it remains to be shown how to solve this rsp. if setting

```
$t_0 := MaxLowerBound_0  
$t_1 := MaxLowerBound_1 
 ...
$t_n := MaxLowerBound_n 
```
if all have a max lower bound constructs always constructs a solution. 
well we check the consistency conditions
```
MaxLowerBound_i <= MinUpperBound_i
Foo == $t_j
j!=j
```
then Foo >= $t_j and Foo <= $t_j thus

```
MaxLowerBound_j[$t_i <- Foo] 
  <=
MinUpperBound_j[$t_i <- Foo] 
```
and thus the equation remains consistent if it was consistent.
If not substitution might yield some inconsistent equations
like "Foo $t_j <= $t_j" where Foo is neither a predicate or constuctor.

Substitution needs a cycle detection (maybe) to terminate, in either case
thats possible may just be expensive. 


### More Examples ###

Two dual ones
```
let f : ( $a   => $a   ) => $a
    x : ( Word => Word )    
 in f x
```
yields
```
$a   <= Word # for the domain match
Word <= $a   # for the codomain match
```
and the dual
```
let f : ( Word => Word ) => Word
    x : ( $a   => $a   )   
 in f x
```
yields
```
$a <= Word # for the codomain match
Word <= $a # for the domain match
```



### Notes on Meta declaration syntax ###

To avoid Turing completeness issues on argument type inference, 
meta variables including type variables and constructors are syntactically 
different from normal variables. 

however then there is no need for a meta keyword that is like let.
Its obvious from the lhs of an = what is defined.



### Constructors, Adts, Structs ###

For practical purposes where Haskell Adt syntax is extension unfriendly.
If adt construtors are extended by data needed for only some part of the
algorithms it is used with, for example due to feature extention,
all old pattern matches break. 

However having atd constructors have examtly one argument i.e. that
is a structure could solve the problem in the following way:

First a type constructor of a struct is also its value constructor.

```
 foo = Foo. ( foo : $bar ; bal : Int)
 bar = Bar. ( x : Int: y : Int)
```
Adts are then a alternative of structs
```
let union FooBar $bar = Foo $bar | Bar 
    x : Foo = Foo.( foo = n ; bar = 2) 
    f : FooBar = FooBar. x # a cast construtor has deticated typing rules FooBar. 
    f' = FooBar::Foo.( foo = n ; bar = 2) # syntax distintion by the 
```

The union syntex has now types on the lhs and rhs of '=' unlike
in haskell where there is a Type on the left and data on the right. 
Simple namesapces can be used directly construct adt members, also 
used for patternmatichening
```
   f ? FooBar::Foo. x -> ...
     | FooBar::Bar. x -> ...   
```
now extnding adts does not break syntax everyhere. 
Also different adts can naturally have the same members.
This indicates that tuples should be seedn as special
structs with the default fields .0 .1 .2 etc.

Caveat, flexibilty comes for more lengthy syntax.

But, the struct keyworkd would be used to define the structs 
inplace.

union FooBar $bar = struct Foo $bar where ( foo : $bar ; bal : Int) # inplace def of Foo 
                  | Bar # use of previous definition of Bar

However inplace struct definitions can be done using the struct ckewordl

##### Generalized adts #####

Is done as a syntax extension

```
let union 
      Expr $a 
        = struct Lift $a    where (val : $a)                     # lifted value
        | struct App  $a    where (val : [t](Lam (t=>$b),Lam t)) # beta reduction 
        | struct Fix  $a    where (val : Lam ($a=>$a))           # fixed point
      Expr ($a, $b)  
        = struct Pair $a $b where (val : $a)                     # product
      Expr ($a=>$b)
        = struct Lam  $a $b where (val : Lam $a => Lam $b)       # lamda abstraction
  in ...
```

We use the postfix . to indicate value constructions, so that they are syntactically different from
type constructions. This is required for the meta pattern cheker, that should match subtype expressions
correctly.  

The meta processor needs to interpret the type and constructor definitions for that. 

as proper syntax extenson, and not a new syntax 

Further, the matching would need the postfix . as well 

```
   x ? FooBar::Foo. f -> ...
     | FooBar::Bar. b -> ...   
```

suggests that f and b are of a name less record typied.
That is a simple SFINE semantics. Its only an error if all avae, representing the raw data structuring.  
alowing record matches like


```
   x ? FooBar::Foo. f -> ...
     | FooBar::Bar. ( x = 1 ; y = v) -> ...    
```

In principle this gives a syntax distinqtion between value and type constructions.
On type constructions the subtyping can be used for meta matching. 

The generality comes with the somewaht awkard bool value constructors

``` 
 x ? Bool::True. () -> ...
   | Bool::False. () -> ...
```

This still looks very consistent, only a bit lenghhy. True and falsue are emtpy struct constructors, i.e. tags only 


### Deprecated/Undecided Definitions ###

These definitions are still changing while developing the initial compiler. 


### Type Class ###

TODO: Needed anymore? 

A optionally parametrized set of types. Type classes does not necessarily 
involve methods, also the "class" keyword is used to define type classes with
methods E.g. struct Foo $a $b = ... defines the type class "Foo $a $b" as the 
set of all types where the variables $a and $b a replaced with types. 
The codomains of polymorphic functions have a type class not a single well 
defined type. 

### Type Class Instance ###
A type created from a type class by replacing all free type variables with
types. As runtime information it has an attached method table.  

### Subtype ###.

Since types are define as type-language constructs sub-typing is not simply
given by the subset relation on the corresponding value sets. They are
defined using language constructs but always have the property that
the values of the subtype are a subset of the values of the supertype.  

Note that Haskell's new-types do satisfy the sub-set(-equal) relation but are by 
construction not a subtype construction. In this context sub-types thus can not
be defined language independent.

However a subtype of a type $t will always have the form
(PredicateOrInterfaceTypeName $t') where $t is a subtype of $t'. And for all $t
it holds by definition that $t is a subtype of $t. 

(Is a formal definition required?)
...


### Biggest Matching Subtype ###

TODO: Generalize, do a formal recursive definition (after tinkering it out in
      Haskell)

This is the type provided as argument type to function calls. It leaves
the structure of the argument type pattern intact i.e. does not change
the underlying value representations where "important" 

Given a type pattern (S3 (S1 $t)) and a type 
(S1 (S2 (S3 Word))) the biggest matching subtype would be
(S3 (S1 (S2 Word))) provides that S3,S2,S1 are commuting subtype predicates.  

This requires the construction of new run-time type information. 


### Base Type ###

Recursively defined. The base type of $t is

- The base type of $t' if $t matches (PredicateOrInterfaceType $t')
- $t otherwise 



### Subtyping and runtime type constructions ###

The language should be usable to program small systems like an arduino. However,
this requires to be thrifty with ram memory occupying structures. Constant
flash memory is not that short. With respect to that all run-time type 
information should be stored as constants if possible. For dependent sized
arrays that is not possible however. This run-time feature must therefore
be supported. 

On an abstract level subtype constructors can be commutative.  
i.e. NonNegative (Odd Int) == Odd (NonNegative Int)
which creates somewhat complicated meta expression value matching.

I tinkered around with additional predicate grouping syntax, that does not 
add clarity.  

The problem is to decide if two predicates are commutative




### Replacement of regions with scoping rules ###

Summary: 

Regions are probably not remove-able without creating significant design costs
somewhere else. Also a region should be seen as a non-contigous set of memeory
adresses where the ordering relation is not the subset relation but given
by the life times, that is defined by syntactic scopes. There is the shunt 
stack for values that need to pass through syntactic scopes. 

Discussion deleted, too much work to clean up.

### The deep copy algorithm ###

It is possible that multiple pointers point to the same object, deep-copy
should not duplicate such objects. So simple mark and sweep will not
be sufficient. 

The algorithm should be something like 

  1. Collect the addresses of all visited pointers into an array - O(n). 
  2. Radix sort the array using the pointer values, so that
     pointers pointing to the same object are in a contiguous blocks - O(n). 
  3. Copy values and update pointers, O(n+m) where m is the cost of copying
     the data.
  4. Delete collection of pointer addresses - O(1) stack deallocation. 

There are applications that need globaly manage pointers to locally generated
data. This will require some addtional memory management facilities. 
Most simple is probably a user allocateable stacks and corresponding extensions
of the collect expressions.  

